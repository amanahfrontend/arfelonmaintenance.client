import {Component, OnInit, ViewEncapsulation, Input, OnDestroy} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-new-contract-modal',
  templateUrl: './new-contract-modal.component.html',
  styleUrls: ['./new-contract-modal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NewContractModalComponent implements OnInit, OnDestroy {
  @Input() data: any;
  subscribtionQuotationDetails: any;
  subscribtionContractTypes: any;
  quotationDetails: any;
  contractTypes: any;
  newContract: any;

  constructor(public activeModal: NgbActiveModal, private lookup: LookupService, private utilities:UtilitiesService) {
  }

  ngOnInit() {
    this.newContract = {};
    this.subscribtionQuotationDetails = this.lookup.getQuotationDetailsById(this.data.id).subscribe((quotationDetails) => {
        this.quotationDetails = quotationDetails;
        this.newContract.price = quotationDetails.price;
        //console.log(this.quotationDetails);
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();

      });

    this.subscribtionContractTypes = this.lookup.getContractTypes().subscribe((contractTypes) => {
        this.contractTypes = contractTypes.data;
        //console.log(this.contractTypes);
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();

      });
  }

  ngOnDestroy() {
    this.subscribtionQuotationDetails.unsubscribe();
  }

}
