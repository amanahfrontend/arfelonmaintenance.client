import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Message } from "primeng/components/common/message";
import { MessageService } from "primeng/components/common/messageservice";
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: "app-estimation",
  templateUrl: "./estimation.component.html",
  styleUrls: ["./estimation.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class EstimationComponent implements OnInit, OnDestroy {
  item: any;
  items: any[];
  msg: Message[];
  getItemsSubscribtion: Subscription;
  totalMarginPrice: number;
  toggleLoading: boolean;
  callId: number;
  caller: any;

  constructor(private activeRoute: ActivatedRoute, private lookUp: LookupService, private modalService: NgbModal, private utilities: UtilitiesService, private messageService: MessageService) {
  }

  ngOnInit() {
    /* Init view values*/
    this.items = [];
    this.totalMarginPrice = 0;
    this.toggleLoading = false;

    /*abstract id from router*/
    this.activeRoute.params.forEach((param: Params) => {
      this.callId = +param["id"];

      this.lookUp.getCustomerCall(this.callId).subscribe(
        (call) => {
          this.caller = call;
        },
        err => {
          if (err.status === 401 && this.utilities.unauthrizedAction()) {
            this.messageService.add({
              severity: "unathorize",
              summary: "unathorize",
              detail: "Unautorize"
            });
          } else {
            this.messageService.add({
              severity: "error",
              summary: "Server Error",
              detail: "Failed to load data due to server error"
            });
          }
        });
    });
  }

  ngOnDestroy() {
    this.caller = null;
    this.getItemsSubscribtion && this.getItemsSubscribtion.unsubscribe;
    this.utilities.routingFromAndHaveSearch = false;
  }

  requestCode(code) {
    this.toggleLoading = true;

    this.getItemsSubscribtion = this.lookUp.getItem(code).subscribe(
      (item) => {
        if (item) {
          item.soldQuantity = 0;
          item.margin = 0;
          item.totalPrice = 0;
          item.totalMarginPrice = 0;
          item.error = "";
          this.items.push(item);
          //console.log(this.items);
        }
        this.toggleLoading = false;
      },
      (err) => {
        this.toggleLoading = false;
        //console.log(err);
      });
  }

  deleteItem(no) {
    this.items = this.items.filter((item) => {
      return item.no !== no;
    });
  }
}
