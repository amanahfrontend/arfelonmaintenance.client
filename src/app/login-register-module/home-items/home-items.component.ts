import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { DragulaService } from 'ng2-dragula';
import { pagesItemSharedService } from '../../api-module/pagesItemsShared/pagesItemsShared.service';

@Component({
  selector: 'app-home-items',
  templateUrl: './home-items.component.html',
  styleUrls: ['./home-items.component.css']
})
export class HomeItemsComponent implements OnInit {
  items = [];
  itemsRoles: string[] = [];
  getDataForType: string;
  getUserDataFlag: boolean;
  getDataForRole: string;
  constructor(private pagesItemSharedService: pagesItemSharedService,
    private lookupS: LookupService, private dragulaService: DragulaService) { }

  ngOnInit() {
    this.pagesItemSharedService.getItemsValues();

    this.items = ['Area', 'Attendance Status', 'Availability', 'Building Types', 'Company Code', 'Contract Type', 'Cost Center', 'Division', 'Governorates', 'Shift', 'Languages', 'Problems'];
    // this.itemsRoles = ['Admin', 'Dispatchers', 'Supervisor', 'Engineers', 'Drivers', 'Foremen', 'Technicians'];
    this.getUserDataFlag = false;
    this.getData('Area');
  }

  getData(type) {
    this.getDataForType = type;
    //console.log(this.getDataForType);
  }

  applyDefaultActiveClass(item) {
    if (this.items.indexOf(item) == 0) {
      return 'uk-open';
    }

    return "";
  }

  applyActiveTab(role) {
    if (this.itemsRoles.indexOf(role) == 0) {
      return 'uk-active';
    }

    return "";
  }

}
