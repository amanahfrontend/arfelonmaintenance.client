import { FormsModule } from '@angular/forms';
import { AuthGuardGuard } from './../api-module/guards/auth-guard.guard';
import { RouterModule, Routes } from '@angular/router';
import { SharedModuleModule } from './../shared-module/shared-module.module';
import { LoginComponentComponent } from './../login-register-module/login-component/login-component.component';
import { HomePageComponent } from './../login-register-module/home-page/home-page.component';
import { userManagmentComponent } from '../admin/userManagment/userManagment.component';
import { AdminUsersComponent } from '../admin/admin-users/admin-users.component';
import { AssignTecComponent } from '../admin/assign-tec/assign-tec.component';
import { SettingDispatchersComponent } from '../admin/setting-dispatchers/setting-dispatchers.component';
import { EditDisAndProbModalComponent } from '../admin/setting-dispatchers/edit-dis-and-prob-modal/edit-dis-and-prob-modal.component';


import { NgModule } from '@angular/core';
import { HomeItemsComponent } from './home-items/home-items.component';
import { TimeSheetComponent } from '../admin/time-sheet/time-sheet.component';
import { HomeUserManagementComponent } from './home-user-management/home-user-management.component';
import { TeamModule } from '../admin/team-management-updated/team.module';
import { MapSettingsComponent } from '../admin/map-settings/map-settings.component';
import { RoleGuard } from '../api-module/guards/role-guard';


const routes: Routes = [
  {
    path: '',
    component: HomePageComponent, children: [
      { path: '', redirectTo: '/master-data', pathMatch: 'full' },
      { path: 'master-data', component: HomeItemsComponent , canActivate : [RoleGuard]},
      { path: 'user-management', component: HomeUserManagementComponent },
      { path: 'team-management', loadChildren: ()=> TeamModule },
      { path: 'dispatcher-settings', component: SettingDispatchersComponent },
      //{ path: 'timesheet', component: TimeSheetComponent },
      {path: 'map-settings', component: MapSettingsComponent}
    ],
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance', 'Supervisor' , 'Admin', 'Dispatcher'] }
  },
  { path: 'login', component: LoginComponentComponent },
];

@NgModule({
  imports: [
    SharedModuleModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    LoginComponentComponent,
    HomePageComponent,
    userManagmentComponent,
    AdminUsersComponent,
    AssignTecComponent,
    SettingDispatchersComponent, 
    EditDisAndProbModalComponent,
    HomeItemsComponent,
    HomeUserManagementComponent,
    MapSettingsComponent
  ],
  entryComponents: [
    EditDisAndProbModalComponent
  ]
})
export class LoginRegisterModuleModule {
}
