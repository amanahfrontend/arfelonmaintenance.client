import { Component, OnInit } from "@angular/core";
import { AngularFireMessaging } from "@angular/fire/messaging";
import { ActivatedRoute, Router } from "@angular/router";
import { pagesItemSharedService } from "../../api-module/pagesItemsShared/pagesItemsShared.service";
import { AlertServiceService } from "./../../api-module/services/alertservice/alert-service.service";
import { AuthenticationServicesService } from "./../../api-module/services/authentication/authentication-services.service";
import { AuthService } from "../../shared/services/auth.service";

@Component({
  selector: "app-login-component",
  templateUrl: "./login-component.component.html",
  styleUrls: ["./login-component.component.css"]
})
export class LoginComponentComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;
  arr: any = [];

  // private _hubConnection: HubConnection;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationServicesService,
    private alertService: AlertServiceService,
    private pagesItemSharedService: pagesItemSharedService,
    private afMessage: AngularFireMessaging,
    private authService: AuthService) {
  }

  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  login() {
    this.loading = true;

    // Mohammed Osman: Disable firebase. {Start}
    //this.afMessage.getToken.subscribe(
    //  (res) => {
    const res = null;// Mohammed Osman. Add this line to remove firebase usage.
    // Mohammed Osman: Disable firebase. {End}

    this.authenticationService.login(this.model.username, this.model.password, res).subscribe(
      loginResponse => {

        this.authService.setLocalStorage("userId", loginResponse.data.userId);
        this.authService.setLocalStorage("deviceId", loginResponse.data.userId);
        this.authService.setLocalStorage("userName", loginResponse.data.userName);

        if (loginResponse.data.roles) {
          this.authenticationService.userRoles.next(loginResponse.data.roles[0]);

          if (loginResponse.data.roles[0] === "Dispatcher") {
            this.arr = ["orderManagement"];
            this.pagesItemSharedService.getItemsValues();

            for (let i = 0; i < this.arr.length; i++) {
              this.pagesItemSharedService.actionHideMe[this.arr[i]] = true;
            }

            localStorage.setItem("hideMe", JSON.stringify(this.pagesItemSharedService.actionHideMe));
            this.router.navigate(["/dispatcher"]);
          } else if (loginResponse.data.roles[0] === "Supervisor") {

            this.arr = ["orderManagement"];
            this.pagesItemSharedService.getItemsValues();

            for (let i = 0; i < this.arr.length; i++) {
              this.pagesItemSharedService.actionHideMe[this.arr[i]] = true;
            }

            localStorage.setItem("hideMe", JSON.stringify(this.pagesItemSharedService.actionHideMe));
            this.router.navigate(["/supervisor"]);
          }
          else {
            this.arr = ["masterData", "usersManagement", "customersManagement", "teamManagement", "dispatcherSetting"];
            this.pagesItemSharedService.getItemsValues();

            for (let i = 0; i < this.arr.length; i++) {
              this.pagesItemSharedService.actionHideMe[this.arr[i]] = true;
            }

            localStorage.setItem("hideMe", JSON.stringify(this.pagesItemSharedService.actionHideMe));
            this.router.navigate(["./" + this.returnUrl]);
          }
        }
      },
      () => {
        this.alertService.error("User name or password is incorrect");
        this.loading = false;
      });
    //});
  }

}
