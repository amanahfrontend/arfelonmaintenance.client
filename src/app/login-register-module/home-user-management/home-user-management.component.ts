import { Component, OnInit } from "@angular/core";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";

@Component({
  selector: "app-home-user-management",
  templateUrl: "./home-user-management.component.html",
  styleUrls: ["./home-user-management.component.css"]
})
export class HomeUserManagementComponent implements OnInit {
  itemsRoles: any[];
  getDataForRole: string;
  items: any[];
  isLoading: boolean = true;
  constructor(private lookupS: LookupService) { }

  ngOnInit() {
    this.isLoading = true;
    this.getRoles();
  }

  getRoles() {
    this.lookupS.getAllRoles().subscribe(
      result => {
        this.itemsRoles = result.data;
        this.isLoading = false;

        if (this.itemsRoles.length > 0) {
          this.getUserData(this.itemsRoles[0].name);
          //console.log("getRoles", this.itemsRoles);
        }
      },
      () => {
        console.log("error");
      });
  }


  getUserData(role) {
    this.getDataForRole = role;
    //console.log(this.getDataForRole);
  }


  applyDefaultActiveClass(item) {
    if (this.items.indexOf(item) === 0) {
      return "uk-open";
    }

    return "";
  }

  applyActiveTab(role) {
    if (this.itemsRoles.indexOf(role) === 0) {
      return "uk-active";
    }

    return "";
  }
}
