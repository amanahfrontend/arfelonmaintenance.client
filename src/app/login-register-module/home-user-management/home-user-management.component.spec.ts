import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeUserManagementComponent } from './home-user-management.component';

describe('HomeUserManagementComponent', () => {
  let component: HomeUserManagementComponent;
  let fixture: ComponentFixture<HomeUserManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeUserManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeUserManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
