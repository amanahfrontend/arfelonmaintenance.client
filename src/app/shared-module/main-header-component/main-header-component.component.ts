import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from "@angular/core";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MessageService } from "primeng/primeng";
import { Subscription } from "rxjs/Subscription";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { NotificationModalComponent } from "../shared/notification-modal/notification-modal.component";
import { SharedNotificationService } from "../shared/shared-notification.service";
import { AuthenticationServicesService } from "./../../api-module/services/authentication/authentication-services.service";

@Component({
  selector: "app-main-header-component",
  templateUrl: "./main-header-component.component.html",
  styleUrls: ["./main-header-component.component.css"]
})
export class MainHeaderComponentComponent implements OnInit, AfterViewInit {
  isLoggedin: boolean = false;
  isloading: boolean = false;
  toggleNotification: boolean;
  newNotificationToggle: boolean;
  notificationBackgroundClass: any[];
  CurentUser: any = {};
  toggelingNotificationIconInterval: any;
  quotationSubscription: Subscription;
  callSubscription: Subscription;
  orderSubscription: Subscription;
  RoleSubscription: Subscription;
  // logInSubscription: Subscription;
  notifications: any[] = [];
  toggelingNotificationIconClass: string[] = [];
  @Output() loggedOut = new EventEmitter();
  role: any = "";

  constructor(
    private sharedNotificationService: SharedNotificationService,
    private lookupService: LookupService,
    private messageService: MessageService,
    private cdRef: ChangeDetectorRef,
    public authenticationService: AuthenticationServicesService,
    private router: Router, private utilities: UtilitiesService,
    private modalService: NgbModal) {
  }

  userId = "";

  ngOnInit() {
    this.getNotifications();

    this.authenticationService.loadNotification.subscribe(
      () => {
        this.getNotifications();
      });

    this.sharedNotificationService.notificationCountSubject.subscribe(
      (count) => {
        this.sharedNotificationService.notificationCount = count;

        this.lookupService.GetNotificationCenterCountByRecieverId(this.userId).subscribe(
          (response) => {
            if (response && response.isSucceeded) {
              this.sharedNotificationService.notificationCount = response.data;
              this.getNotifications();
            }
          });
      });

    this.authenticationService.isLoggedin.subscribe(
      loggedIn => {
        this.isLoggedin = loggedIn;
      });

    this.CurentUser = JSON.parse(this.getLocalStorage("AlghanimCurrentUser"));
    // if (this.CurentUser) {
    this.notificationBackgroundClass = ["hide-notification-transparent-background"];
    // let checkForLogInSubscription;
    // this.logInSubscription = this.authenticationService.isLoggedin.subscribe((value) => {

    // if (this.authenticationService.isLoggedin.value) {
    this.quotationSubscription = this.authenticationService.quotaionSignalR.subscribe(
      (quotation) => {
        console.log("got the quotation");
        quotation.type = "quotation";
        this.notifications.push(quotation);
        this.newNotificationToggle = true;
        this.toggelingNotificationIcon();
      },
      (err) => {
        console.log(err);
      });

    this.callSubscription = this.authenticationService.callSignalR.subscribe(
      (call) => {
        console.log(call);
        console.log("got the call");
        call.type = "call";
        this.notifications.push(call);
        this.newNotificationToggle = true;
        this.toggelingNotificationIcon();
      },
      (err) => {
        console.log(err);
      });


    this.RoleSubscription = this.authenticationService.userRoles.subscribe(
      (role) => {
        this.role = role[0];
        if (this.role === "Admin") {
          this.sharedNotificationService.notificationCount = 0;
        }
      }), (err) => {
        console.error(err);
      }


    // console.log('will subscribe to orders');
    this.orderSubscription = this.authenticationService.orderSignalR.subscribe(
      (order) => {
        console.log("got the order");
        order.type = "order";
        this.notifications.push(order);
        this.newNotificationToggle = true;
        this.toggelingNotificationIcon();
      },
      (err) => {
        console.log(err);
      });

    // } else {
    // this.orderSubscription && this.orderSubscription.unsubscribe();
    // this.callSubscription && this.callSubscription.unsubscribe();
    // this.quotationSubscription && this.quotationSubscription.unsubscribe();
    // }
    // });
  }

  getNotifications() {
    this.notifications = [];
    let currentUser = this.getLocalStorage("AlghanimCurrentUser");

    if (currentUser) {
      this.userId = JSON.parse(currentUser).data.userId;
    }

    if (this.userId) {
      this.lookupService.GetNotificationCenterCountByRecieverId(this.userId).subscribe(
        (response) => {

          if (response && response.isSucceeded) {
            this.sharedNotificationService.notificationCount = response.data;

            let notificationObj = {
              recieverId: this.userId,
              isRead: false,
              pageInfo: { pageNo: 1, pageSize: 5 }
            };

            this.lookupService.postNotificationCenter(notificationObj).subscribe(
              (response) => {
                //console.log("getNotificationCenterById", response.data.data);
                this.notifications = response.data.data;
              });
          }
        });
    }
  }

  onApproveRequest(e, notification: any, index: number, acceptChangeValue: boolean) {
    e.stopPropagation();
    var accept = acceptChangeValue;
    this.notifications[index].className = "wait";
    let orders = JSON.parse(notification.data);
    this.lookupService.putNotificationCenterChangeTeamOrderRank(acceptChangeValue, orders).subscribe((response) => {
      if (response && response.isSucceeded) {
        this.utilities.routingFromAndHaveSearch = true;
        let obj = notification;
        obj.isRead = true;

        this.lookupService.updateNotificationCenterItem(obj).subscribe(
          (response) => {
            if (response && response.isSucceeded) {
              this.sharedNotificationService.notificationCountSubject.next((this.sharedNotificationService.notificationCount));
              this.sharedNotificationService.notificationCount--;
              this.notifications.splice(index, 1);
              this.getNotifications();
              this.toggleNotificationBody();
              if (accept) {
                this.sharedNotificationService.acceptChangeRankSubject.next();
              }
            }
          });
      }
    });
  }

  onNotificationClick(notificationData, index: number) {
    this.isloading = true;
    this.utilities.routingFromAndHaveSearch = true;
    let obj = notificationData;
    obj.isRead = true;

    this.lookupService.updateNotificationCenterItem(obj).subscribe(
      (response) => {
        if (response && response.isSucceeded) {
          this.sharedNotificationService.notificationCountSubject.next((this.sharedNotificationService.notificationCount));

          let modalRef = this.modalService.open(NotificationModalComponent);
          modalRef.componentInstance.data = notificationData;
          this.sharedNotificationService.notificationCount--;
          this.notifications.splice(index, 1);
          this.getNotifications();
          this.isloading = false;

          modalRef.result
            .then(() => {
              console.log("then");
              this.toggleNotificationBody();
            })
            .catch(() => {

            });
        }
      });
  }

  onMarkAllAsRead() {
    this.notifications = [];
    let currentUser = this.getLocalStorage("AlghanimCurrentUser");

    if (currentUser) {
      this.userId = JSON.parse(currentUser).data.userId;
    }

    this.lookupService.putMarkAllAsReadByRecieverIdURL(this.userId).subscribe(
      (response) => {
        if (response && response.isSucceeded) {
          this.notifications = [];
          this.sharedNotificationService.notificationCount = 0;
          this.sharedNotificationService.notificationCountSubject.next(0);
          this.messageService.add({
            severity: "success",
            summary: "Successful!",
            detail: `All marked as read!`
          });
        }
      });
    // putMarkAllAsReadByRecieverIdURL
  }

  logoutAll() {

    this.notifications = [];
    this.newNotificationToggle = false;
    this.toggelingNotificationIconClass = ["show-notification-icon"];
    this.stopIntervalNotificationIconToggeling();
    this.loggedOut.emit();
    this.router.navigate(["/login"]);
  }

  toggleNotificationBody() {
    this.toggleNotification = !this.toggleNotification;
    this.newNotificationToggle = false;
    this.toggelingNotificationIconClass = ["show-notification-icon"];
    this.stopIntervalNotificationIconToggeling();
    if (this.notificationBackgroundClass.toString() === ["show-notification-transparent-background"].toString()) {
      this.notificationBackgroundClass = ["hide-notification-transparent-background"];
    } else {
      this.notificationBackgroundClass = ["show-notification-transparent-background"];
    }
  }

  toggelingNotificationIcon() {
    this.toggelingNotificationIconInterval = setInterval(
      () => {
        console.log(this.newNotificationToggle);
        if (this.newNotificationToggle) {
          if (this.toggelingNotificationIconClass.toString() === ["show-notification-icon"].toString()) {
            this.toggelingNotificationIconClass = ["hide-notification-icon"];
          } else {
            this.toggelingNotificationIconClass = ["show-notification-icon"];
          }
        } else {
          this.toggelingNotificationIconClass = ["show-notification-icon"];
        }
        // this.toggelingNotificationIcon(this.newNotificationToggle);
      },
      1000);
  }

  stopIntervalNotificationIconToggeling() {
    clearInterval(this.toggelingNotificationIconInterval);
  }

  ngAfterViewInit() {
    this.cdRef.detectChanges();
  }

  currentUserFullName() {
    return this.getLocalStorage("userName");
  }

  getLocalStorage(keyName: string) {
    return localStorage.getItem(keyName);
  }
}
