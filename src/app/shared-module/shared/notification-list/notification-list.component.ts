import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import { MessageService } from 'primeng/primeng';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationModalComponent } from '../notification-modal/notification-modal.component';
import { SharedNotificationService } from '../shared-notification.service';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {
  totalRecords: number = 0;
  isLoading: boolean = false;
  notifications: any = [];
  notificationObj: any = {};
  dateObj: any = {
    startDate: null,
    endDate: null
  };
  userId: string = "";
  constructor(private sharedNotificationService: SharedNotificationService,
    private lookupService: LookupService,
    private messageService: MessageService,
    private modalService: NgbModal) { }

  ngOnInit() {
    let alghanimCurrentUser = localStorage.getItem("AlghanimCurrentUser");

    if (alghanimCurrentUser) {
      this.userId = JSON.parse(alghanimCurrentUser).data.userId;
    }

    this.notificationObj = {
      recieverId: this.userId,
      isRead: null,
      pageInfo: {
        pageNo: 1,
        pageSize: 5
      }
    };

    this.showNotificationbetweenDates();
    this.sharedNotificationService.notificationCountSubject
      .subscribe(
        (count) => this.showNotificationbetweenDates()
      );
  }


  loadData(e) {
    this.notificationObj.pageInfo.pageNo = (e.first / 5) + 1;
    this.showNotificationbetweenDates();
  }

  showNotificationbetweenDates() {
    this.isLoading = true;
    this.notificationObj.startDate = this.dateObj.startDate;
    this.notificationObj.endDate = this.dateObj.endDate;
    this.notificationObj.recieverId = this.userId;

    this.lookupService.postNotificationCenterFilteredByDate(this.notificationObj).subscribe(
      (response) => {

        if (response && response.isSucceeded) {

          this.notifications = response.data.data;
          this.totalRecords = response.data.count;
          this.isLoading = false;
        }
      });
  }

  showNotificationbetweenDatesAndResetPaging() {
    this.notificationObj.pageInfo.pageNo = 1;
    if (new Date(this.dateObj.startDate).getTime() >= new Date(this.dateObj.endDate).getTime()) {
      this.showNotificationbetweenDates();
    } else {
      this.messageService.add({
        severity: 'error',
        summary: 'invalid dates!',
        detail: 'Start Date should be before End Date!'
      })
    }

  }

  onResetDates() {
    this.notificationObj.startDate = null;
    this.notificationObj.endDate = null;
    this.showNotificationbetweenDatesAndResetPaging();
  }

  onNotificationClick(notification) {
    if (notification.isRead) {
      let modalRef = this.modalService.open(NotificationModalComponent);
      modalRef.componentInstance.data = notification;

      modalRef.result.then(
        () => {
          console.log('then');
        })
        .catch(() => {
        });
    } else {
      this.isLoading = true;
      let obj = notification;
      obj.isRead = true;
      this.lookupService.updateNotificationCenterItem(obj).subscribe((response) => {
        this.sharedNotificationService.notificationCountSubject.next((this.sharedNotificationService.notificationCount - 1));
        if (response && response.isSucceeded) {
          let modalRef = this.modalService.open(NotificationModalComponent);
          modalRef.componentInstance.data = notification;

          modalRef.result.then(
            () => {
              console.log('then');
            })
            .catch(() => {
            });

          this.isLoading = false;
        }
      });
    }
  }


  onApproveRequest(e, notification: any, index: number, acceptChangeValue: boolean) {
    e.stopPropagation();
    this.isLoading = true;
    let orders = JSON.parse(notification.data);
    var accept = acceptChangeValue;
    this.lookupService.putNotificationCenterChangeTeamOrderRank(acceptChangeValue, orders).subscribe((response) => {
      if (response && response.isSucceeded) {
        if (!notification.isRead) {
          let obj = notification;
          obj.isRead = true;
          this.lookupService.updateNotificationCenterItem(obj).subscribe((response) => {
            if (response && response.isSucceeded) {
              this.isLoading = false;
              if (accept) {
                this.sharedNotificationService.acceptChangeRankSubject.next();
              }
              this.sharedNotificationService.notificationCountSubject.next((this.sharedNotificationService.notificationCount - 1));
            }
          });
        } else {
          this.isLoading = false;
        }
      }
    });
  }

  onDeleteNotification(e: any, notification: any, index: number) {
    e.stopPropagation();
    this.isLoading = true;
    this.lookupService.deleteNotificationCenter(notification.id).subscribe((response) => {
      this.sharedNotificationService.notificationCountSubject.next((this.sharedNotificationService.notificationCount - 1));
      if (response && response.isSucceeded) {
        this.messageService.add({
          severity: 'success',
          summary: 'deleted successfully!',
          detail: 'Order number ' + notification.id + ' is deleted successfully!'
        });
        this.notifications.splice(index, 1);
        this.isLoading = false;
      }
    });
  }

  onUpdateNotification(e: any, notification: any, index: number) {
    e.stopPropagation();
    this.isLoading = true;

    let obj = notification;
    obj.isRead = true;
    this.lookupService.updateNotificationCenterItem(obj).subscribe((response) => {
      this.sharedNotificationService.notificationCountSubject.next((this.sharedNotificationService.notificationCount - 1));
      if (response && response.isSucceeded) {
        this.messageService.add({
          severity: 'success',
          summary: 'updated successfully!',
          detail: 'Order number ' + notification.id + ' is updated successfully!'
        });
        this.isLoading = false;
      }
    });
  }

}
