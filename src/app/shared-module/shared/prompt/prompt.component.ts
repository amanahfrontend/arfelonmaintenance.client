import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-prompt',
  templateUrl: './prompt.component.html',
  styleUrls: ['./prompt.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class PromptComponent implements OnInit, OnDestroy {
  @Input() header;
  @Input() type;
  @Input() data: any;
  statesSubscriptions: Subscription;
  states: any[];
  Days: any = ['Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday'];
  dateToShowOnCal: any;
  defaultDate: any;
  startDate: Date;
  endDate: Date;

  constructor(public activeModal: NgbActiveModal, private lookup: LookupService) {
  }

  ngOnInit() {
    if(this.data.fromTime){
      this.startDate = new Date(this.data.fromTime)
    }
    if(this.data.toTime){
      this.endDate = new Date(this.data.toTime)
    }
    console.log(this.type + this.data.fromTime);
  }

  ngOnDestroy() {
    this.statesSubscriptions && this.statesSubscriptions.unsubscribe();
  }

  close(result) {
    console.log(result);
    if (this.type == 'Area' || this.type == 'Building Types' || this.type == 'Company Code' || this.type == 'Contract Type' 
      || this.type == 'Division' || this.type == 'Languages') {
      result = {
        id: this.data.id,
        area_No: result.area_No,
        code: result.code,
        // description: result.description,
        name: result.name,
      }
    }
    else if (this.type == 'Shift') {
      result = {
        id: this.data.id,
        day: result.day,
        fromTime: this.startDate,
        toTime: this.endDate
      }
    }
    else if (this.type == 'Problems') {
      result = {
        id: this.data.id,
        name: result.name,
        code: result.code,
        exceedHours: result.exceedHours
      }
    }
    else {
      result = {
        id: this.data.id,
        name: result.name,
      }
    }
    this.activeModal.close(result);
  }


  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
