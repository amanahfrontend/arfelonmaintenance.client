import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { userService } from "../../../api-module/services/userService/userService.service";


@Component({
  selector: 'app-teamMember.component',
  templateUrl: './teamMember.component.html',
  encapsulation: ViewEncapsulation.None
})

export class teamMemberModelComponent implements OnInit {

  @Input() header;
  @Input() data: any;
  statuses: any[];
  divisions: any = [];
  isSuccess: boolean = false;
  userData: any = {
    basic: { roleNames: [] },
    otherObj: {},
  }
  costCenters: any = [];
  superVisors: any = [];
  dispatchers: any = [];
  divisionObj: any = {};
  costCenter: any = {};
  supervisor: any = {};
  dispatcher: any = {};

  roleList: any = ['Driver', 'Technician'];
  role: string;

  constructor(public activeModal: NgbActiveModal, private lookup: LookupService, private messageService: MessageService,
    private utilities: UtilitiesService, private userService: userService) {
  }

  ngOnInit() {
    console.log(this.header);
    console.log(this.data);
    console.log(this.roleList);

    this.getDivisions();
    this.getCostCenter();
    this.getSuperVisor();
    this.getDispatchers();

  }

  getDivisions() {
    this.lookup.getDivision().subscribe(result => {
      this.divisions = result.data;
    }, error => {
      console.log('error');
    })
  }

  getCostCenter() {
    this.lookup.getCostCenter().subscribe(result => {
      this.costCenters = result.data;
    }, error => {
      console.log('error');
    })
  }

  getSuperVisor() {
    this.lookup.getAllSuperVisor().subscribe(result => {
      this.superVisors = result.data;
    }, error => {
      console.log('error');
    })
  }

  getDispatchers() {
    this.lookup.getAllDispatchers().subscribe(result => {
      this.dispatchers = result.data;
      console.log(this.dispatchers)
    }, error => {
      console.log('error');
    })
  }

  compareByOptionId(idFist, idSecond) {
    return idFist && idSecond && idFist.id == idSecond.id;
  }


  close(result) {
    console.log(result);
    result = {
      id: this.data.id,
      divisionId: result.divisionId,
      // description: result.description,
      // name: this.data.userName,
    }

    this.activeModal.close(result);
  }

  addTeamMember() {
    this.userData.basic.roleNames = [];
    this.userData.basic.roleNames.push(this.role);

    this.userService.checkUserEmailExist(this.userData.basic.email).subscribe(result => {
      if (result.data == false) {
        this.userService.checkUserNameExist(this.userData.basic.userName).subscribe(result => {
          if (result.data == false) {
            this.userService.checkUserPhoneExist(this.userData.basic.phone1).subscribe(result => {
              if (result.data == false) {

                this.lookup.postUser(this.userData.basic).subscribe(result => {
                  if (result.isSucceeded == true) {

                    this.userData.otherObj.userId = result.data.id;
                    this.userData.otherObj.name = result.data.userName;
                    this.userData.otherObj.divisionId = this.divisionObj.id;
                    this.userData.otherObj.costCenterId = this.costCenter.id;
                    this.userData.otherObj.supervisorId = this.supervisor.id;
                    this.userData.otherObj.statusId = this.dispatcher.id;
                    this.userData.otherObj.divisionName = this.divisionObj.name;
                    this.userData.otherObj.costCenterName = this.costCenter.name;
                    this.userData.otherObj.supervisorName = this.supervisor.name;
                    this.userData.otherObj.dispatcherName = this.dispatcher.name;

                    if (this.role == 'Driver') {
                      this.lookup.postDriver(this.userData.otherObj).subscribe((data) => {
                        if (data.isSucceeded == true) {
                          this.messageService.add({
                            severity: 'success',
                            summary: 'Successful!',
                            detail: "New value saved Successfully!."
                          });
                        }
                        console.log(this.userData.otherObj);
                        this.activeModal.close()
                      },
                        err => {
                          err.status == 401 && this.utilities.unauthrizedAction();

                          this.messageService.add({
                            severity: 'error',
                            summary: 'Failed!',
                            detail: "Failed to Save due to network error"
                          });
                        })
                    }

                    if (this.role == 'Technician') {
                      this.lookup.postTechnician(this.userData.otherObj).subscribe((data) => {
                        if (data.isSucceeded == true) {
                          this.messageService.add({
                            severity: 'success',
                            summary: 'Successful!',
                            detail: "New value saved Successfully!."
                          });
                        }
                        console.log(this.userData.otherObj);
                        this.activeModal.close()
                      },
                        err => {
                          err.status == 401 && this.utilities.unauthrizedAction();

                          this.messageService.add({
                            severity: 'error',
                            summary: 'Failed!',
                            detail: "Failed to Save due to network error"
                          });
                        })
                    }
                  }
                  else {
                    this.messageService.add({
                      severity: 'error',
                      summary: 'Failed!',
                      detail: "Failed to Save due to network error"
                    });
                  }
                })
              }
              else {
                this.messageService.add({
                  severity: 'error',
                  summary: 'Failed!',
                  detail: "this phone already exist"
                });
              }
            })
          }
          else {
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "this user name already exist"
            });
          }
        })
      }
      else {
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: "this email already exist"
        });
      }
    })
  }


  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
