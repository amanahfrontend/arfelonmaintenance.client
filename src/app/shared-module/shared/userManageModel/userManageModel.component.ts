import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { MessageService } from 'primeng/components/common/messageservice';
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { userService } from "../../../api-module/services/userService/userService.service";
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-userManageModel',
  templateUrl: './userManageModel.component.html',
  encapsulation: ViewEncapsulation.None
})
export class userManageModelComponent implements OnInit, OnDestroy {
  @Input() header;
  @Input() role;
  @Input() data: any;
  pf: number;
  statesSubscriptions: Subscription;
  isSaving: boolean;
  states: any[];
  Days: any = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  dateToShowOnCal: any;
  defaultDate: any;
  startDate: Date;
  endDate: Date;
  divisions: any = [];
  isSuccess: boolean = false;
  userObj: any = { roleNames: [] };
  userData: any = {
    basic: { roleNames: [] },
    otherObj: {},
  };
  costCenters: any = [];
  superVisors: any = [];
  dispatchers: any = [];
  engineers: any = [];
  managers: any = [];
  divisionObj: any = {};
  costCenter: any = {};
  supervisor: any = {};
  dispatcher: any = {};
  managerObj: any = {};
  engineer: any = {};
  isDriver: boolean = true;
  isLoading: boolean = true;
  numberOfServicesFinished: number = 0;
  numberOfServicesCalled: number = 0;

  show_resetPasswordFields: boolean = false;
  newPassword: string = null;
  userName: string = null;

  constructor(public activeModal: NgbActiveModal,
    private lookup: LookupService,
    private messageService: MessageService,
    private utilities: UtilitiesService,
    private userService: userService) {
  }

  ngOnInit() {
    //console.log(this.role);
    //console.log(this.header);
    //console.log(this.data);
    this.userName = this.data.name;

    this.getDivisions();
    this.getCostCenter();
    this.getSuperVisor();
    this.getDispatchers();
    this.getEngineers();
    this.getManagers();
    this.getRolesById();
  }

  ngOnDestroy() {
    this.statesSubscriptions && this.statesSubscriptions.unsubscribe();
  }


  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  getManagers() {
    this.seriveCalled();
    this.lookup.getAllMangaer().subscribe(result => {
      this.managers = result.data;
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  getEngineers() {
    this.seriveCalled();
    this.lookup.getAllEngineers().subscribe(result => {
      this.engineers = result.data;
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  getDivisions() {
    this.seriveCalled();
    this.lookup.getDivisionEnUrl().subscribe(result => {
      this.divisions = result.data;
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  getCostCenter() {
    this.seriveCalled();
    this.lookup.getCostCenter().subscribe(result => {
      this.costCenters = result.data;
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  getSuperVisor() {
    this.seriveCalled();
    this.lookup.getAllSuperVisor().subscribe(result => {
      this.superVisors = result.data;
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  getDispatchers() {
    this.seriveCalled();
    this.lookup.getAllDispatchers().subscribe(result => {
      this.dispatchers = result.data;
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  compareByOptionId(idFist, idSecond) {
    return idFist && idSecond && idFist.id == idSecond.id;
  }

  getRolesById() {
    if (this.data.id) {
      if (this.role == 'Admin') {
        this.lookup.getUserById(this.data.id).subscribe(
          res => {
            this.userData.basic = res.data;
            this.pf = this.userData.basic.pf;
            this.isLoading = false;
          }, err => console.log('err')

        );
      }
      if (this.role == 'Dispatcher') {
        this.lookup.getDispatcherById(this.data.id).subscribe(
          res => {
            this.userData.basic = res.data;
            this.isLoading = false;
            this.divisionObj = {
              id: this.userData.basic.divisionId,
              name: this.userData.basic.divisionName
            };
            this.costCenter = {
              id: this.userData.basic.costCenterId,
              name: this.userData.basic.costCenterName
            };
            this.supervisor = {
              id: this.userData.basic.supervisorId,
              name: this.userData.basic.supervisorName
            };
            this.pf = this.userData.basic.pf;
          }, err => console.log('err')

        );
      }
      if (this.role == 'Supervisor') {
        this.lookup.getSuperVisorById(this.data.id).subscribe(
          res => {
            this.userData.basic = res.data;
            this.isLoading = false;
            this.divisionObj = {
              id: this.userData.basic.divisionId,
              name: this.userData.basic.divisionName
            };
            this.managerObj = {
              id: this.userData.basic.managerId,
              name: this.userData.basic.managerName
            };
            this.costCenter = {
              id: this.userData.basic.costCenterId,
              name: this.userData.basic.costCenterName
            };
            this.pf = this.userData.basic.pf;
          }, err => console.log('err')

        );
      }
      if (this.role == 'Manager') {
        this.lookup.getManagerById(this.data.id).subscribe(
          res => {
            this.userData.basic = res.data;
            this.isLoading = false;
            this.divisionObj = {
              id: this.userData.basic.divisionId,
              name: this.userData.basic.divisionName
            };
            this.costCenter = {
              id: this.userData.basic.costCenterId,
              name: this.userData.basic.costCenterName
            };
            this.pf = this.userData.basic.pf;
          }, err => console.log('err')

        );
      }
      if (this.role == 'Material Controller') {
        this.lookup.getMaterialControllerById(this.data.id).subscribe(
          res => {
            this.userData.basic = res.data;
            this.isLoading = false;
            this.divisionObj = {
              id: this.userData.basic.divisionId,
              name: this.userData.basic.divisionName
            };
            this.costCenter = {
              id: this.userData.basic.costCenterId,
              name: this.userData.basic.costCenterName
            };
            this.pf = this.userData.basic.pf;
          }, err => console.log('err')

        );
      }
      if (this.role == 'Engineer') {
        this.lookup.getEngineerById(this.data.id).subscribe(
          res => {
            this.userData.basic = res.data;
            this.isLoading = false;
            this.divisionObj = {
              id: this.userData.basic.divisionId,
              name: this.userData.basic.divisionName
            };
            this.costCenter = {
              id: this.userData.basic.costCenterId,
              name: this.userData.basic.costCenterName
            };
            this.pf = this.userData.basic.pf;
          }, err => console.log('err')

        );
      }
      if (this.role == 'Technician') {
        this.lookup.getTechnicianById(this.data.id).subscribe(
          res => {
            this.userData.basic = res.data;
            this.isLoading = false;
            this.divisionObj = {
              id: this.userData.basic.divisionId,
              name: this.userData.basic.divisionName
            };
            this.costCenter = {
              id: this.userData.basic.costCenterId,
              name: this.userData.basic.costCenterName
            };
            this.supervisor = {
              id: this.userData.basic.supervisorId,
              name: this.userData.basic.supervisorName
            };
            this.managerObj = {
              id: this.userData.basic.managerId,
              name: this.userData.basic.managerName
            };
            this.dispatcher = {
              id: this.userData.basic.dispatcherId,
              name: this.userData.basic.dispatcherName
            };
            this.isDriver = this.userData.basic.isDriver;
            this.pf = this.userData.basic.pf;
          }, err => console.log('err')

        );
      }
      if (this.role == 'Foreman') {
        this.lookup.getForemanById(this.data.id).subscribe(
          res => {
            this.userData.basic = res.data;
            this.divisionObj = {
              id: this.userData.basic.divisionId,
              name: this.userData.basic.divisionName
            };
            this.costCenter = {
              id: this.userData.basic.costCenterId,
              name: this.userData.basic.costCenterName
            };
            this.supervisor = {
              id: this.userData.basic.supervisorId,
              name: this.userData.basic.supervisorName
            };
            this.managerObj = {
              id: this.userData.basic.managerId,
              name: this.userData.basic.managerName
            };
            this.dispatcher = {
              id: this.userData.basic.dispatcherId,
              name: this.userData.basic.dispatcherName
            };
            this.engineer = {
              id: this.userData.basic.engineerId,
              name: this.userData.basic.engineerName
            };
            this.pf = this.userData.basic.pf;
          }, err => console.log('err')

        );
      }
    }
    else {
      this.userData.basic = {};
    }
  }

  close(result) {
    //console.log(result);
    result = {
      id: this.data.id,
      divisionId: result.divisionId,
      // description: result.description,
      // name: this.data.userName,
    };
    this.activeModal.close(result);
  }

  saveUser() {
    this.isSaving = true;
    this.userData.basic.roleNames = [];
    this.userData.basic.roleNames.push(this.role);
    if (this.data.id) {
      this.userData.basic.id = this.data.userId;
      this.userData.phone = this.data.phone;
      this.userData.otherObj.pf = this.pf;
      if (this.role == 'Admin') {
        this.userData.basic.id = this.data.id;
        this.lookup.editUser(this.userData.basic).subscribe(result => {
          if (result.isSucceeded == true) {
            this.messageService.add({
              severity: 'success',
              summary: 'Successful!',
              detail: "New value saved Successfully!."
            });
          }
          //console.log(this.userData.dispatcherObj);
          this.activeModal.close();
        },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to save due to network error"
            });
          });
      }

      if (this.role != 'Admin') {
        this.lookup.editUser(this.userData.basic).subscribe(result => {
          if (result.isSucceeded == true) {
            this.isSuccess = true;
            this.userData.otherObj.isDriver = this.isDriver;
            this.userData.otherObj.pf = this.pf;
            this.userData.otherObj.id = this.data.id;
            this.userData.otherObj.userId = result.data.id;
            this.userData.otherObj.name = result.data.userName;
            if (this.divisionObj) {
              this.userData.otherObj.divisionId = this.divisionObj.id;
              this.userData.otherObj.divisionName = this.divisionObj.name;
            }
            if (this.costCenter) {
              this.userData.otherObj.costCenterId = this.costCenter.id;
              this.userData.otherObj.costCenterName = this.costCenter.name;
            }
            if (this.supervisor) {
              this.userData.otherObj.supervisorId = this.supervisor.id;
              this.userData.otherObj.supervisorName = this.supervisor.name;
            }
            if (this.managerObj) {
              this.userData.otherObj.managerId = this.managerObj.id;
              this.userData.otherObj.managerName = this.managerObj.name;
            }
            if (this.dispatcher) {
              this.userData.otherObj.dispatcherId = this.dispatcher.id;
              this.userData.otherObj.dispatcherName = this.dispatcher.name;
            }
            if (this.engineer) {
              this.userData.otherObj.engineerId = this.engineer.id;
              this.userData.otherObj.engineerName = this.engineer.name;
            }


            if (this.role == 'Dispatcher') {
              this.lookup.editUserDispatcher(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                //console.log(this.userData.dispatcherObj);
                this.activeModal.close();
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                });
            }
            if (this.role == 'Supervisor') {
              this.lookup.editUserSuperVisor(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                //console.log(this.userData.otherObj);
                this.activeModal.close();
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                });
            }
            if (this.role == 'Manager') {
              this.lookup.editUserManager(this.userData.otherObj).subscribe(
                (data) => {
                  if (data.isSucceeded == true) {
                    this.messageService.add({
                      severity: 'success',
                      summary: 'Successful!',
                      detail: "New value saved Successfully!."
                    });
                  }
                  //console.log(this.userData.otherObj);
                  this.activeModal.close();
                },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                });
            }
            if (this.role == 'Material Controller') {
              this.lookup.editUserMateialController(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                //console.log(this.userData.otherObj);
                this.activeModal.close();
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                });
            }
            if (this.role == 'Engineer') {
              this.lookup.editUserEngineer(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                //console.log(this.userData.otherObj);
                this.activeModal.close();
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                });
            }
            if (this.role == 'Technician') {
              this.lookup.editUserTechnician(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                //console.log(this.userData.otherObj);
                this.activeModal.close();
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                });
            }
            if (this.role == 'Foreman') {
              this.lookup.editUserForeman(this.userData.otherObj).subscribe((data) => {
                if (data.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                //console.log(this.userData.otherObj);
                this.activeModal.close();
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                });
            }
          }
          else {
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "Failed to Save due to network error"
            });
          }
        }, error => {
          console.error(error);
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: error
          });
        });
      }
    }
    else {
      {
        this.userService.checkUserNameExist(this.userData.basic.userName).subscribe(result => {
          if (result.data == false) {
            if (this.role == 'Admin') {
              this.lookup.postUser(this.userData.basic).subscribe(result => {
                if (result && result.isSucceeded == true) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Successful!',
                    detail: "New value saved Successfully!."
                  });
                }
                //console.log(this.userData.otherObj);
                this.activeModal.close();
              },
                err => {
                  err.status == 401 && this.utilities.unauthrizedAction();

                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to Save due to network error"
                  });
                });
            }
            if (this.role != 'Admin') {
              this.lookup.postUser(this.userData.basic).subscribe(result => {
                if (result.isSucceeded == true) {
                  this.isSuccess = true;
                  this.userData.otherObj.isDriver = this.isDriver;
                  this.userData.otherObj.pf = this.pf;
                  this.userData.otherObj.userId = result.data.id;
                  this.userData.otherObj.name = result.data.userName;
                  this.userData.otherObj.costCenterId = this.costCenter.id;
                  this.userData.otherObj.costCenterName = this.costCenter.name;
                  if (this.divisionObj) {
                    this.userData.otherObj.divisionId = this.divisionObj.id;
                    this.userData.otherObj.divisionName = this.divisionObj.name;
                  }
                  if (this.supervisor) {
                    this.userData.otherObj.supervisorId = this.supervisor.id;
                    this.userData.otherObj.supervisorName = this.supervisor.name;
                  }
                  if (this.managerObj) {
                    this.userData.otherObj.managerId = this.managerObj.id;
                    this.userData.otherObj.managerName = this.managerObj.name;
                  }
                  if (this.divisionObj) {
                    this.userData.otherObj.dispatcherId = this.dispatcher.id;
                    this.userData.otherObj.dispatcherName = this.dispatcher.name;
                  }
                  if (this.engineer) {
                    this.userData.otherObj.engineerId = this.engineer.id;
                    this.userData.otherObj.engineerName = this.engineer.name;
                  }

                  if (this.role == 'Dispatcher') {
                    this.lookup.postUserDispatcher(this.userData.otherObj).subscribe((data) => {
                      if (data.isSucceeded == true) {
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Successful!',
                          detail: "New value saved successfully!."
                        });
                      }
                      //console.log(this.userData.otherObj);
                      this.activeModal.close();
                    },
                      err => {
                        err.status == 401 && this.utilities.unauthrizedAction();

                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to save due to network error"
                        });
                      });
                  }
                  if (this.role == 'Supervisor') {
                    this.lookup.postSuperVisor(this.userData.otherObj).subscribe((data) => {
                      if (data.isSucceeded == true) {
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Successful!',
                          detail: "New value saved Successfully!."
                        });
                      }
                      //console.log(this.userData.otherObj);
                      this.activeModal.close();
                    },
                      err => {
                        err.status == 401 && this.utilities.unauthrizedAction();

                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to Save due to network error"
                        });
                      });
                  }
                  if (this.role == 'Driver') {
                    this.lookup.postDriver(this.userData.otherObj).subscribe((data) => {
                      if (data.isSucceeded == true) {
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Successful!',
                          detail: "New value saved Successfully!."
                        });
                      }
                      console.log(this.userData.otherObj);
                      this.activeModal.close();
                    },
                      err => {
                        err.status == 401 && this.utilities.unauthrizedAction();

                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to Save due to network error"
                        });
                      });
                  }
                  if (this.role == 'Engineer') {
                    this.lookup.postEngineer(this.userData.otherObj).subscribe((data) => {
                      if (data.isSucceeded == true) {
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Successful!',
                          detail: "New value saved Successfully!."
                        });
                      }
                      //console.log(this.userData.otherObj);
                      this.activeModal.close();
                    },
                      err => {
                        err.status == 401 && this.utilities.unauthrizedAction();

                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to Save due to network error"
                        });
                      });
                  }
                  if (this.role == 'Technician') {
                    this.lookup.postTechnician(this.userData.otherObj).subscribe((data) => {
                      if (data.isSucceeded == true) {
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Successful!',
                          detail: "New value saved Successfully!."
                        });
                      }
                      //console.log(this.userData.otherObj);
                      this.activeModal.close();
                    },
                      err => {
                        err.status == 401 && this.utilities.unauthrizedAction();

                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to Save due to network error"
                        });
                      });
                  }
                  if (this.role == 'Foreman') {
                    this.lookup.postForeman(this.userData.otherObj).subscribe((data) => {
                      if (data.isSucceeded == true) {
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Successful!',
                          detail: "New value saved Successfully!."
                        });
                      }
                      //console.log(this.userData.otherObj);
                      this.activeModal.close();
                    },
                      err => {
                        err.status == 401 && this.utilities.unauthrizedAction();
                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to Save due to network error"
                        });
                      });
                  }


                  if (this.role == 'Manager') {
                    this.lookup.postManager(this.userData.otherObj).subscribe((data) => {
                      if (data.isSucceeded == true) {
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Successful!',
                          detail: "New value saved Successfully!."
                        });
                      }
                      //console.log(data, this.userData.otherObj);
                      this.activeModal.close();
                    },
                      err => {
                        err.status == 401 && this.utilities.unauthrizedAction();

                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to save due to network error"
                        });
                      });
                  }

                  if (this.role == 'Material Controller') {
                    this.lookup.postMaterialController(this.userData.otherObj).subscribe((data) => {
                      if (data.isSucceeded == true) {
                        this.messageService.add({
                          severity: 'success',
                          summary: 'Successful!',
                          detail: "New value saved successfully!."
                        });
                      }
                      console.log(this.userData.otherObj);
                      this.activeModal.close();
                    },
                      err => {
                        err.status == 401 && this.utilities.unauthrizedAction();

                        this.messageService.add({
                          severity: 'error',
                          summary: 'Failed!',
                          detail: "Failed to save due to network error"
                        });
                      });
                  }
                }
                else {
                  this.messageService.add({
                    severity: 'error',
                    summary: 'Failed!',
                    detail: "Failed to save due to network error"
                  });
                }
              });
            }

          }
          else {
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: "this user name already exist"
            });
          }
        });
      }
    }
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  onResetPassword() {
    this.show_resetPasswordFields = true;
  }

  editOtherFields() {
    this.show_resetPasswordFields = false;
  }

  saveNewPassword(form: NgForm) {
    this.isLoading = true;
    const data = {
      Username: this.data.userName,
      NewPassword: form.value.newPassword
    };
    this.lookup.resetUserPassword(data).subscribe(
      res => {
        this.isLoading = false;
        console.log('%c password reset done successfully', 'color: orange; font-weight: bold;');
      }, err => {
        console.log(err);
      }
    );
  }

}
