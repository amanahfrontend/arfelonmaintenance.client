import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { orderModel } from './OrderModel';

export class teamOrderModel 
{
  public constructor(public teamId: number,public teamName ?: string, public  orders ? :Array<orderModel> )
    {}
}