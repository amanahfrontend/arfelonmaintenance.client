import { CustomerCrudService } from './../api-module/services/customer-crud/customer-crud.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertServiceService } from './../api-module/services/alertservice/alert-service.service';
import { AlertComponentComponent } from './shared/alert-component/alert-component.component';
import { FooterComponentComponent } from './footer-component/footer-component.component';
import { MainHeaderComponentComponent } from './main-header-component/main-header-component.component';
import { ApiModuleModule } from './../api-module/api-module.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, Type } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PromptComponent } from './shared/prompt/prompt.component';
import { languagesComponent } from './shared/languageModel/languageModel.component';
import { SearchComponent } from './search/search.component';
import { EditOrderModalComponent } from '../customer/edit-order-modal/edit-order-modal.component'
import { DataViewModule } from 'primeng/dataview';
import { GenerateEditOrderFormComponent } from "../customer/generate-edit-order-form/generate-edit-order-form.component";
import { SplitButtonModule } from 'primeng/components/splitbutton/splitbutton';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { GrowlModule } from 'primeng/components/growl/growl';
import { AccordionModule } from 'primeng/components/accordion/accordion';
import { MessagesModule } from 'primeng/components/messages/messages';
import { MessageModule } from 'primeng/components/message/message';
import { AddEditUserModalComponent } from "../admin/admin-users/add-edit-modal/add-edit-modal.component";
import { FileUploadModule } from 'primeng/components/fileupload/fileupload';
import { ProgressSpinnerModule } from 'primeng/components/progressspinner/progressspinner';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { InputSwitchModule } from 'primeng/components/inputswitch/inputswitch';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { MultiSelectModule } from 'primeng/components/multiselect/multiselect';
import { SelectButtonModule } from 'primeng/components/selectbutton/selectbutton';
// import {DataTableModule} from 'primeng/components/datatable/datatable';
import { DragulaModule } from "ng2-dragula";
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { SharedModule } from 'primeng/components/common/shared';
import { DateFromToComponent } from './date-from-to/date-from-to.component';
import { userManageModelComponent } from './shared/userManageModel/userManageModel.component';
import { teamModelComponent } from './shared/team/teamModel.component';
import { teamMemberModelComponent } from './shared/teamMember/teamMember.component';
import { DispatcherPipe } from '../admin/dispatcher.pipe';
import { TimeSheetComponent } from '../admin/time-sheet/time-sheet.component';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { TabViewModule } from 'primeng/tabview';
import { orderManagmentComponent } from '../admin/orderManagement/orderManagement.component';
import { teamManagmentComponent } from '../admin/teamManagement/teamManagement.component';
import { printOrderComponent } from '../dispatcher/dispatcher-order-board/printOrder/printOrder.component';
import { SimpleAdminContentComponent } from '../admin/simple-admin-content/simple-admin-content.component';
import { NotificationModalComponent } from './shared/notification-modal/notification-modal.component';
import { NotificationListComponent } from './shared/notification-list/notification-list.component';
import { ListboxModule } from 'primeng/listbox';
import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DragulaModule,
    ApiModuleModule,
    FormsModule,
    NgxDatatableModule,
    TooltipModule,
    SplitButtonModule,
    CalendarModule,
    GrowlModule,
    AccordionModule,
    MessagesModule,
    MessageModule,
    FileUploadModule,
    ProgressSpinnerModule,
    InputSwitchModule,
    DropdownModule,
    DataTableModule, SharedModule,
    MultiSelectModule,
    SelectButtonModule,
    ToggleButtonModule,
    TabViewModule,
    DataViewModule,
    ListboxModule,
    NgbModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireMessagingModule,
  ],
  declarations: [
    SimpleAdminContentComponent,
    TimeSheetComponent,
    DispatcherPipe,
    orderManagmentComponent,
    MainHeaderComponentComponent,
    FooterComponentComponent,
    AlertComponentComponent,
    PromptComponent,
    languagesComponent,
    SearchComponent,
    AddEditUserModalComponent,
    EditOrderModalComponent,
    GenerateEditOrderFormComponent,
    DateFromToComponent,
    userManageModelComponent,
    teamMemberModelComponent,
    teamModelComponent,
    // for use in future
    teamManagmentComponent,
    printOrderComponent,
    NotificationModalComponent,
    NotificationListComponent,
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    ListboxModule,
    SimpleAdminContentComponent,
    ToggleButtonModule,
    TimeSheetComponent,
    DispatcherPipe,
    orderManagmentComponent,
    MainHeaderComponentComponent,
    FooterComponentComponent,
    AlertComponentComponent,
    NgxDatatableModule,
    NgbModule,
    ApiModuleModule,
    TooltipModule,
    SearchComponent,
    SplitButtonModule,
    CalendarModule,
    GrowlModule,
    AccordionModule,
    MessagesModule,
    MessageModule,
    FileUploadModule,
    ProgressSpinnerModule,
    GenerateEditOrderFormComponent,
    DragulaModule,
    FormsModule,
    InputSwitchModule,
    DataTableModule, SharedModule,
    DropdownModule,
    MultiSelectModule,
    DateFromToComponent,
    SelectButtonModule,
    NotificationModalComponent,
    DataViewModule,
    // for use in future
    teamManagmentComponent,
    printOrderComponent,
    AngularFireModule,
    AngularFireMessagingModule,
  ],
  entryComponents: [
    PromptComponent,
    AddEditUserModalComponent,
    EditOrderModalComponent,
    languagesComponent,
    userManageModelComponent,
    teamMemberModelComponent,
    teamModelComponent,
    NotificationModalComponent,
  ]
})

export class SharedModuleModule {

  static forRoot(entryComponents?: Array<Type<any> | any[]>): ModuleWithProviders {
    return {
      ngModule: SharedModuleModule,
      providers: [
        AlertServiceService,
        CustomerCrudService
      ]
    };
  }
}

export const rootShared: ModuleWithProviders = SharedModuleModule.forRoot();

