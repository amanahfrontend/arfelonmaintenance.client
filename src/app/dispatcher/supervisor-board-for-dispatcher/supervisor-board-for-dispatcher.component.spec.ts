import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorBoardForDispatcherComponent } from './supervisor-board-for-dispatcher.component';

describe('SupervisorBoardForDispatcherComponent', () => {
  let component: SupervisorBoardForDispatcherComponent;
  let fixture: ComponentFixture<SupervisorBoardForDispatcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorBoardForDispatcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorBoardForDispatcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
