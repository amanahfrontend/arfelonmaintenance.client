import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { UtilitiesService } from '../../api-module/services/utilities/utilities.service';
import { NotificationsService } from '../notifications.service';
import { Subscription } from 'rxjs';
import { SharedNotificationService } from '../../shared-module/shared/shared-notification.service';



@Component({
  selector: 'app-supervisor-board-for-dispatcher',
  templateUrl: './supervisor-board-for-dispatcher.component.html',
  styleUrls: ['./supervisor-board-for-dispatcher.component.css']
})
export class SupervisorBoardForDispatcherComponent implements OnInit {
  toggleLoading: boolean;
  showFilter: boolean;
  fetchingDataDone: boolean;
  isBulkAssign: boolean;
  cornerMessage: any[];
  toggleOverflowX: string[];
  allUnassignedOrders = [];
  disabledStatus: string[];
  orders: any[];
  foremen: any = [];
  dispatcherOrdersSubscription: Subscription;
  getTeamByDispatcherByIdSubscription: Subscription;
  dispatcherList: any = [];
  selectedDispatcher: any = {};

  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private notificationsService: NotificationsService,
    private sharedNotificationService: SharedNotificationService) { }

  ngOnInit() {
    this.getDispatcherBySuper();
  }


  onSelectDispatcher(e: any) {
    console.log(e.value);
    this.selectedDispatcher = {};
    this.selectedDispatcher.id = e.value;
    this.getDispatcherUnassignedOrders();
    this.getTeamsAndOrders();

    // Mohammed Osman: Disable firebase. {Start}
    // this.notificationsService.getPermission();
    // this._receiveNotification();
    // Mohammed Osman: Disable firebase. {End}

    this.onRecieveChangeRank();
  }

  getDispatcherBySuper() {
    this.fetchingDataDone = true;
    this.toggleLoading = true;
    let superId = localStorage.getItem('AlghanimCurrentUserId');
    this.lookup.getDispatcherBySuper(superId).subscribe((res: any) => {
      this.dispatcherList = res.data;
      this.dispatcherList.map((dis: any) => {
        dis.label = dis.name;
        dis.value = dis.id;
      });
      this.toggleLoading = false;
    });
  }

  onRecieveChangeRank() {
    this.sharedNotificationService.acceptChangeRankSubject
      .subscribe(() => this.getTeamsAndOrders());
  }

  getDispatcherUnassignedOrders() {
    this.toggleLoading = true;
    this.dispatcherOrdersSubscription = this.lookup.getAllUnAssignedOrdersForDispatcher(this.selectedDispatcher.id).subscribe((dispatcher) => {
      this.orders = dispatcher.data;
      this.orders.map((order) => {
        order.classes = ['#orderId' + order.id];
        order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
        order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
      });
      this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      });
  }

  getTeamsAndOrders() {
    this.fetchingDataDone = false;
    this.getTeamByDispatcherByIdSubscription = this.lookup.getAllAssignedOrdersForDispatcher(this.selectedDispatcher.id).subscribe(
      (response) => {
        if (response.data) {
          this.foremen = response.data;
          this.foremen.map((forman) => {
            forman.teams.map((team: any) => {
              if (team.orders) {
                team.orders.map((order) => {
                  order.classes = ['#orderId' + order.id];
                  order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                  order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                });
                team.filterOrders = team.orders.slice();
              } else {
                team.filterOrders = [];
                team.orders = [];
              }
            });
          });
        }

        this.fetchingDataDone = true;
      },
      err => {
        this.fetchingDataDone = true;
      });
  }

  showHideFilter() {
    this.showFilter = !this.showFilter;
  }

  // applyFilterBoardTemplate(terms: any) {
  //   let emptyFilterObj: any = {
  //     dispatcherId: this.selectedDispatcher.id
  //   };
  //   terms.dispatcherId = this.selectedDispatcher.id;
  //   if (terms.fromWhere == 3) {
  //     this.lookup.postBoardOrderFilter(terms)
  //       .subscribe(
  //         (res) => {
  //           this.foremen = res.data
  //           this.foremen.map((forman) => {
  //             forman.orders.map((forman: any) => {
  //               if (forman.orders) {
  //                 forman.orders.map((order) => {
  //                   order.classes = ['#orderId' + order.id];
  //                   // if (this.disabledStatus.includes(order.statusName)) {
  //                   //   order.classes.push('disable-drag');
  //                   // }
  //                   order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
  //                   order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
  //                 });
  //                 forman.filterOrders = forman.orders.slice();
  //               } else {
  //                 forman.filterOrders = [];
  //                 forman.orders = [];
  //               }
  //             });

  //           })
  //         },
  //         err => console.log(err)
  //       );
  //     this.lookup.postBoardUnAssignedOrderFilter(terms).subscribe((response) => {
  //       this.orders = response.data,
  //         err => console.log(err)
  //     });
  //   } else if (terms.fromWhere == 2) {
  //     this.lookup.postBoardOrderFilter(terms)
  //       .subscribe(
  //         (res) => {
  //           this.foremen = res.data;
  //           this.foremen.map((forman) => {
  //             forman.teams.map((team: any) => {
  //               if (team.orders) {
  //                 team.orders.map((order) => {
  //                   order.classes = ['#orderId' + order.id];
  //                   // if (this.disabledStatus.includes(order.statusName)) {
  //                   //   order.classes.push('disable-drag');
  //                   // }
  //                   order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
  //                   order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
  //                 });
  //                 team.filterOrders = team.orders.slice();
  //               } else {
  //                 team.filterOrders = [];
  //                 team.orders = [];
  //               }
  //             });
  //           })
  //         },
  //         err => console.log(err)
  //       );
  //     this.lookup.postBoardUnAssignedOrderFilter(emptyFilterObj).subscribe((response) => {
  //       this.orders = response.data,
  //         err => console.log(err)
  //     });
  //   } else if (terms.fromWhere == 1) {
  //     this.lookup.postBoardOrderFilter(emptyFilterObj)
  //       .subscribe(
  //         (res) => {
  //           this.foremen = res.data;
  //           this.foremen.map((forman) => {
  //             forman.teams.map((team: any) => {
  //               if (team.orders) {
  //                 team.orders.map((order) => {
  //                   order.classes = ['#orderId' + order.id];
  //                   // if (this.disabledStatus.includes(order.statusName)) {
  //                   //   order.classes.push('disable-drag');
  //                   // }
  //                   order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
  //                   order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
  //                 });
  //                 team.filterOrders = team.orders.slice();
  //               } else {
  //                 team.filterOrders = [];
  //                 team.orders = [];
  //               }
  //             });
  //           });
  //         },
  //         err => console.log(err)
  //       );
  //     this.lookup.postBoardUnAssignedOrderFilter(terms).subscribe((response) => {
  //       this.orders = response.data,
  //         err => console.log(err)
  //     });
  //   }
  //   this.showHideFilter();
  // }



  applyFilterBoardTemplate(terms: any) {
    let emptyFilterObj: any = {
      dispatcherId: localStorage.getItem("AlghanimCurrentUserId")
    };
    terms.dispatcherId = this.selectedDispatcher.id;
    if (terms.fromWhere == 3) {
      // terms.fromWhere = 1;
      this.lookup.postBoardOrderFilter(terms)
        .subscribe(
          (res) => {
            this.foremen = res.data
            this.foremen.map((forman) => {
              forman.teams.map((team: any) => {
                if (team.orders) {
                  team.orders.map((order) => {
                    order.classes = ['#orderId' + order.id];
                    // if (this.disabledStatus.includes(order.statusName)) {
                    //   order.classes.push('disable-drag');
                    // }
                    order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                    order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                  });
                  team.filterOrders = team.orders.slice();
                } else {
                  team.filterOrders = [];
                  team.orders = [];
                }
              });

            })
          },
          err => console.log(err)
        );
      // terms.fromWhere = 2;
      this.lookup.postBoardUnAssignedOrderFilter(terms).subscribe((response) => {
        this.orders = response.data,
          err => console.log(err)
      });
    } else if (terms.fromWhere == 2) {
      // emptyFilterObj.fromWhere = 1;
      this.lookup.postBoardOrderFilter(terms)
        .subscribe(
          (res) => {
            this.foremen = res.data;
            this.foremen.map((forman) => {
              forman.teams.map((team: any) => {
                if (team.orders) {
                  team.orders.map((order) => {
                    order.classes = ['#orderId' + order.id];
                    // if (this.disabledStatus.includes(order.statusName)) {
                    //   order.classes.push('disable-drag');
                    // }
                    order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                    order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                  });
                  team.filterOrders = team.orders.slice();
                } else {
                  team.filterOrders = [];
                  team.orders = [];
                }
              });
            })
          },
          err => console.log(err)
        );
      // terms.fromWhere = 2;
      this.lookup.postBoardUnAssignedOrderFilter(emptyFilterObj).subscribe((response) => {
        this.orders = response.data,
          err => console.log(err)
      });
    } else if (terms.fromWhere == 1) {
      // terms.fromWhere = 1;
      this.lookup.postBoardOrderFilter(emptyFilterObj)
        .subscribe(
          (res) => {
            this.foremen = res.data;
            this.foremen.map((forman) => {
              forman.teams.map((team: any) => {
                if (team.orders) {
                  team.orders.map((order) => {
                    order.classes = ['#orderId' + order.id];
                    // if (this.disabledStatus.includes(order.statusName)) {
                    //   order.classes.push('disable-drag');
                    // }
                    order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                    order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                    // this.orderClassNaming(order);
                  });
                  team.filterOrders = team.orders.slice();
                } else {
                  team.filterOrders = [];
                  team.orders = [];
                }
              });
            });
          },
          err => console.log(err)
        );
      // emptyFilterObj.fromWhere = 2;
      this.lookup.postBoardUnAssignedOrderFilter(terms).subscribe((response) => {
        this.orders = response.data,
          err => console.log(err)
      });
    }
    this.showHideFilter();
  }


  resetBoardFilter() {
    let terms: any = {};
    terms.fromWhere = 3;
    this.applyFilterBoardTemplate(terms);
  }

  private _receiveNotification() {
    this.notificationsService.receiveMessage()
      .subscribe((msg: any) => {

        this.utilities.setGrowlMessage({
          severity: 'success',
          summary: 'Message',
          detail: msg.notification.body
        });
        const order = JSON.parse(msg.data.order);
        this._searchAndUpdateOrAddOrder(order);
      });
  }

  private _searchAndUpdateOrAddOrder(order) {

    this.foremen.map((foreman: any) => {
      foreman.teams.forEach((team: any) => {
        team.filterOrders.forEach((singleOrder, i) => {
          if (singleOrder.id == order.id) {

            if (order.statusName == "Complete" || order.statusName == "Canceled") {
              team.filterOrders.splice(i, 1);
            } else if (order.statusName == "On-Hold") {
              team.filterOrders.splice(i, 1);
            } else if (order.acceptanceFlag == 3) {
              team.filterOrders.splice(i, 1);
            } else {
              team.filterOrders[i] = order;
            }
          }
        });
      });
    })
    if (order.statusName == 'On-Hold' || order.statusName == 'Open') {
      this.orders.push(order);
    }
  }

}
