import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { Printd } from 'printd';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-printout',
  templateUrl: './printout.component.html',
  styleUrls: ['./printout.component.css']
})
export class PrintoutComponent implements OnInit {
  cornerMessage: any[];
  fetchingDataDone: boolean = true;
  toggleLoading: boolean = false;
  printedData: any[] = [];
  numberOfOrders: number = null;
  foremen: any[] = [];
  teams: any[] = [];
  selected_foremen: any[] = [];
  selected_teams: any = [];
  includeOpenOrders: boolean = false;
  foremenWithOrders: any[] = [];

  filterObj = {
    dispatcherId: +localStorage.getItem('AlghanimCurrentUserId'),
    teamsId: null,
    includeOpenOrders: false
  };

  printedSectionStyle = [`
    .order {
      display: flex;
      flex-direction: row;
      justify-content: flex-start;
      align-items: flex-start;
      padding: 10px;
      border: 01px solid #857c7c;
      margin-bottom: 10px;
    }

    .order-left-column , .order-right-column {
      flex: 1;
    }

    .order-left-column p , .order-right-column p {
      margin: 0;
    }

    .order-left-column p span , .order-right-column p span {
      font-weight: bold;
      margin-right: 10px;
    }

    .orders-container h3 , .orders-container h5 {
      margin-top: 05px;
      margin-bottom: 05px;
    }

    .orders-container {
      height: 1027px;      
    }

    .page-container:last-child > :last-child > .orders-container {
      height: auto !important;
    }
  `];

  @ViewChild('printoutForm') printoutForm: NgForm;
  @ViewChild('printedSection') printedSection: ElementRef;

  constructor(private lookup: LookupService) { }

  ngOnInit() {
    this.filterObj.teamsId = null;
    this.filterObj.includeOpenOrders = false;
    this.getData(this.filterObj)
    this.lookup.getAllForemans().subscribe(
      res => {
        this.foremen = res.data;
      }, err => console.log(err)
    );
  }

  onForemenSelect() {
    if (this.selected_foremen.length > 0) {
      this.toggleLoading = true;
      let data = this.selected_foremen.map(
        foreman => foreman = +foreman.id
      );
      let obj = { formansId: data };
      this.lookup.getTeamsForForemen(obj).subscribe(
        res => {
          this.teams = res.data;
          let teamsIds = this.teams.map(
            team => team = +team.id
          );
          this.filterObj.teamsId = teamsIds;
          this.filterObj.includeOpenOrders = this.includeOpenOrders;
          this.getData(this.filterObj);
        }, err => {
          console.log(err);
          this.toggleLoading = false;
        }
      );
    } else {
      this.filterObj.teamsId = null;
      this.filterObj.includeOpenOrders = this.includeOpenOrders;
      this.getData(this.filterObj);
    }
  }

  onTeamSelect() {
    if (this.selected_teams.length > 0) {
      this.toggleLoading = true;
      let teamsIds = this.selected_teams.map(
        team => team = +team.id
      );
      this.filterObj.teamsId = teamsIds;
      this.filterObj.includeOpenOrders = this.includeOpenOrders;
      this.getData(this.filterObj);
    } else {
      let teamsIds = this.teams.map(
        team => team = +team.id
      );
      this.filterObj.teamsId = teamsIds;
      this.filterObj.includeOpenOrders = this.includeOpenOrders;
      this.getData(this.filterObj);
    }
  }

  getData(obj) {
    this.toggleLoading = true;
    this.numberOfOrders = 0;
    this.lookup.getFilteredOrdersForPrint(obj).subscribe(
      (res: any) => {
        this.toggleLoading = false;
        if(res.data.length !> 0) {
          this.printedData = [];
        }

        this.printedData = res.data[0];
        this.foremenWithOrders = res.data[0].foremenWithOrders;
        for (let i = 0; i < this.foremenWithOrders.length; i++) {
          this.numberOfOrders = this.numberOfOrders + this.foremenWithOrders[i].orders.length;
        }
        this.foremenWithOrders.map(
          foreman => {
            foreman.pages = Math.ceil(foreman.orders.length / 4);
            /* foreman => pages => orders */
            foreman.ordersWithPages = [];
            for (let i = 1; i <= foreman.pages; i++) {
              foreman.ordersWithPages[i - 1] = [];
              for (let j = (i - 1) * 4; (j < foreman.orders.length) && (j < (i * 4)); j++) {
                foreman.orders[j].createdDate = this.formatDate(new Date(foreman.orders[j].createdDate));
                foreman.orders[j].contractExpiryDate = this.formatDate(new Date(foreman.orders[j].contractExpiryDate));
                foreman.ordersWithPages[i - 1].push(foreman.orders[j]);
              }
            }
          }
        );
      }, err => {
        console.log(err);
        this.toggleLoading = false;
      }
    );
  }

  onPrint() {
    let doc = new Printd();
    doc.print(this.printedSection.nativeElement, this.printedSectionStyle);
  }

  onReset(form: NgForm) {
    this.selected_foremen = [];
    this.selected_teams = [];
    this.includeOpenOrders = false;
    this.filterObj.teamsId = null;
    this.getData(this.filterObj);
  }

  onChangeIncludeOpenOrders() {
    if(this.selected_foremen.length > 0) {
      if(this.selected_teams.length > 0) {
        this.onTeamSelect();
      } else {
        this.onForemenSelect();
      }
    } else {
      this.filterObj.teamsId = null;
      this.filterObj.includeOpenOrders = this.includeOpenOrders;
      this.getData(this.filterObj);
    }
  }

  formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
  }
}
