import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersMapComponent } from './orders-map.component';

describe('OrdersMapComponent', () => {
  let component: OrdersMapComponent;
  let fixture: ComponentFixture<OrdersMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
