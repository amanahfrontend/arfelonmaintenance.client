import { Component, ViewChild, ElementRef, NgZone, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { UtilitiesService } from '../../api-module/services/utilities/utilities.service';
import { NotificationsService } from '../notifications.service';

declare var google: any;
// angular google maps with search box
import { FormControl } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

@Component({
  selector: 'app-orders-map',
  templateUrl: './orders-map.component.html',
  styleUrls: ['./orders-map.component.css']
})
export class OrdersMapComponent implements OnInit {
  toggleLoading: boolean;
  showFilter: boolean;
  unAssignedStatusClass: string;
  fullScreenMap: boolean;
  isBulkAssign: boolean;
  toggleOverflowX: string[];
  mapOrders: any = [];
  allUnassignedOrders = [];
  filterStatusClass = [];
  orders = [];
  filterObj: any = {};
  lat: number = 29.378586;
  lng: number = 47.990341;
  zoom: number = 10;
  dispatcherOrdersSubscription: Subscription;
  public searchControl: FormControl;
  searchResultMarker: Marker = null;


  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private notificationService: NotificationsService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    this.filterStatusClass = ['zero-filter-width'];
    this.unAssignedStatusClass = 'full-unassigned-width';
    this.getDispatcherUnassignedOrders();
    this.showHideFilter();
    let obj = this.filterObj = {};
    this.applyFilterMap(obj);
    this.onRefreshRate();

    // Mohammed Osman: Disable firebase. {Start}
    // this._receiveNotification();
    // Mohammed Osman: Disable firebase. {End}

    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.searchResultMarker = {
            lat: this.lat,
            lng: this.lng,
            label: 'search result marker',
            draggable: false
          };
        });
      });
    });
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }

  private _receiveNotification() {
    this.notificationService.receiveMessage()
      .subscribe((msg: any) => {
        this.utilities.setGrowlMessage({
          severity: 'success',
          summary: 'Message',
          detail: msg.notification.body
        });
        // this.applyFilterMap(this.filterObj);
      });
  }

  onRefreshRate() {
    var self = this;
    this.lookup.getMapSettings(1).subscribe((res: any) => {
      var response = res;
      window.setInterval(function () {
        self.applyFilterMap(self.filterObj);
      }, response.data.refresh_Rate * 60000);
    });
  }

  getDispatcherUnassignedOrders() {
    let dispatcherId = localStorage.getItem('AlghanimCurrentUserId');
    this.dispatcherOrdersSubscription = this.lookup.getAllUnAssignedOrdersForDispatcher(dispatcherId).subscribe((dispatcher) => {
      this.orders = dispatcher.data;
      this.allUnassignedOrders = dispatcher.data;
      this.orders.map((order) => {
        order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
        order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
      });
      this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      });
  }



  activeOrder(order) {
    this.lat = order.lat;
    this.lng = order.long;
    this.zoom = 16;
  }


  toggleUnAssigned(status) {
    if (status) {
      this.unAssignedStatusClass = 'full-unassigned-width';
    } else {
      this.unAssignedStatusClass = 'zero-filter-width';
    }
  }


  toggleFilter(status) {
    if (status) {
      this.filterStatusClass = ['full-filter-width'];
    } else {
      this.filterStatusClass = ['zero-filter-width'];
    }
  }

  choosePin(onHoldCount, orderType, orderDate, problem) {
    // reset pin
    var pin = '';
    let pinsDomain = this.chooseFolder(onHoldCount, problem);
    let externalColor = '';
    let internalColor = '';
    // check order type
    if (orderType.toLowerCase() === 'Preventive maintenance order'.toLowerCase()) {
      internalColor = 'blue';
    } else if (orderType.toLowerCase() === 'Cash compressor'.toLowerCase()) {
      internalColor = 'orange';
    } else if (orderType.toLowerCase() === 'Cash'.toLowerCase()) {
      internalColor = 'darkgreen';
    } else {
      internalColor = 'white';
    }
    // check order day
    var date = new Date(orderDate.toLowerCase());
    var d = date.getDay();
    switch (d) {
      case 0: //sunday
        externalColor = 'pink/pink';
        break;
      case 1: // monday
        externalColor = 'lightgreen/lightgreen';
        break;
      case 2: // tuesday
        externalColor = 'lightblue/lightblue';
        break;
      case 3: // wednesday
        externalColor = 'purple/purple';
        break;
      case 4: // thursday
        externalColor = 'turquoise/turquoise';
        break;
      case 5: // friday
        externalColor = 'gray/gray';
        break;
      case 6: // saturday
        externalColor = 'yellow/yellow';
        break;
    }
    pin = pinsDomain + externalColor + '.' + internalColor + '.svg';
    if (onHoldCount) {
      pin = pinsDomain + externalColor + '.' + internalColor + '.onhold.svg';
    }
    return pin;
  }



  chooseFolder(onHoldCount, problem) {
    let baseURL = '';
    if (onHoldCount) {
      baseURL = 'assets/Images/on-hold-pins/';
    } else {
      baseURL = 'assets/Images/pins/';
    }
    const result: any = {
      url: baseURL,
      name: ''
    };
    switch (problem) {
      case 'Commercial CAC':
        result.baseURL += 'CAC/';
        result.name = 'CAC';
        break;
      case 'Chillers':
        result.baseURL += 'CHI/';
        result.name = 'CHI';
        break;
      case 'CHS':
        result.baseURL += 'CHS/';
        result.name = 'CHS';
        break;
      case 'Cleaning Equipment':
        result.baseURL += '/';
        result.name = '';
        break;
      case 'Electrical':
        result.baseURL += 'ELEC/';
        result.name = 'ELEC';
        break;
      case 'Elevators':
        result.baseURL += 'ZELE/';
        result.name = 'ZELE';
        break;
      case 'Fire Fighting':
        result.baseURL += 'ZFIR/';
        result.name = 'ZFIR';
        break;
      case 'Fork Lifts':
        result.name = 'FLT';
        result.baseURL += 'FLT/';
        break;
      case 'Government Service':
        result.name = 'GAC';
        result.baseURL += 'GAC/';
        break;
      case 'Non Res Non Std':
        result.name = 'NSD';
        result.baseURL += 'NSD/';
        break;
      case 'Plumbing':
        result.name = 'PLB';
        result.baseURL += 'PLB/';
        break;
      case 'Residential CAC':
        result.name = 'RAC';
        result.baseURL += 'RAC/';
        break;
      case 'Residential Electrical Service':
        result.name = 'REL';
        result.baseURL += 'REL/';
        break;
      case 'Residential Plumbing Service':
        result.name = 'RPB';
        result.baseURL += 'RPB/';
        break;
      case 'Residential Swimming Pool Serv':
        result.name = 'RSW';
        result.baseURL += 'RSW/';
        break;
      case 'Security Audio Visual':
        result.name = 'SAV';
        result.baseURL += 'SAV/';
        break;
      case 'SRAC Service':
        result.name = 'SRA';
        result.baseURL += 'SRA/';
        break;
      case 'Non Res Standard':
        result.name = 'STD';
        result.baseURL += 'STD/';
        break;
      case 'Stationed operator':
        result.name = 'STO';
        result.baseURL += 'STO/';
        break;
      case 'Subcontractor':
        result.name = 'SUB';
        result.baseURL += 'SUB/';
        break;
      case 'Technology':
        result.name = 'TEC';
        result.baseURL += 'TEC/';
        break;
      case 'Video Data Network':
        result.name = 'VDN';
        result.baseURL += 'VDN/';
        break;
      case 'Water Cooler':
        result.name = 'WTC';
        result.baseURL += 'WTC/';
        break;
      default:
        result.baseURL += 'PINS';
        result.name = 'PINS/';
        break;
    }
    return baseURL + result.name + '/';
  }


  showHideFilter() {
    this.showFilter = !this.showFilter;
  }

  resetFilterMap() {
    this.applyFilterMap({});
  }

  applyFilterMap(terms: any) {
    this.filterObj = terms;
    this.lookup.postMapOrderFilter(terms)
      .subscribe(
        res => {
          this.addPinToMapOrders(res.data);
        },
        err => console.log(err)
      );
  }

  toggleFullScreen() {
    this.fullScreenMap = !this.fullScreenMap;
  }

  addPinToMapOrders(arr) {
    let orders = arr;
    for (let i = 0; i < orders.length; i++) {
      orders[i].pin = this.choosePin(orders[i].onHoldCount ,orders[i].typeName, orders[i].createdDate, orders[i].problemName);
      this.mapOrders.push(orders[i]);
    }
  }

}
