import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatcherContainerComponent } from './dispatcher-container.component';

describe('DispatcherContainerComponent', () => {
  let component: DispatcherContainerComponent;
  let fixture: ComponentFixture<DispatcherContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatcherContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatcherContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
