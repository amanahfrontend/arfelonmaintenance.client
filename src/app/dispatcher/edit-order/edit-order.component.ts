import { MapsAPILoader } from "@agm/core";
import { Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MessageService } from "primeng/primeng";
import { forkJoin } from "rxjs";
import { Subscription } from "rxjs/Subscription";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { LocationOnMapModalComponent } from "../location-on-map-modal/location-on-map-modal.component";
import { AuthService } from "../../shared/services/auth.service";

@Component({
  selector: "app-edit-order",
  templateUrl: "./edit-order.component.html",
  styleUrls: ["./edit-order.component.css"]
})
export class EditOrderComponent implements OnInit, OnDestroy {
  toggleLoading: boolean;
  orderDetails: any = {};
  activatedRouteSubscription: Subscription;
  allPriorities: any[];
  allStatus: any[];
  allCompanyCode: any[];
  allProblem: any[];
  allDivisions: any[];
  allTypes: any[];
  togglePaciLoading: boolean;
  showChangeSubStatusPopUp: boolean;
  subStatuses: any = [];
  previousStatusId: string;

  addMode: boolean = false;

  lat: number = 29.378586;
  lng: number = 47.990341;
  zoom: number = 8;

  // used to show a pop-up window containing the Google map to search for a location
  showMap: boolean = false;

  // search for location on Google maps
  public searchControl: FormControl;

  @ViewChild("search") searchElementRef: ElementRef;

  constructor(
    private messageService: MessageService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private lookupService: LookupService,
    private modelService: NgbModal,
    private mapsApiLoader: MapsAPILoader,
    private ngZone: NgZone,
    private authService: AuthService) {
  }

  ngOnInit() {
    this.getViewData();

    //create search FormControl
    this.searchControl = new FormControl();

    this.loadPlacesAutoComplete();
  }

  private loadPlacesAutoComplete() {
    // load Places Auto Complete
    this.mapsApiLoader.load().then(() => {

      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement, {
          types: ["address"]
        });

      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return "no such place";
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.orderDetails.lat = this.lat;
          this.orderDetails.long = this.lng;
          this.zoom = 15;
        });
      });
    });
  }

  private getViewData() {
    this.toggleLoading = true;
    let currentUserId = this.authService.currentUserId;

    this.activatedRouteSubscription = this.activatedRoute.params.subscribe(
      (params) => {
        forkJoin([
          this.getOrderDetails(+params.orderId),
          this.getPriority(),
          this.getStatus(),
          this.getCompanyCode(),
          this.getProblem(),
          this.getDivision(),
          this.getAllOrderTypes()
        ]).subscribe(
          (res) => {

            if (!isNaN(+params.orderId)) {
              // Edit order mode.
              //console.log(res[0]);

              this.previousStatusId = res[0].data.statusId;
              this.orderDetails = res[0].data;

              if (this.orderDetails.lat) {
                this.lat = this.orderDetails.lat;
              }

              if (this.orderDetails.long) {
                this.lng = this.orderDetails.long;
              }

              if (this.orderDetails.long && this.orderDetails.lat) {
                this.zoom = 15;
              }

              this.orderDetails.fK_UpdatedBy_Id = currentUserId;

            } else {
              // Create order mode.
              this.orderDetails = {};
              this.orderDetails.createdDate = new Date();
              this.orderDetails.fK_CreatedBy_Id = currentUserId;
            }

            this.orderDetails.currentUserId = currentUserId;

            this.allPriorities = res[1].data;
            this.allStatus = res[2].data;
            this.allCompanyCode = res[3].data;
            this.allProblem = res[4].data;
            this.allDivisions = res[5].data;
            this.allTypes = res[6].data;
            this.toggleLoading = false;
          },
          () => {
            this.toggleLoading = false;
          });
      });
  }

  onStatusChange(statusId) {
    if (statusId === 6 || statusId === 7) {
      this.toggleLoading = true;

      return this.lookupService.getAllSubOrderStatus(statusId).subscribe(
        (res) => {
          this.showChangeSubStatusPopUp = true;
          this.toggleLoading = false;
          this.subStatuses = res.data;
        });
    }
  }

  private getOrderDetails(orderId) {

    if (isNaN(orderId)) {
      this.addMode = true;
      orderId = 0;
    }

    return this.lookupService.getOrderDetails(orderId);
  }

  private getPriority() {
    return this.lookupService.getAllPriority();
  }

  private getStatus() {
    return this.lookupService.getAllStatus();
  }

  private getCompanyCode() {
    return this.lookupService.getCompanyCode();
  }

  private getProblem() {
    return this.lookupService.getAllProblems();
  }

  private getDivision() {
    return this.lookupService.getAllDivisions();
  }

  private getAllOrderTypes() {
    return this.lookupService.getAllOrderTypes();
  }

  getLoctionByPaci() {
    const paciCode = this.orderDetails.paci;

    if (paciCode.length === 8) {
      this.togglePaciLoading = true;

      this.lookupService.getLocationByPaci(this.orderDetails.paci).subscribe(
        (res) => {
          this.orderDetails.govName = res.governorate.name;
          this.orderDetails.govId = res.governorate.id;
          this.orderDetails.areaName = res.area.name;
          this.orderDetails.areaId = res.area.id;
          this.orderDetails.blockName = res.block.name;
          this.orderDetails.blockId = res.block.id;
          this.orderDetails.streetName = res.street.name;
          this.orderDetails.streetId = res.street.id;
          this.orderDetails.lat = res.lat;
          this.orderDetails.long = res.lng;
          this.togglePaciLoading = false;
        },
        () => {
          this.togglePaciLoading = false;
        });
    }
  }

  openMapModal() {
    let orderMapRef = this.modelService.open(LocationOnMapModalComponent);
    orderMapRef.componentInstance.order = this.orderDetails;
  }

  onOpenMap() {
    this.showMap = true;
  }

  closeModal() {
    this.showMap = false;
    this.toggleLoading = false;
  }

  private prepareOrderToPost() {

    if (this.orderDetails.customerCode == null) {
      this.showWarningMessage("Customer Code is required field.");
      return null;
    }

    if (this.orderDetails.customerName == null) {
      this.showWarningMessage("Customer Name is required field.");
      return null;
    }

    if (this.orderDetails.phoneOne == null) {
      this.showWarningMessage("Phone #1 is required field.");
      return null;
    }

    if (this.orderDetails.caller_ID == null) {
      this.showWarningMessage("Caller ID is required field.");
      return null;
    }

    if (this.orderDetails.code == null) {
      this.showWarningMessage("Order Code is required field.");
      return null;
    }

    if (this.orderDetails.priorityId == null) {
      this.showWarningMessage("Priority is required field.");
      return null;
    }

    if (this.orderDetails.typeId == null) {
      this.showWarningMessage("Type is required field.");
      return null;
    }

    if (this.orderDetails.statusId == null) {
      this.showWarningMessage("Status is required field.");
      return null;
    }

    if (this.orderDetails.companyCodeId == null) {
      this.showWarningMessage("Company Code is required field.");
      return null;
    }

    if (this.orderDetails.divisionId == null) {
      this.showWarningMessage("Division is required field.");
      return null;
    }

    const orderDataToPost = Object.assign({}, this.orderDetails);

    const statusId = parseInt(this.orderDetails.statusId);
    orderDataToPost.statusName = this.allStatus.find((element) => element.id === statusId).name;

    orderDataToPost.problemName = this.orderDetails.problemName;

    const typeId = parseInt(this.orderDetails.typeId);
    orderDataToPost.typeName = this.allTypes.find((element) => element.id === typeId).name;

    const priorityId = parseInt(this.orderDetails.priorityId);
    orderDataToPost.priorityName = this.allPriorities.find((element) => element.id === priorityId).name;

    const companyCodeId = parseInt(this.orderDetails.companyCodeId);
    orderDataToPost.companyCodeName = this.allCompanyCode.find((element) => element.id === companyCodeId).name;

    const divisionId = parseInt(this.orderDetails.divisionId);
    orderDataToPost.divisionName = this.allDivisions.find((element) => element.id === divisionId).name;

    console.log(orderDataToPost);

    return orderDataToPost;
  }

  ngOnDestroy() {
    this.activatedRouteSubscription.unsubscribe();
  }

  saveOrderEdits(goBackAfterSaving: boolean) {
    const preparedOrderToPost = this.prepareOrderToPost();

    if (preparedOrderToPost == null) {
      return;
    }

    this.toggleLoading = true;
    let addEditOrderRequest;

    if (this.addMode) {
      addEditOrderRequest = this.lookupService.createOrder(preparedOrderToPost);
    } else {

      addEditOrderRequest = this.lookupService.updateOrder(preparedOrderToPost);
    }

    addEditOrderRequest.subscribe(
      () => {
        this.showMap = false;
        this.toggleLoading = false;

        if (goBackAfterSaving) {
          this.router.navigate(["../../"]);

          this.messageService.add({
            severity: "success",
            summary: "Successful!",
            detail: `Order number ` + this.orderDetails.code + ` Edited & Saved Successfully!`
          });
        }
      });
  }

  showWarningMessage(message: string) {

    this.messageService.add({
      severity: "error",
      summary: "Warning!",
      detail: message
    });

  }

  onCancelChangeStatus() {
    this.orderDetails.statusId = this.previousStatusId;
    this.showChangeSubStatusPopUp = false;
  }

  onChangeStatus() {
    let objToPost = {
      orderId: this.orderDetails.id,
      statusId: this.orderDetails.statusId,
      subStatusId: this.orderDetails.subStatusId,
      serveyReport: this.orderDetails.serveyReport
    };

    this.lookupService.changeOrderStatus(objToPost).subscribe(
      () => {
        this.showChangeSubStatusPopUp = false;
        this.messageService.add({
          severity: "success",
          summary: "Successful!",
          detail: `Order number ` + this.orderDetails.code + ` Edited & Saved Successfully!`
        });
      });
  }

  onSaveChangeStatusAndGoBack() {

    let objToPost = {
      orderId: this.orderDetails.id,
      statusId: this.orderDetails.statusId,
      subStatusId: this.orderDetails.subStatusId,
      serveyReport: this.orderDetails.serveyReport
    };

    this.lookupService.changeOrderStatus(objToPost).subscribe(
      () => {
        this.showChangeSubStatusPopUp = false;
        this.router.navigate(["../../"]);
        this.messageService.add({
          severity: "success",
          summary: "Successful!",
          detail: `Order number ` + this.orderDetails.code + ` Edited & Saved Successfully!`
        });
      });
  }

  placeMarker(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
    this.orderDetails.lat = this.lat;
    this.orderDetails.long = this.lng;
  }

  get pageTitle() {
    return this.addMode ? "Create Order" : "Edit Order";
  }

  get isEditMode(): boolean {
    return !this.addMode;
  }
}
