import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderMapFilterComponent } from './order-map-filter.component';

describe('OrderMapFilterComponent', () => {
  let component: OrderMapFilterComponent;
  let fixture: ComponentFixture<OrderMapFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderMapFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderMapFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
