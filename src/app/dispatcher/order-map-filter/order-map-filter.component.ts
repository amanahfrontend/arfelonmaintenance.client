import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { AuthenticationServicesService } from '../../api-module/services/authentication/authentication-services.service';
import { Angular2Csv } from 'angular2-csv';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-order-map-filter',
  templateUrl: './order-map-filter.component.html',
  styleUrls: ['./order-map-filter.component.css']
})
export class OrderMapFilterComponent implements OnInit {
  filter: any = {};
  allAreas: any[] = [];
  allProblems: any[] = [];
  allDivisions: any[] = [];
  allDispatchers: any[] = [];
  allTechnicians: any[] = [];
  allOrderTypes: any[] = [];

  areasObservable: Subscription;
  problemsObservable: Subscription;
  divisionsObservable: Subscription;
  dispatchersObservable: Subscription;
  techniciansObservable: Subscription;
  OrderTypesObservable: Subscription;
  
  allStatuses: any = [
    { name: 'Open' },
    { name: 'Dispatched' },
    { name: 'On-Travel' },
    { name: 'Reached' },
    { name: 'Started' },
    { name: 'On-Hold' }
  ];
  reportOrders: any[] = [];

  @Input() type: string;
  @Output() todayOrders = new EventEmitter;
  @Output() filteredOrders = new EventEmitter;
  @Output() resetFilter = new EventEmitter;
  @Output() orderProgressMode = new EventEmitter;
  @Output() filterTerms = new EventEmitter;

  timeObj = {
    dateFrom: '',
    dateTo: '',
    costCenter: '',
    pfNo: '',
  };

  @ViewChild('filterForm') filterForm: NgForm;

  constructor(private lookup: LookupService,
    private authService: AuthenticationServicesService) {
  }

  ngOnInit() {
    this.filter = {
      fromWhere: 3
    };
    this.getAllAreas();
    this.getAllProblems();
    this.getAllDevisions();
    this.getAllDispatchers();
    this.getAllTechnicians();
    this.getAllOrderTypes();
  }

  ngOnDestroy() {
    this.areasObservable && this.areasObservable.unsubscribe();
    this.problemsObservable && this.problemsObservable.unsubscribe();
    this.divisionsObservable && this.divisionsObservable.unsubscribe();
    this.dispatchersObservable && this.dispatchersObservable.unsubscribe();
    this.techniciansObservable && this.techniciansObservable.unsubscribe();
  }

  applyFilter() {
    // let dispatcherId = this.authService.CurrentUser().id;
    // console.log(this.filter);
    
    let filterAreas = [];
    let filterProblems = [];
    let filterStatus = [];
    let filterDivisions = [];
    let filterDispatchers = [];
    let filterTechnicians = [];
    let filterOrderTypes = [];
    this.filter.areas && this.filter.areas.map((area) => {
      filterAreas.push(area.id);
    });
    this.filter.problems && this.filter.problems.map((problem) => {
      filterProblems.push(problem.id);
    });
    this.filter.status && this.filter.status.map((status) => {
      filterStatus.push(status.id);
    });
    this.filter.divisions && this.filter.divisions.map((division) => {
      filterDivisions.push(division.id);
    });
    this.filter.dispatchers && this.filter.dispatchers.map((division) => {
      filterDispatchers.push(division.id);
    });
    this.filter.Technicians && this.filter.technicians.map((technician) => {
      filterTechnicians.push(technician.id);
    });
    this.filter.OrderTypes && this.filter.OrderTypes.map((orderType) => {
      filterOrderTypes.push(orderType.id);
    });

    let filterObjectToPost = {
      dateFrom: this.timeObj.dateFrom,
      dateTo: this.timeObj.dateTo,
      status: filterStatus,
      problemId: filterProblems,
      divisionId: filterDivisions,
      dispatcherId: filterDispatchers,
      technicianId: filterTechnicians,
      areaId: filterAreas,
      orderTypeId: filterOrderTypes,
      fromWhere: this.filter.fromWhere
    };
    console.log(filterObjectToPost);
    for (let property in filterObjectToPost) {
      if (filterObjectToPost.hasOwnProperty(property)) {
        if (Array.isArray(filterObjectToPost[property])) {
          if (!filterObjectToPost[property].length) {
            delete filterObjectToPost[property];
          }
        } else {
          if (!filterObjectToPost[property]) {
            delete filterObjectToPost[property];
          }
        }
      }
    }
    if (this.type == 'order board' || this.type == 'map') {
      this.filterTerms.emit(filterObjectToPost);
    } else {
      this.postFilterProperties(filterObjectToPost);
    }
  }

  postFilterProperties(filterObjectToPost) {
    if (this.type == 'report') {
      this.lookup.filterOrders(filterObjectToPost).subscribe((res) => {
        console.log(res);
        this.orderProgressMode.emit(this.filter.hasOrderProgress);
        this.reportOrders = res;
        this.filteredOrders.emit(res);
        console.log(this.filter.hasOrderProgress);

        // this.todayOrdersStatus = false;
      },
        err => {

        });
    } else {
      this.lookup.filterOrdersMap(filterObjectToPost).subscribe((res) => {
        console.log(res);
        this.filteredOrders.emit(res);
        console.log(this.filter.hasOrderProgress);
        this.orderProgressMode.emit(this.filter.hasOrderProgress);
        // this.todayOrdersStatus = false;
      },
        err => {

        });
    }
  }

  emitTodayOrders() {
    this.filter = {};
    this.todayOrders.emit();
  }

  checkFilter() {
    let checkingObject = Object.assign({}, this.filter);
    delete checkingObject.fromWhere;
    if (JSON.stringify(checkingObject) == JSON.stringify({})) {
      // console.log('will return true because filter is clear');
      return true;
    } else {
      for (let property in checkingObject) {
        // console.log(property);
        if (checkingObject.hasOwnProperty(property)) {
          // console.log('will return false');
          if (Array.isArray(checkingObject[property])) {
            if (checkingObject[property].length) {
              return false;
            }
          } else {
            if (checkingObject[property]) {
              return false;
            }
          }
        }
      }
      return true;
    }
  }

  applyResetFilter() {
    this.filterForm.resetForm();
    this.filter = {
      fromWhere: 3
    };
    this.reportOrders = [];
    this.resetFilter.emit();
  }

  getAllProblems() {
    this.problemsObservable = this.lookup.getAllProblems().subscribe((problems) => {
      this.allProblems = problems.data;
    },
      err => {
      });
  }

  getAllAreas() {
    this.areasObservable = this.lookup.getAllAreas()
    .subscribe(
      res => this.allAreas = res.data,
      err => console.log(err) 
    );
  }

  getAllDevisions() {
    this.divisionsObservable = this.lookup.getAllDivisions()
    .subscribe(
      res => this.allDivisions = res.data,
      err => console.log(err)
    );
  }

  getAllDispatchers() {
    this.dispatchersObservable = this.lookup.getAllDispatchers()
    .subscribe(
      res => this.allDispatchers = res.data,
      err => console.log(err)
    );
  }

  getAllTechnicians() {
    this.techniciansObservable = this.lookup.getAllTechnicians()
    .subscribe(
      res => this.allTechnicians = res.data,
      err => console.log(err)
    );
  }

  getAllOrderTypes() {
    this.OrderTypesObservable = this.lookup.getAllOrderTypes()
    .subscribe(
      res => this.allOrderTypes = res.data,
      err => console.log(err)
    );
  }

}
