import { DispatcherOrderBoardComponent } from './dispatcher-order-board/dispatcher-order-board.component';
import { AuthGuardGuard } from '../api-module/guards/auth-guard.guard';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { DispatcherContainerComponent } from './dispatcher-container/dispatcher-container.component';
import { NotificationListComponent } from '../shared-module/shared/notification-list/notification-list.component';
import { OrdersMapComponent } from './orders-map/orders-map.component';
import { AssignedOrdersBoardComponent } from './assigned-orders-board/assigned-orders-board.component';
import { Routes } from '@angular/router';
import { PrintoutComponent } from './printout/printout.component';

export const dispatcherRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardGuard],
    data: { roles: ['Dispatcher'] },
    component: DispatcherContainerComponent,
    children: [
      {
        path: '',
        component: DispatcherOrderBoardComponent,
        redirectTo: 'board',
        pathMatch: 'full'
      },
      {
        path: 'board',
        component: AssignedOrdersBoardComponent
      },
      {
        path: "notifications",
        component: NotificationListComponent
      },
      {
        path: 'map',
        component: OrdersMapComponent
      },
      {
        path: 'board/order/create',
        component: EditOrderComponent,
        canActivate: [AuthGuardGuard],
        data: { roles: ['Dispatcher'] }
      },
      {
        path: 'board/edit-order/:orderId',
        component: EditOrderComponent,
        canActivate: [AuthGuardGuard],
        data: { roles: ['Dispatcher'] }
      },
      {
        path: 'map/edit-order/:orderId',
        component: EditOrderComponent,
        canActivate: [AuthGuardGuard],
        data: { roles: ['Dispatcher'] }
      },
      {
        path: 'printout',
        component: PrintoutComponent,
        canActivate: [AuthGuardGuard],
        data: { roles: ['Dispatcher'] }
      }
    ]
  }
];
