import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { UtilitiesService } from '../../api-module/services/utilities/utilities.service';
import { DragulaService } from 'ng2-dragula';
import { NotificationsService } from '../notifications.service';

@Component({
  selector: 'app-supervisor-board',
  templateUrl: './supervisor-board.component.html',
  styleUrls: ['./supervisor-board.component.css'],
  providers: [DragulaService]
})
export class SupervisorBoardComponent implements OnInit {
  toggleLoading: boolean;
  showFilter: boolean;
  fetchingDataDone: boolean;
  isBulkAssign: boolean;
  cornerMessage: any[];
  toggleOverflowX: string[];
  allUnassignedOrders = [];
  disabledStatus: string[];
  orders: any[];
  dispatchers: any = [];

  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private dragulaService: DragulaService,
    private notificationsService: NotificationsService) { }

  ngOnInit() {
    this.getSupervisorUnassignedOrders();
    this.getDispatchersUnassignedOrders();

    // Mohammed Osman: Disable firebase. {Start}
    // this.notificationsService.getPermission();
    // this._receiveNotification();
    // Mohammed Osman: Disable firebase. {End}

    this.dragulaService.drop.subscribe(
      (eventData) => {
        this.postAssignOrUnAssignOrders(eventData);
      });

    // this.dragulaService.setOptions('orders', {
    //   moves: (el)=> !el.classList.contains('disable-drag')
    // });
  }


  getSupervisorUnassignedOrders() {
    let supervisorId = localStorage.getItem('AlghanimCurrentUserId');
    this.lookup.getAllUnAssignedOrdersForSupervisor(supervisorId).subscribe((supervisor) => {

      this.orders = supervisor.data;
      this.orders.map((order) => {
        order.classes = ['#orderId' + order.id];
        // if (!order.lat || !order.long) {
        //   order.classes.push('disable-drag');
        // }
        order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
        order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
      });
      this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      });
  }

  getDispatchersUnassignedOrders() {
    let supervisorId = localStorage.getItem('AlghanimCurrentUserId');
    this.fetchingDataDone = false;
    this.lookup.getAllAssignedOrdersForSupervisor(supervisorId).subscribe((response) => {

      this.fetchingDataDone = true;
      if (response.data) {
        this.dispatchers = response.data;
        this.dispatchers.map((dis) => {
          dis.dispatcherName = dis.dispatcherId;
          if (dis.orders) {
            dis.orders.map((order) => {
              order.classes = ['#orderId' + order.id];
              order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
              order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
            });
            dis.filterOrders = dis.orders.slice();
          } else {
            dis.filterOrders = [];
            dis.orders = [];
          }
        });
      }
      console.log("dispatchers", this.dispatchers);
    },
      err => {

      });
  }

  extractIdFromDragulaElement(classList, propertyName) {
    let id;
    classList.forEach((className) => {
      if (className.includes(propertyName)) {
        id = className.replace(propertyName, '');
      }
    });
    return id;
  }

  postAssignOrUnAssignOrders(dropEventData) {
    console.log(dropEventData);
    if (dropEventData[0] === 'orders') {
      let columnMovedTo = dropEventData[2].classList[0];
      let columnMovedFrom = dropEventData[3].classList[0];
      if (columnMovedFrom === 'unassigned-orders-container') {
        let orderId = +this.extractIdFromDragulaElement(dropEventData[1].classList, '#orderId');
        let dispatcherId = +this.extractIdFromDragulaElement(dropEventData[2].classList, '#dispatcherId') || 0;
        let assignOrderObj = { orderId, dispatcherId };
        this.postAssignOrders(assignOrderObj);
      } else if (columnMovedTo === 'unassigned-orders-container') {
        let orderId = +this.extractIdFromDragulaElement(dropEventData[1].classList, '#orderId');
        const unAssignOrderObj = { orderId, dispatcherId: 0 };
        this.postUnAssignOrders(unAssignOrderObj);
      } else {
        let orderId = +this.extractIdFromDragulaElement(dropEventData[1].classList, '#orderId');
        let dispatcherId = +this.extractIdFromDragulaElement(dropEventData[2].classList, '#dispatcherId') || 0;
        let assignOrderObj = { orderId, dispatcherId };
        this.postAssignOrders(assignOrderObj);
      }
    }
  }

  postUnAssignOrders(unAssignOrderObj) {
    this.toggleLoading = true;
    this.lookup.unAssginOrderFromDispatcher(unAssignOrderObj).subscribe(
      (res) => {
        this.toggleLoading = false;
      });
  }

  postAssignOrders(assignOrderObj) {
    this.toggleLoading = true;
    this.lookup.assginOrderToDispatcher(assignOrderObj)
      .subscribe(
        (res) => {
          this.toggleLoading = false;
        });
  }

  private _receiveNotification() {
    this.notificationsService.receiveMessage()
      .subscribe((msg: any) => {
        this.utilities.setGrowlMessage({
          severity: 'success',
          summary: 'Message',
          detail: msg.notification.body
        });
        const order = JSON.parse(msg.data.order);
        this._searchAndUpdateOrAddOrder(order);
      });
  }

  private _searchAndUpdateOrAddOrder(order) {
    // this.dispatchers.map((dis: any) => {
    //   dis.filterOrders.forEach((singleOrder, i) => {
    //       if (singleOrder.id == order.id) {
    //         if (order.statusName == "Complete" || order.statusName == "Canceled") {
    //           dis.filterOrders.splice(i, 1);
    //         } else if (order.statusName == "On-Hold") {
    //           dis.filterOrders.splice(i, 1);
    //         } else if (order.acceptanceFlag == 3) {
    //           dis.filterOrders.splice(i, 1);
    //         } else {
    //           dis.filterOrders[i] = order;
    //         }
    //       }
    //     });
    //   });
    // if (order.statusName == 'On-Hold' || order.statusName == 'Open') {
    //   this.orders.push(order);
    // }
  }

}
