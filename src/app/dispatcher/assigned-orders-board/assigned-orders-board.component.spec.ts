import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignedOrdersBoardComponent } from './assigned-orders-board.component';

describe('AssignedOrdersBoardComponent', () => {
  let component: AssignedOrdersBoardComponent;
  let fixture: ComponentFixture<AssignedOrdersBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignedOrdersBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedOrdersBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
