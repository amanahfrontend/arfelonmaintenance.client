import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { UtilitiesService } from '../../api-module/services/utilities/utilities.service';
import { DragulaService } from 'ng2-dragula';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from '../notifications.service';
import { Subscription } from 'rxjs';
import { SharedNotificationService } from '../../shared-module/shared/shared-notification.service';
import { MessageService } from 'primeng/primeng';

@Component({
  selector: 'app-assigned-orders-board',
  templateUrl: './assigned-orders-board.component.html',
  styleUrls: ['./assigned-orders-board.component.css'],
  providers: [DragulaService]
})
export class AssignedOrdersBoardComponent implements OnInit {
  toggleLoading: boolean;
  showFilter: boolean;
  fetchingDataDone: boolean;
  isBulkAssign: boolean;
  cornerMessage: any[];
  toggleOverflowX: string[];
  allUnassignedOrders = [];
  disabledStatus: string[];
  orders: any[];
  foremen: any = [];
  dispatcherOrdersSubscription: Subscription;
  getTeamByDispatcherByIdSubscription: Subscription;

  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private dragulaService: DragulaService,
    private modelService: NgbModal,
    private notificationsService: NotificationsService,
    private sharedNotificationService: SharedNotificationService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.disabledStatus = ['On-Travel', 'Reached', 'Started'];
    this.getDispatcherUnassignedOrders();
    this.getTeamsAndOrders();

    // Mohammed Osman: Disable firebase. {Start}
    // this.notificationsService.getPermission();
    // this._receiveNotification();
    // Mohammed Osman: Disable firebase. {End}

    // this.dragulaService.setOptions('orders', {
    //   revertOnSpill: true,
    //   invalid : (el)=> el.classList.contains('disable-drag')
    // });

    this.dragulaService.drop.subscribe(
      (eventData) => {
        this.postAssignOrUnAssignOrders(eventData);
      });

    this.onRecieveChangeRank();
    // this.dragulaService.drag.subscribe((eventData) => {
    //   if(eventData[1].classList.contains('disable-drag')){
    //     let link = document.createElement("p");
    //     link.focus();
    //     // throw "disabled order"
    //   }
    // });
    // this.dragulaService.setOptions('orders', {
    //   invalid: (el)=> false
    // })

    this.dragulaService.setOptions('orders', {
      moves: (el) => {
        if (!el.classList.contains('disable-drag') && !el.classList.contains('no-coordinates')) {
          return true;
        } else if (el.classList.contains('no-coordinates')) {
          this.messageService.add({
            severity: 'error',
            summary: "can't be dispatched",
            detail: "No coordinates available"
          });
          return false;
        } else {
          return false;
        }
      }
    });
    // this.dragulaService.setOptions('orders', {

    //   moves: function (el: any, container: any, handle: any): any {
    //     if (el.classList.contains('disable-drag')) {
    //       return false;
    //     }
    //     return true;
    //   }

    // });
    // accepts: (el, target, source, sibling) => {
    //   if (sibling === null) {
    //     return false;
    //   }

    // }

    // moves : (el)=> !el.classList.contains('bordered')
    // moves: (el)=> !el.classList.contains('bordered')
    //   function (order: any, container: any, handle: any): any {
    //     if (order.classList.contains('disable-drag')) {
    //       return false;
    //     } else {
    //       return true;
    //     }
    //   }
  }


  // onDrag(e:any){

  //   e.preventDefault();
  // }

  onRecieveChangeRank() {
    this.sharedNotificationService.acceptChangeRankSubject
      .subscribe(() => this.getTeamsAndOrders());
  }


  getDispatcherUnassignedOrders() {
    let dispatcherId = localStorage.getItem('AlghanimCurrentUserId');
    this.dispatcherOrdersSubscription = this.lookup.getAllUnAssignedOrdersForDispatcher(dispatcherId).subscribe((dispatcher) => {
      this.orders = dispatcher.data;
      // this.allUnassignedOrders = dispatcher.data;
      this.orders.map((order) => {
        order.classes = ['#orderId' + order.id];
        if (!order.lat || !order.long) {
          // if(!order.classes){
          //   order.classes = [];
          // }
          order.classes.push('no-coordinates');
        }
        order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
        order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
      });
      this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      });
  }

  getTeamsAndOrders() {
    let dispatcherId = localStorage.getItem('AlghanimCurrentUserId');
    this.fetchingDataDone = false;
    this.getTeamByDispatcherByIdSubscription = this.lookup.getAllAssignedOrdersForDispatcher(dispatcherId).subscribe(
      (response) => {
        this.fetchingDataDone = true;
        if (response.data) {
          this.foremen = response.data;
          this.foremen.map((forman) => {
            forman.teams.map((team: any) => {
              if (team.orders) {
                team.orders.map((order) => {
                  order.classes = ['#orderId' + order.id];
                  if (this.disabledStatus.includes(order.statusName)) {
                    order.classes.push('disable-drag');
                  }
                  order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                  order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                  // this.orderClassNaming(order);
                });
                team.filterOrders = team.orders.slice();
              } else {
                team.filterOrders = [];
                team.orders = [];
              }
            });

          });
        }

      },
      err => {

      });
  }

  showHideFilter() {
    this.showFilter = !this.showFilter;
  }

  applyFilterBoardTemplate(terms: any) {
    let emptyFilterObj: any = {
      dispatcherId: localStorage.getItem("AlghanimCurrentUserId")
    };
    terms.dispatcherId = localStorage.getItem("AlghanimCurrentUserId");
    if (terms.fromWhere == 3) {
      // terms.fromWhere = 1;
      this.lookup.postBoardOrderFilter(terms)
        .subscribe(
          (res) => {
            this.foremen = res.data;
            this.foremen.map((forman) => {
              forman.teams.map((team: any) => {
                if (team.orders) {
                  team.orders.map((order) => {
                    order.classes = ['#orderId' + order.id];
                    if (this.disabledStatus.includes(order.statusName)) {
                      order.classes.push('disable-drag');
                    }
                    order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                    order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                  });
                  team.filterOrders = team.orders.slice();
                } else {
                  team.filterOrders = [];
                  team.orders = [];
                }
              });

            });
          },
          err => console.log(err)
        );
      // terms.fromWhere = 2;
      this.lookup.postBoardUnAssignedOrderFilter(terms).subscribe((response) => {
        this.orders = response.data,
          err => console.log(err);
      });
    } else if (terms.fromWhere == 2) {
      // emptyFilterObj.fromWhere = 1;
      this.lookup.postBoardOrderFilter(terms)
        .subscribe(
          (res) => {
            this.foremen = res.data;
            this.foremen.map((forman) => {
              forman.teams.map((team: any) => {
                if (team.orders) {
                  team.orders.map((order) => {
                    order.classes = ['#orderId' + order.id];
                    if (this.disabledStatus.includes(order.statusName)) {
                      order.classes.push('disable-drag');
                    }
                    order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                    order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                  });
                  team.filterOrders = team.orders.slice();
                } else {
                  team.filterOrders = [];
                  team.orders = [];
                }
              });
            });
          },
          err => console.log(err)
        );
      // terms.fromWhere = 2;
      this.lookup.postBoardUnAssignedOrderFilter(emptyFilterObj).subscribe((response) => {
        this.orders = response.data,
          err => console.log(err);
      });
    } else if (terms.fromWhere == 1) {
      // terms.fromWhere = 1;
      this.lookup.postBoardOrderFilter(emptyFilterObj)
        .subscribe(
          (res) => {
            this.foremen = res.data;
            this.foremen.map((forman) => {
              forman.teams.map((team: any) => {
                if (team.orders) {
                  team.orders.map((order) => {
                    order.classes = ['#orderId' + order.id];
                    if (this.disabledStatus.includes(order.statusName)) {
                      order.classes.push('disable-drag');
                    }
                    order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                    order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                    // this.orderClassNaming(order);
                  });
                  team.filterOrders = team.orders.slice();
                } else {
                  team.filterOrders = [];
                  team.orders = [];
                }
              });
            });
          },
          err => console.log(err)
        );
      // emptyFilterObj.fromWhere = 2;
      this.lookup.postBoardUnAssignedOrderFilter(terms).subscribe((response) => {
        this.orders = response.data,
          err => console.log(err);
      });
    }
    this.showHideFilter();
  }

  resetBoardFilter() {
    let terms: any = {};
    terms.fromWhere = 3;
    this.applyFilterBoardTemplate(terms);
  }

  extractIdFromDragulaElement(classList, propertyName) {
    let id;
    classList.forEach((className) => {
      if (className.includes(propertyName)) {
        id = className.replace(propertyName, '');
      }
    });
    return id;
  }

  postAssignOrUnAssignOrders(dropEventData) {
    console.log(dropEventData);
    if (dropEventData[0] === 'orders') {
      // this.toggleLoading = true;
      let columnMovedTo = dropEventData[2].classList[0];
      let columnMovedFrom = dropEventData[3].classList[0];
      if (columnMovedTo === columnMovedFrom) { // item is moved in the same column
        this.onChangeRank(dropEventData[2]);
      } else {
        // on moving order from unassigned to assigned
        if (columnMovedFrom === 'unassigned-orders-container') { // item is moved from unassigned to assigned

          let orderId = +this.extractIdFromDragulaElement(dropEventData[1].classList, '#orderId');
          let teamId = +this.extractIdFromDragulaElement(dropEventData[2].classList, '#teamId') || 0;
          let assignOrderObj = { orderId, teamId };
          this.postAssignOrders(assignOrderObj, dropEventData[2]);
        } else if (columnMovedTo === 'unassigned-orders-container') { // on moving order from assigned to unassigned
          let orderId = +this.extractIdFromDragulaElement(dropEventData[1].classList, '#orderId');
          const unAssignOrderObj = { orderId };
          this.postUnAssignOrders(unAssignOrderObj);
        } else {
          let orderId = +this.extractIdFromDragulaElement(dropEventData[1].classList, '#orderId');
          let teamId = +this.extractIdFromDragulaElement(dropEventData[2].classList, '#teamId') || 0;
          let assignOrderObj = { orderId, teamId };
          this.postAssignOrders(assignOrderObj, dropEventData[2]);
        }
      }
    }
    // const item = dropEventData[1];
    // const target = dropEventData[2];
    // const teamId = +this.extractIdFromDragulaElement(target.classList, '#teamId') || 0;
    // const orderId = +this.extractIdFromDragulaElement(item.classList, '#orderId');
    // const sourceId = +this.extractIdFromDragulaElement(dropEventData[3].classList, '#teamId') || 0;
    // if (teamId != sourceId) {
    //   if (teamId) {
    //     const assignOrderObj = {
    //       orderId,
    //       teamId
    //     };
    //     this.postAssignOrders(assignOrderObj);
    //   } else {
    //     const unAssignOrderObj = {
    //       orderId
    //     };
    //     this.postUnAssignOrders(unAssignOrderObj);
    //   }
    // }
  }

  onChangeRank(columnMovedToObject) {
    this.toggleLoading = true;
    let columnChildren = {};
    for (let i = 0; i < columnMovedToObject.children.length; i++) {
      columnChildren[+this.extractIdFromDragulaElement(columnMovedToObject.children[i].classList, '#orderId')] = i;
      if (i === (columnMovedToObject.children.length - 1)) {
        let objectToPost = {
          teamId: +this.extractIdFromDragulaElement(columnMovedToObject.classList, '#teamId') || 0,
          orders: columnChildren
        };
        this.lookup.rankTeamOrders(objectToPost).subscribe(
          res => {
            console.log(res);
            this.toggleLoading = false;
            this.getDispatcherUnassignedOrders();
            this.getTeamsAndOrders();
          }
        );
      }
    }
  }

  postUnAssignOrders(unAssignOrderObj) {
    this.toggleLoading = true;
    this.lookup.unAssginOrder(unAssignOrderObj).subscribe(
      (res) => {
        this.toggleLoading = false;
      });
  }

  postAssignOrders(assignOrderObj, objectForChangeRank) {
    this.toggleLoading = true;
    this.lookup.assginOrder(assignOrderObj)
      .subscribe(
        (res) => {
          this.onChangeRank(objectForChangeRank);
        });
  }

  private _receiveNotification() {
    this.notificationsService.receiveMessage()
      .subscribe((msg: any) => {

        this.utilities.setGrowlMessage({
          severity: 'success',
          summary: 'Message',
          detail: msg.notification.body
        });
        const order = JSON.parse(msg.data.order);
        this._searchAndUpdateOrAddOrder(order);
      });
  }

  private _searchAndUpdateOrAddOrder(order) {

    this.foremen.map((foreman: any) => {
      foreman.teams.forEach((team: any) => {
        team.filterOrders.forEach((singleOrder, i) => {
          if (singleOrder.id == order.id) {

            if (order.statusName == "Complete" || order.statusName == "Canceled") {
              team.filterOrders.splice(i, 1);
            } else if (order.statusName == "On-Hold") {
              team.filterOrders.splice(i, 1);
            } else if (order.acceptanceFlag == 3) {
              team.filterOrders.splice(i, 1);
            } else {
              team.filterOrders[i] = order;
            }
          }
        });
      });
    });
    if (order.statusName == 'On-Hold' || order.statusName == 'Open') {
      this.orders.push(order);
    }
  }

}
