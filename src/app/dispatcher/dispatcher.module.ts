import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { dispatcherRoutes } from './dispatcher-routes'
import { SetTimeModalComponent } from './set-time-modal/set-time-modal.component';
import { TransferDispatcherModalComponent } from './transfer-dispatcher-modal/transfer-dispatcher-modal.component';
import { OrderProgressModalComponent } from './order-progress-modal/order-progress-modal.component';
import { OrderDetailsModalComponent } from './order-details-modal/order-details-modal.component';
import { BulkAssignModalComponent } from './bulk-assign-modal/bulk-assign-modal.component';
import { PreventiveComponent } from './preventive/preventive/preventive.component';
import { PreventiveDetailsModalComponent } from './preventive/preventive-details-modal/preventive-details-modal.component';
import { NotificationsService } from './notifications.service';
import { LocationOnMapModalComponent } from './location-on-map-modal/location-on-map-modal.component';
import { DispatchingModule } from './dispatching.module';

@NgModule({
  imports: [
    DispatchingModule,
    RouterModule.forChild(dispatcherRoutes)
  ], entryComponents: [
    SetTimeModalComponent,
    TransferDispatcherModalComponent,
    OrderProgressModalComponent,
    OrderDetailsModalComponent,
    BulkAssignModalComponent,
    PreventiveComponent,
    PreventiveDetailsModalComponent,
    LocationOnMapModalComponent
  ],
  providers: [
    NotificationsService
  ]
})

export class DispatcherModule {
}
