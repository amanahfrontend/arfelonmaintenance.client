import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatcherOorderBoardComponent } from './dispatcher-oorder-board.component';

describe('DispatcherOorderBoardComponent', () => {
  let component: DispatcherOorderBoardComponent;
  let fixture: ComponentFixture<DispatcherOorderBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatcherOorderBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatcherOorderBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
