import { Component, OnInit, OnDestroy } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { UtilitiesService } from '../../api-module/services/utilities/utilities.service';
import { Subscription } from 'rxjs';
import { DragulaService } from 'ng2-dragula';
import { AuthenticationServicesService } from '../../api-module/services/authentication/authentication-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditOrderModalComponent } from '../../customer/edit-order-modal/edit-order-modal.component';
import { NotificationsService } from '../notifications.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-dispatcher-oorder-board',
  templateUrl: './dispatcher-oorder-board.component.html',
  styleUrls: ['./dispatcher-oorder-board.component.css']
})
export class DispatcherOorderBoardComponent implements OnInit, OnDestroy {
  foremen: any = [];
  toggleLoading: boolean;
  orders: any[];
  openOrders: any[];
  onholdOrders: any[];
  orderMap: any[] = [];
  orderMapFiltered: any[] = [];
  fetchingDataDone: boolean;
  // teamsWithOrders: any[];
  cornerMessage: any[];
  selectedOrders: any[] = [];
  dispatcherOrdersSubscription: Subscription;
  getTeamByDispatcherByIdSubscription: Subscription;
  orderStatusObservable: Subscription;
  allVehiclesSubscription: Subscription;
  activatedRouteSubscription: Subscription;
  toggleMap: boolean;
  isBulkAssign: boolean;
  toggleOverflowX: string[];
  lat: number = 29.378586;
  lng: number = 47.990341;
  zoom: number = 10;
  display: boolean;
  allVehicles: any[] = [];
  availabilityButtons: any[] = [];
  filterStatusClass: string[] = [];
  todayOrdersStatus: boolean;
  vehiclesStatus: boolean;
  currentActiveTab: number;
  notificationSubscription: Subscription;
  orderTypes = [
    {
      label: 'Open',
      value: 'Open',
    },
    {
      label: 'On Hold',
      value: 'On-Hold',
    }
  ];
  selectedOrderType: string = 'Open';
  isFilterActive = false;
  showFilter = false;
  showMapFilter = false;
  disabledStatus: string[] = ['On-Travel', 'Reached', 'Started'];
  fullScreenMap: boolean = false;

  allUnassignedOrders: any = [];
  allAssignedOrders: any = [];
  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private dragulaService: DragulaService,
    private authService: AuthenticationServicesService,
    private modelService: NgbModal,
    private notificationsService: NotificationsService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.currentActiveTab = 1;
    this.availabilityButtons = [
      { label: 'Yes', value: true },
      { label: 'No', value: false }
    ];
    this.getDispatcherOrders();
    // this.getDisAllOrders();
    this.getTeamsAndOrders();
    this.subscribeToActivatedRoute();
    // this.getAllVehicles();
    this.display = true;
    this.todayOrdersStatus = true;
    this.vehiclesStatus = true;
    this.utilities.toggleFullWidth = true;
    this.filterStatusClass = ['zero-filter-width'];
    // this.hubInit();
    this.dragulaService.setOptions('orders', {
      removeOnSpill: false,
      copySortSource: false,
      copy: false,
      revertOnSpill: true,
      moves: (el) => !el.classList.contains('disable-drag')
    });

    this.dragulaService.dropModel.subscribe((eventData) => {
      console.log(eventData);

      // if (this._isRankingProcess(eventData)) {
      // const isAllowedRanking: any
      // = this._isAllowedRanking(eventData);

      // if (isAllowedRanking.isAllowed) {
      //   this._postTeamOrdersRank(isAllowedRanking.targetTeamId);
      // } else {
      //   this.foremen.map((foreman: any) => {
      //     for (let i = 0, l = foreman.teams.length; i < l; i++) {
      //       if (foreman.teams[i].teamId == isAllowedRanking.targetTeamId) {
      //         
      //         setTimeout(() => {
      //           foreman.teams[i].filterOrders = [...foreman.teams[i].orders];
      //         }, 0);
      //         break;
      //       }
      //     }

      //   })
      //   this.utilities.setGrowlMessage({
      //     severity: 'error',
      //     detail: 'This Order Can\'t ranked as first order',
      //     summary: 'Not Allowed'
      //   });
      // }
      // } else {

      this.postAssignOrUnAssignOrders(eventData);
      // }
      // });

    });

    // Mohammed Osman: Disable firebase. {Start}
    // this.notificationsService.getPermission();
    // this._receiveNotification();
    // Mohammed Osman: Disable firebase. {End}
  }

  private _isAllowedRanking(dropEventData) {
    let isAllowed;
    const targetTeamId = +this.extractIdFromDragulaElement(dropEventData[2].classList, '#teamId'),
      sourceTeamId = +this.extractIdFromDragulaElement(dropEventData[3].classList, '#teamId');
    if (targetTeamId && sourceTeamId && (targetTeamId == sourceTeamId)) {
      const teamObj = this.foremen.map((foreman: any) => {
        foreman.teams.find((team: any) => team.teamId == targetTeamId);
      });
      let orderWithDisabledDraggingState;
      for (let i = 0, l = teamObj.filterOrders.length; i < l; i++) {
        if (this.disabledStatus.includes(teamObj.filterOrders[i].statusName)) {
          orderWithDisabledDraggingState = teamObj.filterOrders[i];
          console.log(teamObj.filterOrders.indexOf(teamObj.filterOrders[i]));

          if (teamObj.filterOrders.indexOf(teamObj.filterOrders[i]) == 0) {
            isAllowed = true;
            break;
          }
        }
        if (!orderWithDisabledDraggingState && i == l - 1) {
          isAllowed = true;
        }
      }
    }
    return {
      isAllowed,
      targetTeamId
    };
  }

  subscribeToActivatedRoute() {
    this.activatedRouteSubscription = this.activatedRoute.params.subscribe((params) => {
      this.toggleMap = !!+params.showMap;
    });
  }

  private _postTeamOrdersRank(teamId) {
    const rankObjectToPost = this._prepareTeamOrdersRankDataToPost(teamId);
    if (rankObjectToPost) {
      this.toggleLoading = true;
      this.lookup.rankTeamOrders(rankObjectToPost)
        .subscribe((res) => {
          this.toggleLoading = false;
          if (res.isSucceeded) {
            this.utilities.setGrowlMessage({
              severity: 'success',
              detail: 'Orders Rank Updated Successfully',
              summary: 'Successfully'
            });
          } else {
            this.foremen.map((foreman: any) => {
              foreman.teams.forEach((team: any) => {
                if (team.teamId == rankObjectToPost.teamId) {
                  team.filterOrders = team.orders.slice();
                }
              });
            });
            this.utilities.setGrowlMessage({
              severity: 'error',
              detail: 'Orders Rank Failed to Update',
              summary: 'Failed'
            });
          }
        }, () => {
          this.toggleLoading = false;
          this.foremen.map((foreman: any) => {
            foreman.teams.forEach((team: any) => {
              if (team.teamId == rankObjectToPost.teamId) {
                team.filterOrders = team.orders.slice();
              }
            });
          });
          this.utilities.setGrowlMessage({
            severity: 'error',
            detail: 'Orders Rank Failed to Update',
            summary: 'Failed'
          });
        });
    }
  }

  private _isRankingProcess(dropEventData) {
    const targetTeamId = +this.extractIdFromDragulaElement(dropEventData[2].classList, '#teamId'),
      sourceTeamId = +this.extractIdFromDragulaElement(dropEventData[3].classList, '#teamId');
    if (targetTeamId && sourceTeamId && (targetTeamId == sourceTeamId)) {
      return true;
    }
  }

  private _prepareTeamOrdersRankDataToPost(teamId) {
    const rankObjectToPost = {
      teamId: '',
      orders: {}
    };
    this.foremen.map((foreman) => {
      foreman.teams.map((team) => {

        if (team.teamId == teamId) {
          rankObjectToPost.teamId = team.teamId;
          team.filterOrders.forEach((order, i) => {
            rankObjectToPost.orders[order.id] = ++i;
          });
        }

      });
    });
    return rankObjectToPost;
  }

  private _receiveNotification() {
    this.notificationsService.receiveMessage()
      .subscribe((msg: any) => {

        this.utilities.setGrowlMessage({
          severity: 'success',
          summary: 'Message',
          detail: msg.notification.body
        });
        const order = JSON.parse(msg.data.order);
        this._searchAndUpdateOrAddOrder(order);
      });
  }

  private _searchAndUpdateOrAddOrder(order) {

    let orderNotFound = true;
    // const orderProperitiesName = ['onholdOrders', 'openOrders'];
    // orderProperitiesName.forEach((propertyName) => {
    //   this[propertyName].forEach((singleOrder, i) => {
    //     if (singleOrder.id == order.id) {
    //       this[propertyName][i] = order;
    //       orderNotFound = false;
    //     }
    //   });
    // });
    this.foremen.map((foreman: any) => {
      foreman.teams.forEach((team: any) => {
        team.filterOrders.forEach((singleOrder, i) => {
          if (singleOrder.id == order.id) {

            if (order.statusName === "Complete" || order.statusName === "Canceled") {
              team.filterOrders.splice(i, 1);
            } else if (order.statusName === "On-Hold") {
              team.filterOrders.splice(i, 1);
            } else if (order.acceptanceFlag === 3) {
              team.filterOrders.splice(i, 1);
            } else {
              team.filterOrders[i] = order;
              if (order.acceptanceFlag !== 3) {
                orderNotFound = false;
              }
            }
          }
        });
      });
    });
    if (orderNotFound) {
      if (order.statusName == 'On-Hold' || order.statusName == 'Open') {
        this.orders.push(order);
      }
    }
  }

  showHideFilter() {
    this.showFilter = !this.showFilter;
  }

  showHideMapFilter() {
    this.showMapFilter = !this.showMapFilter;
  }

  postAssignOrUnAssignOrders(dropEventData) {
    const item = dropEventData[1];
    const target = dropEventData[2];
    const teamId = +this.extractIdFromDragulaElement(target.classList, '#teamId') || 0;
    const orderId = +this.extractIdFromDragulaElement(item.classList, '#orderId');
    const sourceId = +this.extractIdFromDragulaElement(dropEventData[3].classList, '#teamId') || 0;
    if (teamId != sourceId) {
      if (teamId) {
        const assignOrderObj = {
          orderId,
          teamId
        };
        this.postAssignOrders(assignOrderObj);
      } else {
        const unAssignOrderObj = {
          orderId
        };
        this.postUnAssignOrders(unAssignOrderObj);
      }
    }
  }

  extractIdFromDragulaElement(classList, propertyName) {
    let id;
    classList.forEach((className) => {
      if (className.includes(propertyName)) {
        id = className.replace(propertyName, '');
      }
    });
    return id;
  }

  postAssignOrders(assignOrderObj) {
    this.toggleLoading = true;
    this.lookup.assginOrder(assignOrderObj).subscribe((res) => {
      this.getDispatcherOrders();
      this.getTeamsAndOrders();
      this.toggleLoading = false;
    }, () => {
      this.toggleLoading = false;
    });
  }

  postUnAssignOrders(unAssignOrderObj) {
    this.toggleLoading = true;

    this.lookup.unAssginOrder(unAssignOrderObj).subscribe((res) => {
      this.getDispatcherOrders();
      this.getTeamsAndOrders();
      this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      });
  }

  openViewOrderModal(notificationOrder) {
    let editOrderModalRef = this.modelService.open(EditOrderModalComponent);
    editOrderModalRef.componentInstance.data = notificationOrder;
    editOrderModalRef.componentInstance.notificationOrder = true;
  }

  ngOnDestroy() {
    this.activatedRouteSubscription && this.activatedRouteSubscription.unsubscribe();
    this.dragulaService.destroy('orders');
    this.utilities.toggleFullWidth = false;
    this.dispatcherOrdersSubscription && this.dispatcherOrdersSubscription.unsubscribe();
    this.getTeamByDispatcherByIdSubscription && this.getTeamByDispatcherByIdSubscription.unsubscribe();
    this.allVehiclesSubscription && this.allVehiclesSubscription.unsubscribe();
    this.orderStatusObservable && this.orderStatusObservable.unsubscribe();
    this.utilities.setSavedNotificationText('');
    this.notificationSubscription && this.notificationSubscription.unsubscribe();
  }

  filterUnAssignedOrders(listOfOrders) {
    console.log('filter unassigned orders output =', listOfOrders);
    this.orders = listOfOrders;
  }

  orderTransfered(id) {
    console.log(id);
    this.orders = this.orders.filter((order) => {
      return id != order.id;
    });
    console.log(id);
    this.foremen.map((foreman: any) => {
      foreman.teams.map((tec: any) => {
        tec.orders = tec.orders.filter((order) => {
          return id != order.id;
        });
      });
    });
  }

  addOrderToSelection(order) {
    if (!this.selectedOrders.includes(order.id)) {
      this.selectedOrders.push(order.id);
    } else {
      this.selectedOrders = this.selectedOrders.filter((singleOrderId) => {
        return order.id != singleOrderId;
      });
    }
    console.log(this.selectedOrders);
  }

  toggleActiveTab(activeTab) {
    this.currentActiveTab = activeTab;
  }

  toggleTodayOrders() {
    this.todayOrdersStatus = !this.todayOrdersStatus;
    if (this.todayOrdersStatus) {
      // this.getDisAllOrders();
    } else {
      this.orderMap = [];
    }
  }

  toggleVehiclesStatus() {
    this.vehiclesStatus = !this.vehiclesStatus;
    if (this.vehiclesStatus) {
      // this.getAllVehicles();
    } else {
      this.allVehicles = [];
    }
  }

  private _filterOrdersListBoard(list, terms) {
    let termsCount = 0
      , filterOrders = [];

    for (let property in terms) {
      if (terms.hasOwnProperty(property)) {
        termsCount++;
      }
    }

    list.map((order) => {
      let existedTermsCount = 0;
      for (let property in terms) {
        if (terms.hasOwnProperty(property)) {
          if (Array.isArray(terms[property])) {
            terms[property].map((element) => {
              if (order[property] == element) {
                existedTermsCount++;
              }
            });
          } else {
            if (property == 'phone') {
              if ((terms[property] == order.phoneOne) || (terms[property] == order.phoneTwo)) {
                existedTermsCount++;
              }
            } else {
              if (terms[property] == order[property]) {
                existedTermsCount++;
              }
            }
          }
        }
      }
      if (existedTermsCount == termsCount) {
        filterOrders.push(order);
      }
    });

    return filterOrders;
  }

  applyFilterBoardTemplate(terms: any[]) {
    this.toggleFilter(false);
    this.showHideFilter();
    this.lookup.postBoardOrderFilter(terms)
      .subscribe(
        res => this.orders = res.data,
        err => console.log(err)
      );
  }

  applyFilterBoard(terms) {
    this.isFilterActive = true;
    if (terms.fromWhere == 1) { // filter unassigned orders only
      console.log('from where = ' + terms.fromWhere);
      this.resetBoardFilter();
      delete terms.fromWhere;
      this.filterUnAssignedOrders(this._filterOrdersListBoard(this.allUnassignedOrders, terms));
    } else if (terms.fromWhere == 2) { // filter assigned orders only
      console.log('from where = ' + terms.fromWhere);
      this.resetBoardFilter();
      delete terms.fromWhere;
      this.openOrders = [];
      this.onholdOrders = [];
      this.foremen.map((foreman: any) => {
        foreman.teams.map((team: any) => {
          team.filterOrders = this._filterOrdersListBoard(team.orders, terms);
        });
      });
    } else if (terms.fromWhere == 3) { // filter both assigned and unassigned orders
      console.log('from where = ' + terms.fromWhere);
      this.resetBoardFilter();
      delete terms.fromWhere;
      this.foremen.map((foreman: any) => {
        foreman.teams.map((team: any) => {
          team.filterOrders = this._filterOrdersListBoard(team.orders, terms);
        });
      });
      this.filterUnAssignedOrders(this._filterOrdersListBoard(this.allUnassignedOrders, terms));
    }
  }

  resetBoardFilter() {
    this.isFilterActive = false;
    this.foremen.map((forman) => {
      forman.teams.map((team) => {
        team.filterOrders = team.orders.slice();
      });
    });
    this.filterUnAssignedOrders(this.allUnassignedOrders);
  }

  activeOrder(order) {
    console.log(order.lat);
    console.log(order.long);
    if (this.toggleMap) {
      this.lat = order.lat;
      this.lng = order.long;
      this.zoom = 16;
    }
  }

  toggleFilter(status) {
    console.log(status);
    if (status) {
      this.filterStatusClass = ['full-filter-width'];
    } else {
      this.filterStatusClass = ['zero-filter-width'];
    }
  }

  applyToggleMap() {
    if (this.toggleMap) {
      this.toggleOverflowX = ['hide-overflowX'];
    } else {
      this.toggleOverflowX = ['show-overflowX'];
    }
  }


  // orderClassNaming(order){
  //   if (order.priorityName === "Medium") {
  //     if (order.typeName === "Contract service Order" || order.typeName === "Enhancement Job Order" || order.typeName === "Preventive Maintenance Order") {
  //       order.classThemeName = "day" + new Date(order.contractStartDate).getDay();
  //     } else if (order.typeName === "Cash Call Order" || order.typeName === "SRAC Installation Order") {
  //       order.classThemeName = "dark-green"
  //     } else if (order.typeName === "Cash Compressor") {
  //       order.classThemeName = "orange"
  //     } else if (order.typeName === "MC Potential rders") {
  //       order.classThemeName = "white"
  //     }
  //   } else if (order.priorityName === "High") {
  //     order.classThemeName = "gold"
  //   } else if (order.priorityName === "Very High") {
  //     order.classThemeName = "red"
  //   }
  // }

  getDispatcherOrders() {
    // this.toggleLoading = true;
    let dispatcherId = localStorage.getItem('AlghanimCurrentUserId');
    this.dispatcherOrdersSubscription = this.lookup.getAllUnAssignedOrdersForDispatcher(dispatcherId).subscribe((dispatcher) => {
      this.orders = dispatcher.data;
      this.allUnassignedOrders = dispatcher.data;
      this.orders.map((order) => {
        order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
        order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
        // this.orderClassNaming(order);
      });
      // this.filteredOrders = this.orders.slice();
      console.log(this.orders);
      this.filterUnAssignedOrders(this.orders);
      // this.orderMap = this.orders.slice();
      //console.log(this.orders);
      this.toggleLoading = false;
    },
      err => {
        this.toggleLoading = false;
      });
  }

  getTeamsAndOrders() {

    let dispatcherId = localStorage.getItem('AlghanimCurrentUserId');
    this.fetchingDataDone = false;
    this.getTeamByDispatcherByIdSubscription = this.lookup.getAllAssignedOrdersForDispatcher(dispatcherId).subscribe(
      (response) => {
        this.fetchingDataDone = true;
        if (response.data) {
          this.foremen = response.data;
          this.allAssignedOrders = response.data;
          this.foremen.map((forman) => {
            forman.teams.map((team: any) => {
              if (team.orders) {
                team.orders.map((order) => {
                  order.classes = ['#orderId' + order.id];
                  if (this.disabledStatus.includes(order.statusName)) {
                    order.classes.push('disable-drag');
                  }
                  order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
                  order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
                  // this.orderClassNaming(order);
                });
                team.filterOrders = team.orders.slice();
              } else {
                team.filterOrders = [];
                team.orders = [];
              }
            });

          });

        }
      },
      err => {

      });
  }

  getDisAllOrders() {
    let dispatcherId = localStorage.getItem('AlghanimCurrentUserId');
    this.todayOrdersStatus = true;
    this.lat = 29.378586;
    this.lng = 47.990341;
    this.zoom = 8;

    this.lookup.getDisAllOrders(dispatcherId).subscribe((res) => {
      this.orderMap = res.data || [];
      console.log(this.orderMap);
      this.orderMap.map((order) => {
        order.pin = {
          url: this.choosePin(order.priorityName, order.typeName, order.contractStartDate, order.problemName),
          scaledSize: {
            width: 40,
            height: 40
          }
        };
      });
      this.orderMapFiltered = this.orderMap.slice();
    },
      err => {
        console.log('failed');
      });
  }


  choosePin(priority, orderType, orderDate, problem) {
    // reset pin
    var pin = '';
    let pinsDomain = this.chooseFolder(problem) + "/";
    // check priority
    if (priority.toLowerCase() == 'High'.toLowerCase()) {
      pin = pinsDomain + 'gold.svg';
    } else if (priority.toLowerCase() == 'Very High'.toLowerCase()) {
      pin = pinsDomain + 'red.svg';
    } else if (priority.toLowerCase() === 'Medium'.toLowerCase()) {
      if (orderType.toLowerCase() == 'Cash Call order'.toLowerCase() ||
        orderType.toLowerCase() == 'SRAC installation order'.toLowerCase()) {
        pin = pinsDomain + 'darkgreen.svg';
      } else if (orderType.toLowerCase() == 'Cash Compressor'.toLowerCase()) {
        pin = pinsDomain + 'orange.svg';
      } else if (orderType.toLowerCase() == 'MC potential orders'.toLowerCase()) {
        pin = pinsDomain + 'white.svg';
      } else if (orderType.toLowerCase() == 'Contract service order'.toLowerCase() ||
        orderType.toLowerCase() == 'Enhancement job order'.toLowerCase() ||
        orderType.toLowerCase() == 'Preventive Maintenance Order'.toLowerCase()) {
        var date = new Date(orderDate.toLowerCase());
        var d = date.getDay();
        switch (d) {
          case 0:
            pin = pinsDomain + 'pink.svg';
            break;
          case 1:
            pin = pinsDomain + 'lightgreen.svg';
            break;
          case 2:
            pin = pinsDomain + 'lightblue.svg';
            break;
          case 3:
            pin = pinsDomain + 'purple.svg';
            break;
          case 4:
            pin = pinsDomain + 'lightblue.svg';
            break;
          case 5:
            pin = pinsDomain + 'gray.svg';
            break;
          case 6:
            pin = pinsDomain + 'yellow.svg';
            break;
        }
      }
    }
    return pin;
  }


  chooseFolder(problem) {

    let baseURL = 'assets/Images/PINS/';
    const result: any = {
      url: baseURL,
      name: ''
    };
    switch (problem) {
      case 'Commercial CAC':
        result.baseURL += 'CAC/';
        result.name = 'CAC';
        break;
      case 'Chillers':
        result.baseURL += 'CHI/';
        result.name = 'CHI';
        break;
      case 'CHS':
        result.baseURL += 'CHS/';
        result.name = 'CHS';
        break;
      case 'Cleaning Equipment':
        result.baseURL += '/';
        result.name = '';
        break;
      case 'Electrical':
        result.baseURL += 'ELEC/';
        result.name = 'ELEC';
        break;
      case 'Elevators':
        result.baseURL += 'ZELE/';
        result.name = 'ZELE';
        break;
      case 'Fire Fighting':
        result.baseURL += 'ZFIR/';
        result.name = 'ZFIR';
        break;
      case 'Fork Lifts':
        result.name = 'FLT';
        result.baseURL += 'FLT/';
        break;
      case 'Government Service':
        result.name = 'GAC';
        result.baseURL += 'GAC/';
        break;
      case 'Non Res Non Std':
        result.name = 'NSD';
        result.baseURL += 'NSD/';
        break;
      case 'Plumbing':
        result.name = 'PLB';
        result.baseURL += 'PLB/';
        break;
      case 'Residential CAC':
        result.name = 'RAC';
        result.baseURL += 'RAC/';
        break;
      case 'Residential Electrical Service':
        result.name = 'REL';
        result.baseURL += 'REL/';
        break;
      case 'Residential Plumbing Service':
        result.name = 'RPB';
        result.baseURL += 'RPB/';
        break;
      case 'Residential Swimming Pool Serv':
        result.name = 'RSW';
        result.baseURL += 'RSW/';
        break;
      case 'Security Audio Visual':
        result.name = 'SAV';
        result.baseURL += 'SAV/';
        break;
      case 'SRAC Service':
        result.name = 'SRA';
        result.baseURL += 'SRA/';
        break;
      case 'Non Res Standard':
        result.name = 'STD';
        result.baseURL += 'STD/';
        break;
      case 'Stationed operator':
        result.name = 'STO';
        result.baseURL += 'STO/';
        break;
      case 'Subcontractor':
        result.name = 'SUB';
        result.baseURL += 'SUB/';
        break;
      case 'Technology':
        result.name = 'TEC';
        result.baseURL += 'TEC/';
        break;
      case 'Video Data Network':
        result.name = 'VDN';
        result.baseURL += 'VDN/';
        break;
      case 'Water Cooler':
        result.name = 'WTC';
        result.baseURL += 'WTC/';
        break;
      default:
        result.name = '';
        result.baseURL += '/';
        break;

    }
    return baseURL + result.name;
  }

  resetFilterMap() {
    this.getDispatcherOrders();
    this.filterUnAssignedOrders(this.allUnassignedOrders);
  }

  applyFilterMap(terms: any[]) {
    this.toggleFilter(false);
    this.showHideFilter();
    this.lookup.postMapOrderFilter(terms)
      .subscribe(
        res => this.orders = res.data,
        err => console.log(err)
      );
  }

  toggleFullScreen() {
    this.fullScreenMap = !this.fullScreenMap;
  }

}
