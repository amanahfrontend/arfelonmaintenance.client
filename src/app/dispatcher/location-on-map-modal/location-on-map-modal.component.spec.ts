import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationOnMapModalComponent } from './location-on-map-modal.component';

describe('LocationOnMapModalComponent', () => {
  let component: LocationOnMapModalComponent;
  let fixture: ComponentFixture<LocationOnMapModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationOnMapModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationOnMapModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
