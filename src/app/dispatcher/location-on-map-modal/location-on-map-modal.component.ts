import { Component, Input, OnInit, ViewChild, ElementRef, NgZone, AfterViewInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import {} from 'googlemaps';

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

declare var google: any;

@Component({
  selector: 'app-location-on-map-modal',
  templateUrl: './location-on-map-modal.component.html',
  styleUrls: ['./location-on-map-modal.component.css']
})
export class LocationOnMapModalComponent implements OnInit {
  public lat: number = 29.378586;
  public lng: number = 47.990341;
  public zoom: number = 12;
  @Input() order: any;

  // search for location on google maps
  public searchControl: FormControl;

  @ViewChild('search') searchElementRef: ElementRef;

  constructor(public activeModal: NgbActiveModal,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) { }

  ngOnInit() {

    //create search FormControl
    this.searchControl = new FormControl();

    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return 'no such place';
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  setLangANdLat(e) {
    this.order.lat = e.coords.lat;
    this.order.long = e.coords.lng;
  }

  closeModal() {
    this.activeModal.close();
  }

}
