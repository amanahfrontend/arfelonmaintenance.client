import { Injectable } from '@angular/core';
// import {AngularFireDatabase} from '@angular/fire/database';
// import {AngularFireAuth} from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
// import * as firebase from 'firebase';
import 'rxjs/add/operator/take';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { LookupService } from '../api-module/services/lookup-services/lookup.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  // messaging = this.afMessage;
  currentMessage = new BehaviorSubject(null);

  constructor(private afMessage: AngularFireMessaging, private lookupService: LookupService) {
  }

  updateToken(token) {
    // this.afAuth.authState.take(1).subscribe(user => {
    //   if (!user) return;
    //
    //   const data = {[user.uid]: token};
    //   this.db.object('fcmTokens/').update(data);
    // });
  }

  // Mohammed Osman: Disable firebase. {Start}
  //getPermission() {
  //this.afMessage.requestPermission
  // .subscribe((res) => {
  //   console.log('Notification permission granted.');
  //   this.afMessage.getToken
  //     .subscribe((res) => {
  //       console.log(res);

  //       localStorage.setItem("deviceId", res);
  //       this.lookupService.addUserDevice(res)
  //         .subscribe((res) => {
  //           console.log(res);
  //         },
  //           (err) => {
  //             console.log(err);
  //           });
  //     });
  // }, (err) => {
  //   console.log('Unable to get permission to notify.', err);
  // });

  // .then(token => {
  //   console.log(token);
  //   this.updateToken(token);
  // })
  // .catch((err) => {
  //   console.log('Unable to get permission to notify.', err);
  // });
  //}
  // Mohammed Osman: Disable firebase. {End}

  receiveMessage() {
    return this.afMessage.messages;
  }

}
