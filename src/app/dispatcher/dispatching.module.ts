import { NgModule } from '@angular/core';
import { DispatcherOrderBoardComponent } from './dispatcher-order-board/dispatcher-order-board.component';
import { SharedModuleModule } from '../shared-module/shared-module.module';
import { OrderCardComponent } from './order-card/order-card.component';
import { SetTimeModalComponent } from './set-time-modal/set-time-modal.component';
import { TransferDispatcherModalComponent } from './transfer-dispatcher-modal/transfer-dispatcher-modal.component';
import { OrderProgressModalComponent } from './order-progress-modal/order-progress-modal.component';
import { AgmCoreModule } from '@agm/core';
import { SidebarModule } from 'primeng/components/sidebar/sidebar';
import { ReportComponent } from './report/report.component';
import { OrderFilterComponent } from './order-filter/order-filter.component';
import { VisitTimeCalenderComponent } from './visit-time-calender/visit-time-calender.component';
import { ScheduleModule } from 'primeng/components/schedule/schedule';
import { OrderDetailsModalComponent } from './order-details-modal/order-details-modal.component';
import { ToggleButtonModule } from 'primeng/primeng';
import { BulkAssignModalComponent } from './bulk-assign-modal/bulk-assign-modal.component';
import { PreventiveComponent } from './preventive/preventive/preventive.component';
import { PreventiveDetailsModalComponent } from './preventive/preventive-details-modal/preventive-details-modal.component';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { TimeSheetComponent } from './time-sheet/time-sheet.component';
import { DispatcherContainerComponent } from './dispatcher-container/dispatcher-container.component';
import { NotificationsService } from './notifications.service';
import { LocationOnMapModalComponent } from './location-on-map-modal/location-on-map-modal.component';
import { OrderMapFilterComponent } from './order-map-filter/order-map-filter.component';
import { DispatcherOorderBoardComponent } from './dispatcher-oorder-board/dispatcher-oorder-board.component';
import { OrdersMapComponent } from './orders-map/orders-map.component';
import { AssignedOrdersBoardComponent } from './assigned-orders-board/assigned-orders-board.component';
import { SupervisorBoardComponent } from './supervisor-board/supervisor-board.component';
import { SupervisorBoardForDispatcherComponent } from './supervisor-board-for-dispatcher/supervisor-board-for-dispatcher.component';
import { PrintoutComponent } from './printout/printout.component';

@NgModule({
    imports: [
        SharedModuleModule,
        SidebarModule,
        ScheduleModule,
        ToggleButtonModule,
        AgmCoreModule.forRoot({
            apiKey: localStorage.getItem('webApiKey'),
            libraries: ["places"]
        })
    ],
    declarations: [
        DispatcherOrderBoardComponent,
        OrderCardComponent,
        SetTimeModalComponent,
        TransferDispatcherModalComponent,
        OrderProgressModalComponent,
        ReportComponent,
        OrderFilterComponent,
        VisitTimeCalenderComponent,
        OrderDetailsModalComponent,
        BulkAssignModalComponent,
        PreventiveComponent,
        PreventiveDetailsModalComponent,
        EditOrderComponent,
        TimeSheetComponent,
        DispatcherContainerComponent,
        LocationOnMapModalComponent,
        OrderMapFilterComponent,
        DispatcherOorderBoardComponent,
        OrdersMapComponent,
        AssignedOrdersBoardComponent,
        SupervisorBoardComponent,
        SupervisorBoardForDispatcherComponent,
        PrintoutComponent
    ],
    entryComponents: [
        SetTimeModalComponent,
        TransferDispatcherModalComponent,
        OrderProgressModalComponent,
        OrderDetailsModalComponent,
        BulkAssignModalComponent,
        PreventiveComponent,
        PreventiveDetailsModalComponent,
        LocationOnMapModalComponent
    ],
    exports: [
        DispatcherOrderBoardComponent,
        OrderCardComponent,
        SetTimeModalComponent,
        TransferDispatcherModalComponent,
        OrderProgressModalComponent,
        ReportComponent,
        OrderFilterComponent,
        VisitTimeCalenderComponent,
        OrderDetailsModalComponent,
        BulkAssignModalComponent,
        PreventiveComponent,
        PreventiveDetailsModalComponent,
        EditOrderComponent,
        TimeSheetComponent,
        DispatcherContainerComponent,
        LocationOnMapModalComponent,
        OrderMapFilterComponent,
        DispatcherOorderBoardComponent,
        OrdersMapComponent,
        AssignedOrdersBoardComponent,
        SupervisorBoardComponent,
        SupervisorBoardForDispatcherComponent,
        PrintoutComponent
    ]
})

export class DispatchingModule {
}
