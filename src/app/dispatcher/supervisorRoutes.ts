import { DispatcherOrderBoardComponent } from './dispatcher-order-board/dispatcher-order-board.component';
import { AuthGuardGuard } from '../api-module/guards/auth-guard.guard';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { DispatcherContainerComponent } from './dispatcher-container/dispatcher-container.component';
import { NotificationListComponent } from '../shared-module/shared/notification-list/notification-list.component';
import { Routes } from '@angular/router';
import { SupervisorBoardComponent } from './supervisor-board/supervisor-board.component';
import { SupervisorBoardForDispatcherComponent } from './supervisor-board-for-dispatcher/supervisor-board-for-dispatcher.component';

export const supervisorRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardGuard],
    data: { roles: ['Supervisor'] },
    component: DispatcherContainerComponent,
    children: [
      {
        path: '',
        component: DispatcherOrderBoardComponent,
        redirectTo: 'board',
        pathMatch: 'full',
      },
      {
        path: 'board',
        component: SupervisorBoardComponent
      },
      {
        path: "notifications",
        component: NotificationListComponent
      },
      {
        path: 'dispatcher',
        component: SupervisorBoardForDispatcherComponent,
      },
      {
        path: 'board/edit-order/:orderId',
        component: EditOrderComponent,
        canActivate: [AuthGuardGuard],
        data: { roles: ['Supervisor'] }
      },
      {
        path: 'dispatcher/edit-order/:orderId',
        component: EditOrderComponent,
        canActivate: [AuthGuardGuard],
        data: { roles: ['Supervisor'] }
      }
    ]
  }
];
