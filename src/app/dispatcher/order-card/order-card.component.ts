import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {EditOrderModalComponent} from '../../customer/edit-order-modal/edit-order-modal.component'
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SetTimeModalComponent} from '../set-time-modal/set-time-modal.component';
import {TransferDispatcherModalComponent} from '../transfer-dispatcher-modal/transfer-dispatcher-modal.component';
import {OrderProgressModalComponent} from '../order-progress-modal/order-progress-modal.component';

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.css']
})

export class OrderCardComponent implements OnInit {
  @Input() order: any;
  @Input() bulkAssignMode: any;
  // @Input() isOrderSelected: boolean;
  @Output() orderSelected = new EventEmitter;
  @Output() orderTransfered = new EventEmitter;
  currentDetailedOrderId: any;
  editOrderModalRef: any;
  setTimeModalRef: any;
  openTransferOrder: any;
  openOrderProgress: any;

  constructor(private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  selectOrder() {
    this.orderSelected.emit(this.order);
  }

  chooseColor(orderType, orderPriority, orderDate) {
    let style = {
      backgroundColor: '',
      color: null
    };
    // let's get the date number
    let date = new Date(orderDate).getDay();
    if (orderPriority.toLowerCase() === 'Very High'.toLowerCase()) {
      style.backgroundColor = 'red';
      style.color = 'black';
    } else if (orderPriority.toLowerCase() === 'High'.toLowerCase()) {
      style.backgroundColor = 'gold';
      style.color = 'black';
    } else { // condition : orderPriority === 'Medium'
      if (orderType.toLowerCase() === 'Cash Call order'.toLowerCase() || 
        orderType.toLowerCase() === 'SRAC installation order'.toLowerCase()) {
        style.backgroundColor = 'darkgreen';
        style.color = 'white';
      } else if (orderType.toLowerCase() === 'Cash Compressor'.toLowerCase()) {
        style.backgroundColor = 'orange';
        style.color = 'black';
      } else if (orderType.toLowerCase() === 'MC potential orders'.toLowerCase()) {
        style.backgroundColor = 'white';
        style.color = 'black';
      } else if (orderType.toLowerCase() === 'Contract service order'.toLowerCase() ||
        orderType.toLowerCase() === 'Enhancement job order'.toLowerCase() ||
        orderType.toLowerCase() === 'Preventive maintenance order'.toLowerCase()) {
          switch (date) {
            case 0 : 
              style.backgroundColor = 'pink';
              style.color = 'black';
              break;
            case 1 :
              style.backgroundColor = 'lightgreen';
              style.color = 'black';
              break;
            case 2 :
              style.backgroundColor = 'lightblue';
              style.color = 'black';
              break;
            case 3 :
              style.backgroundColor = 'purple';
              style.color = 'white';
              break;
            case 4 :
              style.backgroundColor = 'turquoise';
              style.color = 'white';
              break;
            case 5 :
              style.backgroundColor = 'gray';
              style.color = 'white';
              break;
            case 6 :
              style.backgroundColor = 'yellow';
              style.color = 'black';
          }
        }
    }
    return style;
  }

  setCurrentDetailedOrderId(id) {
    this.currentDetailedOrderId = id;
  }

  openEditOrderModal(data) {
    this.editOrderModalRef = this.modalService.open(EditOrderModalComponent);
    this.editOrderModalRef.componentInstance.data = data;
    this.editOrderModalRef.result
      .then(() => {

      })
      .catch(() => {

      })
  }

  openSetTimeModal(order) {
    this.setTimeModalRef = this.modalService.open(SetTimeModalComponent, {
      size: 'sm'
    });
    this.setTimeModalRef.componentInstance.order = order;
    this.setTimeModalRef.result
      .then(() => {
        console.log('set time success');
      })
      .catch(() => {
        console.log('failed');
      })
  }

  openTransfer(order) {
    this.openTransferOrder = this.modalService.open(TransferDispatcherModalComponent, {
      size: 'sm'
    });
    this.openTransferOrder.componentInstance.order = order;
    this.openTransferOrder.result
      .then(() => {
        console.log('closed successfully');
        this.orderTransfered.emit(this.order.id);
      })
      .catch(() => {

      })
  }

  openOrderProgressModal(order) {
    this.openOrderProgress = this.modalService.open(OrderProgressModalComponent);
    this.openOrderProgress.componentInstance.order = order;
    this.openOrderProgress.result
      .then(() => {

      })
      .catch(() => {

      })
  }

}
