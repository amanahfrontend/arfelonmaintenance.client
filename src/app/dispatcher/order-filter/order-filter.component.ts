import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Component, OnInit, Output, EventEmitter, Input, OnDestroy, ViewChild } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { Subscription } from 'rxjs';
import { AuthenticationServicesService } from '../../api-module/services/authentication/authentication-services.service';
import { item } from '../../api-module/services/globalPath';

@Component({
  selector: 'app-order-filter',
  templateUrl: './order-filter.component.html',
  styleUrls: ['./order-filter.component.css']
})

export class OrderFilterComponent implements OnInit, OnDestroy {
  filter: any;
  // orderMap: any[];
  allStatuses = [];
  allAreas: any[] = [];
  allProblems: any[] = [];
  allDispatchers: any[] = [];
  allTechnicians: any[] = [];
  allOrderTypes: any[] = [];
  allDivisions: any[] = [];
  statusesObservable: Subscription;
  areasObservable: Subscription;
  problemsObservable: Subscription;
  dispatchersObservable: Subscription;
  techniciansObservable: Subscription;
  OrderTypesObservable: Subscription;
  divisionsObservable: Subscription;
  tecsByDisSubscription: Subscription;
  filterObjectToPost: object;
  tecs: any[];

  allRejectionStatus: any = [];
  startDateFrom: Date;
  startDateTo: Date;
  reportOrders: any[] = [];

  @Input() type: string;
  @Output() todayOrders = new EventEmitter;
  @Output() filteredOrders = new EventEmitter;
  @Output() resetFilter = new EventEmitter;
  @Output() orderProgressMode = new EventEmitter;
  @Output() filterTermsBoard = new EventEmitter;
  @Output() filterTermsMap = new EventEmitter;


  constructor(private lookup: LookupService,
    private authService: AuthenticationServicesService) {
  }

  ngOnInit() {
    this.filter = {};
    this.filter.fromWhere = 3;
    this.filter.RejectionStatus = 4;
    this.getAllStatuses();
    this.getAllAreas();
    this.getAllProblems();
    this.getAllTechnicians();
    this.getAllOrderTypes();
    this.getAllDevisions();
    this.getAllDispatchers();
    this.allRejectionStatus = [
      { name: 'Rejected', id: 3 },
      { name: 'accepted', id: [0, 1, 2] },
    ]
  }

  ngOnDestroy() {
    this.statusesObservable && this.statusesObservable.unsubscribe();
    this.areasObservable && this.areasObservable.unsubscribe();
    this.problemsObservable && this.problemsObservable.unsubscribe();
    this.dispatchersObservable && this.dispatchersObservable.unsubscribe();
    this.techniciansObservable && this.techniciansObservable.unsubscribe();
    this.OrderTypesObservable && this.OrderTypesObservable.unsubscribe();
    this.divisionsObservable && this.divisionsObservable.unsubscribe();
    this.tecsByDisSubscription && this.tecsByDisSubscription.unsubscribe();
  }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Code': 'Code',
      'Created Date': 'Created Date',
      'Status': 'Status',
      'Type': 'Type',
      'Priority': 'Priority',
      'Problem': 'Problem',
      'Customer Name': 'Customer Name',
      'Customer Phone': 'Customer Phone',
      'Governorate': 'Governorate',
      'Area': 'Area',
      'Block': 'Block',
      'Street': 'Street',
      'Dispatcher Name': 'Dispatcher Name',
      'Technician Name': 'Technician Name',
      'Order Progress': 'Order Progress',
      'Progress Date': 'Progress Date',
      'Progress Creator': 'Progress Creator',
      'Progress by Tecnician': 'Progress by Tecnician'
    });
    this.reportOrders.map((order) => {
      exportData.push({
        'Code': order.order.code,
        'Created Date': order.createdDateView,
        'Status': order.order.orderStatus.name,
        'Type': order.order.orderType.name,
        'Priority': order.order.orderPriority.name,
        'Problem': order.order.orderProblem.name,
        'Customer Name': order.order.customer.name,
        'Customer Phone': order.phoneView,
        'Governorate': order.order.location.governorate,
        'Area': order.order.location.area,
        'Block': order.order.location.block,
        'Street': order.order.location.street,
        'Dispatcher Name': order.order.dispatcherName,
        'Technician Name': order.order.technicanName,
        'Order Progress': order.orderProgress && order.orderProgress.progressStatus.name,
        'Progress Date': order.orderProgress && order.orderProgress.createdDateView,
        'Progress Creator': order.orderProgress && order.orderProgress.createdBy.userName,
        'Progress by Tecnician': order.orderProgress && order.orderProgress.technicanName
      });
    });
    let reportName = 'Report' + Date.now();
    return new Angular2Csv(exportData, reportName, {
      showLabels: true
      // showTitle: true
    });
  }

  applyFilter() {
    // let dispatcherId = this.authService.CurrentUser().id;
    // console.log(this.filter);
    let filterAreas = [];
    let filterProblems = [];
    let filterStatus = [];
    let filterTechnicians = [];
    let filterOrderTypes = [];
    let filterDivisions = [];
    let filterDispatchers = [];
    // get selected areas
    this.filter.areas && this.filter.areas.map((area) => {
      filterAreas.push(area.id);
    });
    // get selected problems
    this.filter.problems && this.filter.problems.map((problem) => {
      filterProblems.push(problem.id);
    });
    // get selected orders
    this.filter.status && this.filter.status.map((status) => {
      filterStatus.push(status.id);
    });
    // get selected technicians
    this.filter.technicians && this.filter.technicians.map(
      (technician) => {
        filterTechnicians.push(technician.id)
      }
    );
    // get selected order types
    this.filter.orderTypes && this.filter.orderTypes.map(
      (ordertype) => {
        filterOrderTypes.push(ordertype.id)
      }
    );
    // get selected divisions
    this.filter.divisions && this.filter.divisions.map(
      (division) => {
        filterDivisions.push(division.id)
      }
    );
    // get selected dispatchers
    this.filter.dispatchers && this.filter.dispatchers.map(
      (dispatcher) => {
        filterDispatchers.push(dispatcher.id)
      }
    );
    // apply filter in case of type is 'order board'
    if (this.type == 'order board') {
      this.filterObjectToPost = {
        orderCode: this.filter.code,
        customerName: this.filter.customerName,
        customerCode: this.filter.customerCode,
        customerPhone: this.filter.phone,
        areaIds: filterAreas,
        blockName: this.filter.blockName,
        problemIds: filterProblems,
        statusIds: filterStatus,
        rejection: this.filter.RejectionStatus,
        dispatcherId: filterDispatchers,
        fromWhere: this.filter.fromWhere,
      };
    } else if (this.type == 'map') { // apply filter in case of type is 'map'
      this.filterObjectToPost = {
        areaId: filterAreas,
        dateFrom: this.filter.dateFrom,
        dateTo: this.filter.dateTo,
        technicianId: filterTechnicians,
        orderTypeId: filterOrderTypes,
        problemId: filterProblems,
        statusId: filterStatus,
        divisionId: filterDivisions,
        dispatcherId: filterDispatchers,
        assigned: this.filter.fromWhere
      };
    }
    console.log(this.filterObjectToPost);
    // removing unused filter objects to send the used objects only
    for (let property in this.filterObjectToPost) {
      if (this.filterObjectToPost.hasOwnProperty(property)) {
        if (Array.isArray(this.filterObjectToPost[property])) {
          if (!this.filterObjectToPost[property].length) {
            delete this.filterObjectToPost[property];
          }
        } else {
          if (!this.filterObjectToPost[property]) {
            delete this.filterObjectToPost[property];
          }
        }
      }
    }
    // emitting an event when using the order board filter
    if (this.type == 'order board') {
      this.filterTermsBoard.emit(this.filterObjectToPost);
    } else if (this.type == 'map') { // emitting an event when using the map filter
      this.filterTermsMap.emit(this.filterObjectToPost);
    } else {
      this.postFilterProperties(this.filterObjectToPost);
    }
  }


  postFilterProperties(filterObjectToPost) {
    console.log(filterObjectToPost);
    if (this.type == 'report') {
      this.lookup.filterOrders(filterObjectToPost).subscribe((res) => {
        console.log(res);
        this.orderProgressMode.emit(this.filter.hasOrderProgress);
        this.reportOrders = res;
        this.filteredOrders.emit(res);
        console.log(this.filter.hasOrderProgress);

        // this.todayOrdersStatus = false;
      },
        err => {

        });
    } else {
      this.lookup.filterOrdersMap(filterObjectToPost).subscribe((res) => {
        console.log(res);
        this.filteredOrders.emit(res);
        console.log(this.filter.hasOrderProgress);
        this.orderProgressMode.emit(this.filter.hasOrderProgress);
        // this.todayOrdersStatus = false;
      },
        err => {

        });
    }
  }

  emitTodayOrders() {
    this.filter = {};
    this.todayOrders.emit();
  }

  checkFilter() {
    let checkingObject = Object.assign({}, this.filter);
    if (this.type == 'order board') {
      delete checkingObject.fromWhere;
    }
    if (JSON.stringify(checkingObject) == JSON.stringify({})) {
      // console.log('will return true because filter is clear');
      return true;
    } else {
      for (let property in checkingObject) {
        // console.log(property);
        if (checkingObject.hasOwnProperty(property)) {
          // console.log('will return false');
          if (Array.isArray(checkingObject[property])) {
            if (checkingObject[property].length) {
              return false;
            }
          } else {
            if (checkingObject[property]) {
              return false;
            }
          }
        }
      }
      return true;
    }
  }

  applyResetFilter() {
    this.filter = {
      fromWhere: 3,
      RejectionStatus: 4
    };
    this.reportOrders = [];
    this.resetFilter.emit();
  }

  getAllStatuses() {
    this.statusesObservable = this.lookup.getAllStatus()
      .subscribe(
        res => this.allStatuses = res.data,
        err => console.log(err)
      );
  }

  getAllProblems() {
    this.problemsObservable = this.lookup.getAllProblems()
      .subscribe(
        res => this.allProblems = res.data,
        err => console.log(err)
      );
  }

  getAllAreas() {
    this.areasObservable = this.lookup.getAllAreas()
      .subscribe(
        res => this.allAreas = res.data,
        err => console.log(err)
      );
  }

  getAllTechnicians() {
    this.techniciansObservable = this.lookup.getAllTechnicians()
      .subscribe(
        res => this.allTechnicians = res.data,
        err => console.log(err)
      );
  }

  getAllOrderTypes() {
    this.OrderTypesObservable = this.lookup.getAllOrderTypes()
      .subscribe(
        res => this.allOrderTypes = res.data,
        err => console.log(err)
      );
  }

  getAllDevisions() {
    this.divisionsObservable = this.lookup.getAllDivisions()
      .subscribe(
        res => this.allDivisions = res.data,
        err => console.log(err)
      );
  }

  getAllDispatchers() {
    this.dispatchersObservable = this.lookup.getAllDispatchers()
      .subscribe(
        res => this.allDispatchers = res.data,
        err => console.log(err)
      );
  }
}
