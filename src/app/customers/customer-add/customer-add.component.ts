import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { MessageService } from "primeng/primeng";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { CustomerService } from "../services/customer.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-customer-add",
  templateUrl: "./customer-add.component.html",
  styleUrls: ["./customer-add.component.css"]
})
export class CustomerAddComponent implements OnInit {
  @Input() header;
  @Input() data: any;
  isSaving: boolean;
  isSuccess: boolean = false;
  userObj: any = {};
  customerData: any = {};

  isLoading: boolean = false;

  constructor(
    public activeModal: NgbActiveModal,
    private messageService: MessageService,
    private utilities: UtilitiesService,
    private readonly customerService: CustomerService) {
  }

  ngOnInit() {
    this.customerData = this.data;
  }

  close(result) {
    result = {
      id: this.data.id,
      divisionId: result.divisionId
    };

    this.activeModal.close(result);
  }

  saveCustomer() {
    this.isSaving = true;

    let request: Observable<any>;
    let successMessage: string;

    if (this.data.id) {
      request = this.customerService.updateCustomer(this.customerData);
      successMessage = "New customer added successfully.";
    } else {
      request = this.customerService.addCustomer(this.customerData);
      successMessage = "Customer updated successfully.";
    }

    request.subscribe(
      result => {
        if (result.isSucceeded === true) {

          this.messageService.add({
            severity: "success",
            summary: "Successful!",
            detail: successMessage
          });
        }

        this.activeModal.close();
      },
      err => {
        err.status === 401 && this.utilities.unauthrizedAction();

        this.messageService.add({
          severity: "error",
          summary: "Failed!",
          detail: "Failed to save due to network error."
        });

      });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
