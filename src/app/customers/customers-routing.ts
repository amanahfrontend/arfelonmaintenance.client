import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuardGuard } from "../api-module/guards/auth-guard.guard";
import { CustomersListComponent } from "./customers-list/customers-list.component";

const customersRoutes: Routes = [
  {
    path: "",// AppRootModule will call it like "/customers";
    component: CustomersListComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ["Admin", "CallCenter", "Maintenance"] },
    children: [
      {
        path: "list",
        component: CustomersListComponent
      }
    ]
  }
];

@NgModule(
  {
    imports: [RouterModule.forChild(customersRoutes)],
    exports: [RouterModule]
  })
export class CustomersRoutingModule {
}
