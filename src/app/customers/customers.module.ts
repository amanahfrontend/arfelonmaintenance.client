import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { SpinnerModule } from "primeng/primeng";
import { CustomersListComponent } from "./customers-list/customers-list.component";
import { CustomersRoutingModule } from "./customers-routing";
import { SharedModuleModule } from "../shared-module/shared-module.module";
import { CustomerAddComponent } from "./customer-add/customer-add.component";

@NgModule(
  {
    imports: [
      CommonModule, FormsModule, CustomersRoutingModule,
      SharedModuleModule, SpinnerModule],
    declarations: [CustomersListComponent, CustomerAddComponent],
    entryComponents: [CustomerAddComponent]
  })
export class CustomersModule {

}
// This module is not used.
// All components under the folder "customers" aren't used.
