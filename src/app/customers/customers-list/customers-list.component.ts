import { Component, OnInit, Input } from "@angular/core";
import { Subscription } from "rxjs";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MessageService } from "primeng/primeng";
import { languagesComponent } from "../../shared-module/shared/languageModel/languageModel.component";
import { CustomerService } from "../services/customer.service";
import { CustomerAddComponent } from "../customer-add/customer-add.component";

@Component({
  selector: "app-customers-list",
  templateUrl: "./customers-list.component.html",
  styleUrls: ["./customers-list.component.css"]
})
export class CustomersListComponent implements OnInit {

  @Input() role: string;
  @Input() getUserData: string;
  modalRef: any;
  rows: any[];
  toggleLoading: boolean;
  itemsRoles: any = [];

  constructor(
    private readonly customerService: CustomerService,
    private readonly messageService: MessageService,
    private readonly modalService: NgbModal) {
  }

  ngOnInit() {
    this.rows = [];
    this.loadData();
  }

  loadData() {

    this.customerService.getCustomers().subscribe(
      response => {
        this.toggleLoading = false;
        this.rows = response;
        // console.log(JSON.stringify(this.rows));
      },
      err => {
        this.messageService.add({
          severity: "error",
          summary: "Failed!",
          detail: "Failed to get data."
        });

        this.toggleLoading = false;
      });
  }

  add() {
    this.openModal({}, `Add customer`);

    this.modalRef.result.then(
      () => {
        this.loadData();
      })
      .catch(result => {
        //console.log('nothing added');
        this.messageService.add({
          severity: "info",
          summary: "Nothing Added!",
          detail: "You didn't saved new value."
        });
      });
  }

  edit(row) {
    this.openModal(Object.assign({}, row), "Edit customer");

    this.modalRef.result.then(
      (editedValue) => {
        console.log(editedValue);

        this.loadData();
      })
      .catch((result) => {
        this.messageService.add({
          severity: "info",
          summary: "Nothing Edited!",
          detail: "You didn't change the old value"
        });
      });
  }

  addEditLang(row) {
    this.openModalLanguage(Object.assign({}, row), "language", this.role);

    this.modalRef.result.then(
      (newValue) => {
        this.loadData();

      })
      .catch((result) => {
        this.messageService.add({
          severity: "info",
          summary: "Nothing Edited!",
          detail: "You didn't change the old value"
        });
      });
  }

  openModal(data, header) {
    this.modalRef = this.modalService.open(CustomerAddComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.data = data;
  }

  openModalLanguage(data, header, role?) {
    console.log("open Modal Language");
    this.modalRef = this.modalService.open(languagesComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.role = role;
    this.modalRef.componentInstance.data = data;
  }

}
