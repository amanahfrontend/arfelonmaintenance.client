import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import * as myGlobals from "../../api-module/services/globalPath";
import { AuthService } from "../../shared/services/auth.service";

@Injectable({
  providedIn: "root"
})
export class CustomerService {

  constructor(
    private readonly httpClient: HttpClient,
    private readonly authService: AuthService) {
  }

  getCustomers(): Observable<any[]> {
    const url = myGlobals.BaseUrlCustomer + "Customer/Get";

    const currentUser = JSON.parse(this.authService.getLocalStorage("AlghanimCurrentUser"));
    let headers = new Headers({ 'Authorization': "bearer " + currentUser.data.token });
    headers.append("Content-Type", "application/json");

    return this.httpClient.get(url).pipe(map(response => {
      const result = <any[]>response["data"];
      return result;
    }));
  }

  addCustomer(model): Observable<any> {
    const url = myGlobals.BaseUrlCustomer + "Customer/Add";

    model.rowStatusId = 1;
    model.createdByUserId = this.authService.currentUserId;

    return this.httpClient.post(url, model).pipe(map(
      response => {
        const result = <any>response;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }


  updateCustomer(model): Observable<any> {
    model.rowStatusId = 1;
    model.modifiedByUserId = this.authService.currentUserId;

    const url = myGlobals.BaseUrlCustomer + "Customer/Update";

    return this.httpClient.put(url, model).pipe(map(
      data => {
        const response = <any>data;
        return response;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }
    ));
  }
}
