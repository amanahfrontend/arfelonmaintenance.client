import { ModuleWithProviders, NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
//import { CustomersModule } from "./customers/customers.module";
import { PageNotFoundComponentComponent } from "./page-not-found-component/page-not-found-component.component";

const routes: Routes = [
  { path: "", loadChildren: "./login-register-module/login-register-module.module#LoginRegisterModuleModule" },
  { path: "search", loadChildren: "./customer/customer.module#CustomerModule" },
  { path: "admin", loadChildren: "./admin/admin.module#AdminModule" },
  { path: "dispatcher", loadChildren: "./dispatcher/dispatcher.module#DispatcherModule" },
  { path: "supervisor", loadChildren: "./dispatcher/supervisor.module#SupervisorModule" },
  { path: "assign-item", loadChildren: "./assign-items/assign-items.module#AssignItemsModule" },
  //{ path: "customers", loadChildren: () => CustomersModule },
  { path: "**", component: PageNotFoundComponentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {
}

export const RoutingComponents = [];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);
