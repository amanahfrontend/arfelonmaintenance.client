import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EditDisAndProbModalComponent } from "./edit-dis-and-prob-modal/edit-dis-and-prob-modal.component";
import { DataTable, MessageService } from 'primeng/primeng';
import { SettingsDispatchersService } from './settings-dispatchers.service';

@Component({
  selector: 'app-setting-dispatchers',
  templateUrl: './setting-dispatchers.component.html',
  styleUrls: ['./setting-dispatchers.component.css']
})
export class SettingDispatchersComponent implements OnInit {
  disWithProbs: any[];
  allDispatchers: any = [];
  toggleLoading: boolean;
  allAreas;
  dispatcherList = [];
  allProblems: any = [];
  isLoading: boolean;

  page = {
    //The number of elements in the page
    size: 5,
    //The total number of elements
    totalElements: 0,
    //The total number of pages
    totalPages: 0,
    //The current page number
    pageNumber: 0,

    offset: 1
  };

  @ViewChild('dt') dataTable: DataTable;
  @ViewChild('el') el: ElementRef;

  paginationobj = {
    pageNo: 1,
    pageSize: 5,
    count: 0
  };

  searchObj = {
    dispatchers: [],
    orderProblems: [],
    areas: []
  };

  numberOfServicesCalled: number = 0;
  numberOfServicesFinished: number = 0;

  constructor(private lookup: LookupService,
    private modalService: NgbModal,
    private settingsDispatchersService: SettingsDispatchersService,
    private messageService: MessageService) {
  }

  onSearch() {
    this.getDispatchers();
  }


  setPage(e) {

    this.page.pageNumber = e.offset;
    this.paginationobj.count = e.count;
    this.paginationobj.pageNo = e.offset + 1;
    this.getDispatchers();
  }

  onPageChangeGetData(event: { first: number, rows: number }) {

    this.paginationobj.pageNo = (event.first / event.rows) + 1;
    this.getDispatchers();
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.getDispatchers();
    this.getAllProblems();
    this.getAreas();
    this.getDispatcherList();
    this.settingsDispatchersService.closeModal.subscribe(() => {
      this.isLoading = true;
      this.getDispatchers();
    });
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  checkIfServicesFinished() {
    this.numberOfServicesFinished++;

    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.toggleLoading = false;
      this.isLoading = false;
    }
  }


  onSelect(e) {
    console.log(e.selected[0]);
  }

  AddNewProblem() {
    let modalRef = this.modalService.open(EditDisAndProbModalComponent);
    modalRef.result.then(
      () => {
        this.getDispatchers();
      })
      .catch(() => {
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Edited!',
          detail: "You didn't submit!"
        });
      });
  }


  getDispatcherList() {
    this.lookup.getALLDispatcherSettingsList().subscribe(
      (data) => {
        this.dispatcherList = data;
        
        this.dispatcherList.map((dis, i) => {
          this.dispatcherList[i] = {
            label: dis.name,
            value: dis.name
          };
        });
      },
      err => {
        console.error("error", err);
      });
  }


  // setupAreasForFilter() {
  //   this.allAreas = [];
  //   this.disWithProbs.map((dis) => {
  //     if (!this.allAreas.includes(dis.area) && dis.area != '-') {
  //       this.allAreas.push(dis.area);
  //     }
  //   });
  //   this.allAreas.map((area, i) => {
  //     this.allAreas[i] = {
  //       label: area,
  //       value: area
  //     }
  //   });
  //   console.log(this.allAreas)
  // }

  getAreas() {
    this.seriveCalled();
    this.lookup.getALLareasEnUrl().subscribe(
      (data) => {
        this.allAreas = data.data;

        this.allAreas.map((area, i) => {
          this.allAreas[i] = {
            label: area.name,
            value: area.name
          };
        });
        this.checkIfServicesFinished();
      },
      err => {
      });
  }

  getAllProblems() {
    this.lookup.getAllOrderProblem().subscribe(
      (problems) => {
        this.allProblems = problems.data;

        this.allProblems.map((prob, i) => {
          this.allProblems[i] = {
            label: prob.name,
            value: prob.name
          };
        });

      },
      err => {
      });
  }

  getAllDispatchers() {
    this.seriveCalled();
    this.lookup.getAllDispatchers().subscribe(
      (dispatchers) => {

        if (this.disWithProbs.length) {
          let result = this.disWithProbs.map(a => a.fK_Dispatcher_Id);
          var unique = result.filter((v, i, a) => a.indexOf(v) === i);
          this.allDispatchers = [];
          dispatchers.data.map((dis) => {
            if (!~unique.indexOf(dis.id)) {
              this.allDispatchers.push(dis);
            }
          });

        } else {
          this.allDispatchers = dispatchers.data;
        }

        this.allDispatchers.map((dis, i) => {
          this.allDispatchers[i] = {
            fK_Dispatcher_Id: dis.id,
            dispatcher: {
              id: dis.id,
              userName: dis.name
            },
            area: '-',
            orderProblem: {
              name: '-'
            }
          };
        });

        this.checkIfServicesFinished();
      },
      err => {

      });
  }

  openEditModal(dispatcher, event) {
    event.target.parentElement.parentElement.blur();
    // console.warn(e)
    let modalRef = this.modalService.open(EditDisAndProbModalComponent);
    modalRef.componentInstance.data = dispatcher;
    modalRef.result
      .then(() => {

        this.getDispatchers();
      })
      .catch(() => {
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Edited!',
          detail: "You didn't change the old value"
        });
      });
  }

  // remove(dis) {
  //   console.log(dis);
  //   this.lookup.removeDisByProb(dis.id).subscribe(() => {
  //     console.log('success');
  //     this.disWithProbs = this.disWithProbs.filter((disWIthProb) => {
  //       return disWIthProb.id != dis.id;
  //     })
  //   },
  //     err => {
  //       console.log('failed');
  //     })
  // }


  onSelectArea(e) {
    console.log(e);
  }


  getDispatchers() {
    this.seriveCalled();
    let objToPost = {
      dispatcherNames: this.searchObj.dispatchers,
      orderProblemNames: this.searchObj.orderProblems,
      areaNames: this.searchObj.areas,
      pageInfo: this.paginationobj
    };

    this.lookup.getAllDispatcherSettingspaginated(objToPost).subscribe(
      (diswithProbs) => {

        this.page.totalElements = diswithProbs.data.count;
        this.disWithProbs = diswithProbs.data.data;

        this.getAllDispatchers();
        this.checkIfServicesFinished();
      },
      err => {
        console.log('failed');
      });
  }
}
