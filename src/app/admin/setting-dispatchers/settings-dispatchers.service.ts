import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/subject';

@Injectable({
  providedIn: 'root'
})
export class SettingsDispatchersService {

  constructor() { }
  closeModal = new Subject();
}
