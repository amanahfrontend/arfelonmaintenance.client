import {RouterModule, Routes} from '@angular/router';
import {SharedModuleModule} from '../shared-module/shared-module.module';
import { NgDragDropModule } from 'ng-drag-drop';
import {NgModule} from '@angular/core';
import {AdminMainPageComponent} from './admin-main-page/admin-main-page.component';
import {AdminUploadResourcesComponent} from './admin-upload-resources/admin-upload-resources.component';
import {MultiSelectModule} from 'primeng/components/multiselect/multiselect';
import { FormsModule } from '@angular/forms';
import { MapSettingsComponent } from './map-settings/map-settings.component';

const routes: Routes = [
  {
    path: '',
    component: AdminMainPageComponent,
    // canActivate: [AuthGuardGuard],
    data: {roles: ['Admin']}
  }
];

@NgModule({
  imports: [
    SharedModuleModule,
    MultiSelectModule,
    FormsModule,
    RouterModule.forChild(routes),
    NgDragDropModule.forRoot()
  ],
  declarations: [
    AdminMainPageComponent,
    AdminUploadResourcesComponent
  ],
  entryComponents: [
  ]
})

export class AdminModule {
}
