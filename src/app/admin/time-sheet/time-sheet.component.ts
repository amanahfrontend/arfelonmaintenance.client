import { Component, OnInit } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/primeng';
import { BaseUrlOrder } from '../../api-module/services/globalPath';

@Component({
  selector: 'app-time-sheet',
  templateUrl: './time-sheet.component.html',
  styleUrls: ['./time-sheet.component.css']
})
export class TimeSheetComponent implements OnInit {
  timeObj = {
    dateFrom: '',
    dateTo: '',
    costCenter: '',
    pfNo: '',
  };

  rows = [];
  columns = [
    { prop: 'dayDate' },
    { name: 'name' },
    { name: 'pfNo' },
    { name: 'time' },
    { name: 'workType' },
  ];

  loadingIndicator: boolean;

  constructor(private lookUp: LookupService, private MessageService: MessageService) { }

  ngOnInit() {
  }

  onExport() {
    this.lookUp.exportTimeSheetResults(this.rows).subscribe((results: any) => {
      console.log(results._body.substring(29));
      let link = document.createElement("a");
      link.download = "TimeSheet";
      let baseUrl = BaseUrlOrder.substring(0, 75);
      link.href = baseUrl + results._body.substring(29);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    })
  }

  ShowTime(timeObj) {
    if (timeObj.dateFrom == '' || timeObj.dateTo == '') {
      this.MessageService.add({
        severity: 'error',
        summary: 'Failed',
        detail: `Enter Valid Dates.`
      })
    } else {
      this.loadingIndicator = true;
      this.lookUp.GetTimeSheet(this.timeObj).subscribe((Timesheetobj) => {
        this.loadingIndicator = false;
        Timesheetobj.data.map((item) => {
          item.time = item.time.toFixed(2);
          if (item.time[7] == ":") {
            item.time = item.time.substring(0, item.time.length - 1);
          }
          item.key_Date = item.key_Date.slice(0, 10);
        });
        this.rows = Timesheetobj.data;
      });

    }

  }
}


