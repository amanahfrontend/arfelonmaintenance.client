import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from 'primeng/components/common/messageservice';
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { languagesComponent } from '../../shared-module/shared/languageModel/languageModel.component';
import { userManageModelComponent } from '../../shared-module/shared/userManageModel/userManageModel.component';

@Component({
  selector: 'app-userManagment',
  templateUrl: './userManagment.component.html',
  styleUrls: ['./userManagment.component.css']
})
export class userManagmentComponent implements OnInit, OnChanges, OnDestroy {
  @Input() role: string;
  @Input() getUserData: string;
  dataSubscription: Subscription;
  deleteDataSubscription: Subscription;
  modalRef: any;
  rows: any[];
  toggleLoading: boolean;
  itemsRoles: any = [];

  constructor(
    private lookUp: LookupService,
    private messageService: MessageService,
    private modalService: NgbModal) {
  }

  ngOnInit() {
    this.rows = [];
  }

  ngOnChanges() {
    this.toggleLoading = true;
    !this.rows && (this.rows = []);

    if (this.role === 'Admin' && this.getUserData === 'Admin') {
      console.log(this.role);
      this.dataSubscription = this.lookUp.getAllUserAmin().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
          console.log('admin' + JSON.stringify(this.rows));
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.role == 'Dispatcher' && this.getUserData == 'Dispatcher') {

      this.dataSubscription = this.lookUp.getAllDispatchers().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.role == 'Supervisor' && this.getUserData == 'Supervisor') {
      console.log(this.role);
      this.dataSubscription = this.lookUp.getAllSuperVisor().subscribe((data) => {
        this.toggleLoading = false;
        this.rows = data.data;
        console.log(this.rows);
      },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.role == 'Engineer' && this.getUserData == 'Engineer') {

      this.dataSubscription = this.lookUp.getAllEngineers().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.role == 'IC Agent' && this.getUserData == 'IC Agent') {

      this.dataSubscription = this.lookUp.getAllIcAgents().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.role == 'Foreman' && this.getUserData == 'Foreman') {
      this.dataSubscription = this.lookUp.getAllForemans().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.role == 'Technician' && this.getUserData == 'Technician') {
      this.dataSubscription = this.lookUp.getAllTechnicians().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.role == 'Manager' && this.getUserData == 'Manager') {
      this.dataSubscription = this.lookUp.getAllMangaer().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.role == 'Material Controller' && this.getUserData == 'Material Controller') {
      this.dataSubscription = this.lookUp.getAllMaterial().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
        },
        err => {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }

  }

  ngOnDestroy() {
    this.dataSubscription && this.dataSubscription.unsubscribe();
    this.deleteDataSubscription && this.deleteDataSubscription.unsubscribe();
  }

  add() {
    console.log('role' + this.role);
    this.openModal({}, `Add ${this.role}`, this.role);
    this.modalRef.result.then(
      (newValue) => {

        // console.log(newValue);

        if (this.role == 'Admin') {
          this.ngOnChanges();
        }
        else if (this.role == 'Dispatcher') {
          this.ngOnChanges();
        }
        else if (this.role == 'Supervisor') {
          this.ngOnChanges();
        }
        else if (this.role == 'Engineer') {
          this.ngOnChanges();
        }
        else if (this.role == 'Driver') {
          this.ngOnChanges();
        }
        else if (this.role == 'Foreman') {
          this.ngOnChanges();
        }
        else if (this.role == 'Technician') {
          this.ngOnChanges();
        }
        else if (this.role == 'Manager') {
          this.ngOnChanges();
        }
        else if (this.role == 'Material Controller') {
          this.ngOnChanges();
        }

      })
      .catch((result) => {
        //console.log('nothing added');
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Added!',
          detail: "You didn't saved new value."
        });
      });
  }

  edit(row) {

    this.openModal(Object.assign({}, row), 'Edit', this.role);

    this.modalRef.result.then(
      (editedValue) => {
        console.log(editedValue);
        if (this.role == 'Admin') {
          this.ngOnChanges();
        }
        else if (this.role == 'Dispatcher') {
          console.log('Dispatcher' + row);
          this.ngOnChanges();
        }
        else if (this.role == 'Engineer') {
          this.ngOnChanges();
        }
        else if (this.role == 'Supervisor') {
          this.ngOnChanges();
        }
        else if (this.role == 'Driver') {
          this.ngOnChanges();
        }
        else if (this.role == 'Foreman') {
          this.ngOnChanges();
        }
        else if (this.role == 'Technician') {
          this.ngOnChanges();
        }
        else if (this.role == 'Manager') {
          this.ngOnChanges();
        }
        else if (this.role == 'MaterialController') {
          this.ngOnChanges();
        }

      })
      .catch((result) => {
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Edited!',
          detail: "You didn't change the old value"
        });
      });
  }

  addEditLang(row) {
    this.openModalLanguage(Object.assign({}, row), 'language', this.role);

    this.modalRef.result.then(
      (newValue) => {
        this.ngOnChanges();

      })
      .catch((result) => {
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Edited!',
          detail: "You didn't change the old value"
        });
      });
  }

  openModal(data, header, role?) {
    console.log('open Modal');
    this.modalRef = this.modalService.open(userManageModelComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.role = role;
    this.modalRef.componentInstance.data = data;
  }

  openModalLanguage(data, header, role?) {
    console.log('open Modal Language');
    this.modalRef = this.modalService.open(languagesComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.role = role;
    this.modalRef.componentInstance.data = data;
  }

}
