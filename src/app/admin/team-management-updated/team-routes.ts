import { Routes } from "@angular/router";
import { AddTeamComponent } from "./add-team/add-team.component";
import { EditTeamComponent } from "./edit-team/edit-team.component";
import { VehicleComponent } from "./vehicle/vehicle.component";
import { TeamManagementUpdatedComponent } from "./team-management-updated.component";

export const teamRoutes: Routes = [
    {
        path: "", component: TeamManagementUpdatedComponent, children: [
            {path: "" , redirectTo: '/team-management/add', pathMatch: "full"},
            { path: 'add', component: AddTeamComponent },
            { path: 'update', component: EditTeamComponent },
            { path: 'vehicle', component: VehicleComponent }
        ]
    },
];