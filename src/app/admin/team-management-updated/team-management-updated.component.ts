import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-team-management-updated',
  templateUrl: './team-management-updated.component.html',
  styleUrls: ['./team-management-updated.component.css']
})
export class TeamManagementUpdatedComponent implements OnInit {

  selectedTab: string = "add";
  constructor() { }

  ngOnInit() {
  }

  selectTab(tabName:string){
    this.selectedTab = tabName;
  }


}