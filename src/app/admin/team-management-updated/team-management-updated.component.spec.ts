import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamManagementUpdatedComponent } from './team-management-updated.component';

describe('TeamManagementUpdatedComponent', () => {
  let component: TeamManagementUpdatedComponent;
  let fixture: ComponentFixture<TeamManagementUpdatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamManagementUpdatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamManagementUpdatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
