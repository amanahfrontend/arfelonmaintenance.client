import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.css']
})
export class EditTeamComponent implements OnInit {
  userObj: any = {
    name: "",
  };
  divisions: any = [];
  isSuccess: boolean = false;
  drivers: any = [];
  formans: any = [];
  divisionObj: any = {
    id: 0
  };
  driverObj: any = {};
  formanObj: any = {};
  objToPost: any = {};
  unassignedMembers: any = [];
  shiftObj: any = { id: null };
  shifts: any = [];
  memberToPost: any;
  memberTeamToPost: any;
  teamToPost: Array<any> = [];
  teams: any = [];
  unassignedVehicles: Array<any> = [];
  currentTeam: any = { members: [] };
  numberOfServicesFinished: number = 0;
  isLoading: boolean = false;
  PF: string = "";
  plateNo: string = "";
  teamName: string = "";
  numberOfServicesCalled = 0;
  disableChangeDivision: boolean = false;
  currentlyUnassignedMembers = [];
  curentlyUnassignedVehicles = [];

  constructor(private lookUp: LookupService, private messageService: MessageService) { }


  ngOnInit() {
    this.getDivisions();
    this.getUnassignedVehicles();
    this.getTeams();
    this.getAllFormen();
    this.getShifts();
  }


  assinTeamName(name: string) {
    this.userObj.name = name;
  }


  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  compareByOptionId(idFist, idSecond) {
    return idFist && idSecond && idFist.id == idSecond.id;
  }

  getShifts() {
    this.seriveCalled();

    this.lookUp.getShift().subscribe(
      result => {
        this.shifts = result.data;

        this.shiftObj = this.shifts[0];
        this.checkIfServicesFinished();
      }, error => {
        console.log('error');
      });
  }

  getTeams() {
    this.teams = [];
    this.seriveCalled();

    this.lookUp.getAllTeams().subscribe(
      result => {
        this.teams = result.data;

        this.checkIfServicesFinished();
      }, error => {
        console.log('error');
      });
  }

  getDivisions() {
    this.seriveCalled();

    this.lookUp.getDivisionEnUrl().subscribe(
      result => {
        this.divisions = result.data;
        this.divisionObj = this.divisions[0];
        this.checkIfServicesFinished();
      }, error => {
        console.log('error');
      });
  }

  getUnassignedVehicles() {
    this.seriveCalled();

    this.lookUp.getAllUnassignedVehicles().subscribe(
      result => {
        this.unassignedVehicles = result.data;

        this.unassignedVehicles.map((v, i) => {
          if (this.currentTeam.vehicle && this.currentTeam.vehicle.memberParentName == v.memberParentName) {
            this.unassignedVehicles.splice(i, 1);
          }
        });

        this.checkIfServicesFinished();
      }, error => {
        console.log('error');
      });
  }

  getUnassignedMembers(divisionId) {
    this.seriveCalled();

    this.lookUp.getAllUnassignedMempersByDivision(divisionId).subscribe(
      result => {
        this.unassignedMembers = result.data;
        for (let i = (this.unassignedMembers.length - 1); i >= 0; i--) {
          if (this.unassignedMembers[i].isDriver && this.currentTeam.driver && this.currentTeam.driver.memberParentId == this.unassignedMembers[i].memberParentId) {
            this.unassignedMembers.splice(i, 1);
          }
          if (!this.unassignedMembers[i].isDriver && this.currentTeam.members.length > 0) {
            this.currentTeam.members.map((member: any) => {
              if (member.memberParentId == this.unassignedMembers[i].memberParentId) {
                this.unassignedMembers.splice(i, 1);
              }
            });
          }
        }
        this.checkIfServicesFinished();
      }, error => {
        console.log('error');
      });
  }

  getAllFormen() {
    this.seriveCalled();

    this.lookUp.getAllForemans().subscribe(
      result => {
        this.formans = result.data;
        this.formanObj = this.formans[0];
        this.checkIfServicesFinished();
      }, error => {
        console.log('error');
      });
  }



  assignVehicle(vehicle: any, index: number) {
    if (this.currentTeam.vehicle && this.currentTeam.vehicle.memberParentName) {
      this.unassignedVehicles.push(this.currentTeam.vehicle);
    }
    this.currentTeam.vehicle = vehicle;
    this.unassignedVehicles.splice(index, 1);
    this.curentlyUnassignedVehicles = this.unassignedVehicles;
  }

  addTech(tech: any, i: number) {
    this.disableChangeDivision = true;
    if (tech.isDriver) {
      if (this.currentTeam.driver) {
        this.unassignedMembers.push(this.currentTeam.driver);
      }
      this.currentTeam.driver = tech;
    } else {
      this.currentTeam.members.push(tech);
    }
    this.unassignedMembers.splice(i, 1);
    this.currentlyUnassignedMembers = this.unassignedMembers;
  }

  removeDriver(driver) {
    this.unassignedMembers.unshift(driver);
    if (this.userObj.name == this.currentTeam.driver.memberParentName) {
      this.userObj.name = "";
    }
    if (this.currentTeam.members.length == 0) {
      this.disableChangeDivision = false;
    }
    this.currentTeam.driver = null;
    this.currentlyUnassignedMembers = this.unassignedMembers;
  }

  removeTeamMember(member: any, index: number) {

    this.unassignedMembers.unshift(member);
    this.currentTeam.members.splice(index, 1);

    if (this.userObj.name == member.memberParentName) {
      this.userObj.name = "";
    }

    if (this.currentTeam.members.length == 0 && !this.currentTeam.driver) {
      this.disableChangeDivision = false;
    }

    this.currentlyUnassignedMembers = this.unassignedMembers;
  }

  removeVehicle(vehicle) {

    this.unassignedVehicles.unshift(vehicle);
    this.currentTeam.vehicle = null;
    this.curentlyUnassignedVehicles = this.unassignedVehicles;
  }

  getTechsBeforSearch() {
    this.PF = "";
    this.unassignedMembers = this.currentlyUnassignedMembers;
  }

  getVehiclesBeforeSearch() {
    this.plateNo = "";
    this.unassignedVehicles = this.curentlyUnassignedVehicles;
  }

  onTechSearch() {
    this.seriveCalled();
    this.lookUp.getTechnicianByPF(this.PF).subscribe((result) => {
      this.unassignedMembers = result.data;
      for (let i = (this.unassignedMembers.length - 1); i >= 0; i--) {
        if (this.unassignedMembers[i].isDriver && this.currentTeam.driver && this.currentTeam.driver.memberParentId == this.unassignedMembers[i].memberParentId) {
          this.unassignedMembers.splice(i, 1);
        }
        if (!this.unassignedMembers[i].isDriver && this.currentTeam.members.length > 0) {
          this.currentTeam.members.map((member: any) => {
            if (member.memberParentId == this.unassignedMembers[i].memberParentId) {
              this.unassignedMembers.splice(i, 1);
            }
          });
        }
      }
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  onTeamSearch() {
    this.seriveCalled();
    this.teams = [];
    this.lookUp.getTeamByName(this.teamName).subscribe((result) => {
      this.teams = result.data;
      this.checkIfServicesFinished();
      this.getTeams();
    }, error => {
      console.log('error');
    });
  }

  onDeleteTeam(id) {
    this.isLoading = true;
    let membersIdArray = [];

    this.lookUp.getMembersByTeamId(id).subscribe((result) => {
      result.data.map((member) => {
        membersIdArray.push(member.id);
      });

      this.lookUp.unAssginMembers(membersIdArray).subscribe((response) => {

        this.lookUp.deleteTeam(id).subscribe((response) => {
          this.getTeams();
        });
      });
    });
  }


  onVehicleSearch() {
    this.seriveCalled();
    this.lookUp.getVehicleByPlateNo(this.plateNo).subscribe((result) => {
      this.unassignedVehicles = result.data;
      this.unassignedVehicles.map((v, i) => {
        if (this.currentTeam.vehicle && this.currentTeam.vehicle.memberParentName == v.memberParentName) {
          this.unassignedVehicles.splice(i, 1);
        }
      });
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  GetMembersByTeamId(id) {
    this.seriveCalled();
    this.currentTeam.members = [];

    this.lookUp.getMembersByTeamId(id).subscribe((result) => {
      this.checkIfServicesFinished();

      result.data.map((member) => {
        if (member.memberTypeName === 'Vehicle') {
          this.currentTeam.vehicle = member;
        } else if (member.isDriver) {
          this.currentTeam.driver = member;
          this.disableChangeDivision = true;
        } else {
          this.currentTeam.members.push(member);
          this.disableChangeDivision = true;
        }
      });
    }, error => {
      console.log('error');
    });
  }



  onSelectTeam(team) {
    this.formanObj.id = team.foremanId;
    this.formanObj.name = team.foremanName;
    this.divisionObj.name = team.divisionName;
    this.divisionObj.id = team.divisionId;
    this.shiftObj.id = team.shiftId || this.shifts[0].id;
    this.userObj.name = team.name;
    this.currentTeam.members = [];
    this.currentTeam.driver = null;
    this.currentTeam.id = team.id;
    this.GetMembersByTeamId(team.id);
    this.getUnassignedMembers(team.divisionId);
  }


  onSubmit() {
    if (!this.userObj.name) {
      this.messageService.add({
        severity: 'error',
        summary: 'Failed!',
        detail: 'Select name to submit'
      });
    } else if (!this.divisionObj.id && this.divisionObj.id !== 0) {
      this.messageService.add({
        severity: 'error',
        summary: 'Failed!',
        detail: 'Select Division to submit'
      });
    } else if (!this.formanObj.id && this.formanObj.id !== 0) {
      this.messageService.add({
        severity: 'error',
        summary: 'Failed!',
        detail: 'Select Forman to submit'
      });
    } else if (!this.shiftObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: 'Failed!',
        detail: 'Select Shift to submit'
      });
      // } else if (!this.currentTeam.driver) {
      //   this.messageService.add({
      //     severity: 'error',
      //     summary: 'Failed!',
      //     detail: 'Select Driver to submit'
      //   });
      // } else if (!this.currentTeam.vehicle) {
      //   this.messageService.add({
      //     severity: 'error',
      //     summary: 'Failed!',
      //     detail: 'Select Vehicle to submit'
      //   });
      // } else if (this.currentTeam.divisionObj.id !== this.currentTeam.driver.divisionId) {
      //   this.messageService.add({
      //     severity: 'error',
      //     summary: 'Failed!',
      //     detail: 'Select driver from same division to submit'
      //   });
    } else {
      this.teamToPost = [];
      this.objToPost = {
        divisionId: this.divisionObj.id,
        divisionName: this.divisionObj.name,
        foremanId: this.formanObj.id,
        foremanName: this.formanObj.name,
        name: this.userObj.name,
        id: this.currentTeam.id,
        ShiftId: this.shiftObj.id
      };
      if (this.currentTeam.vehicle) {
        this.teamToPost.push(this.currentTeam.vehicle.id);
      }
      if (this.currentTeam.driver) {
        this.teamToPost.push(this.currentTeam.driver.id);
      }
      this.currentTeam.members.forEach(member => {
        if (this.objToPost.divisionId === member.divisionId) {
          this.teamToPost.push(member.id);
        } else {
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: 'Select Technicians from same division to submit'
          });
          this.isLoading = true;
          return false;
        }

      });

      this.seriveCalled();

      this.lookUp.updateTeam(this.objToPost).subscribe((teamCreationResponse) => {

        let teamIdWithMembers = {
          teamId: this.objToPost.id,
          members: this.teamToPost
        };

        this.lookUp.assginMembersToTeam(teamIdWithMembers).subscribe(
          (assignTeamMembersResponse) => {
            
            this.checkIfServicesFinished();
            this.getTeams();
            this.messageService.add({
              severity: 'success',
              summary: 'Success!',
              detail: 'Team Updated Successfully!'
            });
          }, (error) => {
            console.log(error);
            this.messageService.add({
              severity: 'error',
              summary: 'Failed!',
              detail: error
            });
          }
        );
      }, (error) => {
        console.log(error);
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: error
        });
      });
    }


  }
}
