import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.css']
})
export class AddTeamComponent implements OnInit {

  userObj: any = {
    name: "",
  };
  statuses: any[];
  divisions: any = [];
  isSuccess: boolean = false;
  drivers: any = [];
  formans: any = [];
  shifts: any = [];
  superVisors: any = [];
  dispatchers: any = [];
  engineers: any = [];
  divisionObj: any = {};
  shiftObj: any = {};
  engObj: any = {};
  supervisorObj: any = {};
  dispatcherObj: any = {};
  statusObj: any = {};
  driverObj: any = {};
  formanObj: any = {};
  objToPost: any = {};
  unassignedMembers: any = [];
  memberToPost: any;
  memberTeamToPost: any;
  teamToPost: Array<any> = [];
  teams: any = [];
  unassignedVehicles: Array<any> = [];
  currentTeam: any = { members: [] };
  numberOfServicesFinished: number = 0;
  isLoading: boolean = false;
  PF: string = "";
  plateNo: string = "";
  disableChangeDivision: boolean = false;
  numberOfServicesCalled: number = 0;

  constructor(private lookUp: LookupService, private messageService: MessageService) { }


  ngOnInit() {
    this.getDivisions();
    this.getUnassignedMembers(1);
    this.getUnassignedVehicles();
    this.getAllFormen();
    this.getShifts();
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }

  assinTeamName(name: string) {
    this.userObj.name = name;
  }

  compareByOptionId(idFist, idSecond) {
    return idFist && idSecond && idFist.id == idSecond.id;
  }

  getShifts() {
    this.seriveCalled();
    this.lookUp.getShift().subscribe(result => {
      this.shifts = result.data;

      this.shiftObj = this.shifts[0];
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  getDivisions() {
    this.seriveCalled();
    this.lookUp.getDivisionEnUrl().subscribe(result => {
      this.divisions = result.data;
      this.divisionObj = this.divisions[0];
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  getUnassignedVehicles() {
    this.seriveCalled();
    this.lookUp.getAllUnassignedVehicles().subscribe(result => {
      this.unassignedVehicles = result.data;
      this.unassignedVehicles.map((v, i) => {
        if (this.currentTeam.vehicle && this.currentTeam.vehicle.memberParentName == v.memberParentName) {
          this.unassignedVehicles.splice(i, 1);
        }
      });
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  removeMember(unAssignedMember, i) {
    if (unAssignedMember.isDriver && this.currentTeam.driver && this.currentTeam.driver.memberParentId == unAssignedMember.memberParentId) {
      this.unassignedMembers.splice(i, 1);
    }
    if (!unAssignedMember.isDriver && this.currentTeam.members.length > 0) {
      this.currentTeam.members.map((member: any) => {
        if (member.memberParentId == unAssignedMember.memberParentId) {
          this.unassignedMembers.splice(i, 1);
        }
      });
    }
  }

  getUnassignedMembers(divisionId) {
    this.seriveCalled();
    this.isLoading = true;
    this.lookUp.getAllUnassignedMempersByDivision(divisionId).subscribe(result => {
      this.unassignedMembers = result.data;
      for (let i = (this.unassignedMembers.length - 1); i >= 0; i--) {
        if (this.unassignedMembers[i].isDriver && this.currentTeam.driver && this.currentTeam.driver.memberParentId == this.unassignedMembers[i].memberParentId) {
          this.unassignedMembers.splice(i, 1);
        }
        if (!this.unassignedMembers[i].isDriver && this.currentTeam.members.length > 0) {
          this.currentTeam.members.map((member: any) => {
            if (member.memberParentId == this.unassignedMembers[i].memberParentId) {
              this.unassignedMembers.splice(i, 1);
            }
          });
        }
      }
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  getAllFormen() {
    this.seriveCalled();
    this.lookUp.getAllForemans().subscribe(result => {
      this.formans = result.data;
      this.formanObj = this.formans[0];
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }


  assignVehicle(vehicle: any, index: number) {
    if (this.currentTeam.vehicle && this.currentTeam.vehicle.memberParentName) {
      this.unassignedVehicles.push(this.currentTeam.vehicle);
    }
    this.currentTeam.vehicle = vehicle;
    this.unassignedVehicles.splice(index, 1);
  }

  addTech(tech: any, i: number) {
    this.disableChangeDivision = true;
    if (tech.isDriver) {
      if (this.currentTeam.driver) {
        this.unassignedMembers.push(this.currentTeam.driver);
      }
      this.currentTeam.driver = tech;
    } else {
      this.currentTeam.members.push(tech);
    }
    this.unassignedMembers.splice(i, 1);
  }

  removeDriver(driver) {
    this.unassignedMembers.unshift(driver);
    if (this.userObj.name == this.currentTeam.driver.memberParentName) {
      this.userObj.name = "";
    }
    if (this.currentTeam.members.length == 0) {
      this.disableChangeDivision = false;
    }
    this.currentTeam.driver = null;
  }

  removeTeamMember(member: any, index: number) {

    this.unassignedMembers.unshift(member);
    this.currentTeam.members.splice(index, 1);
    if (this.userObj.name == member.memberParentName) {
      this.userObj.name = "";
    }
    if (this.currentTeam.members.length == 0 && !this.currentTeam.driver) {
      this.disableChangeDivision = false;
    }
  }

  removeVehicle(vehicle) {

    this.unassignedVehicles.unshift(vehicle);
    this.currentTeam.vehicle = null;
  }

  onTechSearch() {
    this.seriveCalled();
    this.lookUp.getTechnicianByPF(this.PF).subscribe((result) => {
      this.unassignedMembers = result.data;
      for (let i = (this.unassignedMembers.length - 1); i >= 0; i--) {
        if (this.unassignedMembers[i].isDriver && this.currentTeam.driver && this.currentTeam.driver.memberParentId == this.unassignedMembers[i].memberParentId) {
          this.unassignedMembers.splice(i, 1);
        }
        if (!this.unassignedMembers[i].isDriver && this.currentTeam.members.length > 0) {
          this.currentTeam.members.map((member: any) => {
            if (member.memberParentId == this.unassignedMembers[i].memberParentId) {
              this.unassignedMembers.splice(i, 1);
            }
          });
        }
      }
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }

  onVehicleSearch() {
    this.isLoading = true;
    this.seriveCalled();
    this.lookUp.getVehicleByPlateNo(this.plateNo).subscribe((result) => {
      this.unassignedVehicles = result.data;
      this.unassignedVehicles.map((v, i) => {
        if (this.currentTeam.vehicle && this.currentTeam.vehicle.memberParentName == v.memberParentName) {
          this.unassignedVehicles.splice(i, 1);
        }
      });
      this.checkIfServicesFinished();
    }, error => {
      console.log('error');
    });
  }


  onSubmit() {
    if (!this.userObj.name) {
      this.messageService.add({
        severity: 'error',
        summary: 'Failed!',
        detail: 'Select name to submit'
      });
    } else if (!this.divisionObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: 'Failed!',
        detail: 'Select Division to submit'
      });
      // }else if (!this.engObj.id) {
      //   this.messageService.add({
      //     severity: 'error',
      //     summary: 'Failed!',
      //     detail: 'Select Engineer to submit'
      //   });
      // }else if (!this.supervisorObj.id) {
      //   this.messageService.add({
      //     severity: 'error',
      //     summary: 'Failed!',
      //     detail: 'Select Supervisor to submit'
      //   });
      // }else if (!this.dispatcherObj.id) {
      //   this.messageService.add({
      //     severity: 'error',
      //     summary: 'Failed!',
      //     detail: 'Select Dispatcher to submit'
      //   });
    } else if (!this.formanObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: 'Failed!',
        detail: 'Select Forman to submit'
      });
    } else if (!this.shiftObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: 'Failed!',
        detail: 'Select Shift to submit'
      });
      // }else if (!this.currentTeam.driver) {
      //   this.messageService.add({
      //     severity: 'error',
      //     summary: 'Failed!',
      //     detail: 'Select Driver to submit'
      //   });
      // }else if (!this.currentTeam.vehicle) {
      //   this.messageService.add({
      //     severity: 'error',
      //     summary: 'Failed!',
      //     detail: 'Select Vehicle to submit'
      //   });
    } else {
      this.teamToPost = [];
      this.objToPost = {
        divisionId: this.divisionObj.id,
        divisionName: this.divisionObj.name,
        foremanId: this.formanObj.id,
        foremanName: this.formanObj.name,
        shiftId: this.shiftObj.id,
        name: this.userObj.name
      };
      if (this.currentTeam.vehicle) {
        this.teamToPost.push(this.currentTeam.vehicle.id);
      }

      if (this.currentTeam.driver) {
        this.teamToPost.push(this.currentTeam.driver.id);
      }
      this.currentTeam.members.forEach(member => {
        this.teamToPost.push(member.id);
      });

      this.isLoading = true;

      this.seriveCalled();
      this.lookUp.postTeam(this.objToPost).subscribe(
        (teamCreationResponse) => {

          let teamIdWithMembers = {
            teamId: teamCreationResponse.data.id,
            members: this.teamToPost
          };

          this.lookUp.assginMembersToTeam(teamIdWithMembers).subscribe(
            (assignTeamMembersResponse) => {

              this.checkIfServicesFinished();
              this.currentTeam.driver = null;
              this.currentTeam.vehicle = null;
              this.currentTeam.members = [];
              this.userObj.name = "";
              this.messageService.add({
                severity: 'success',
                summary: 'Success!',
                detail: 'Team Added Successfully!'
              });
            });
        },
        (error) => {
          console.log(error);
          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: error
          });
        }
      );
    }

  }
}
