import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VehicleUpdateModalComponent } from './vehicle-update-modal/vehicle-update-modal.component';
import { MessageService } from 'primeng/primeng';
import { VehicleService } from './vehicle.service';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {
  vehicles: any[];
  columns: any[];
  editing: any = {};
  toggleLoading: boolean;
  isLoading: boolean;
  constructor(private lookup: LookupService, private modalService: NgbModal, private messageService: MessageService, private vehicleService: VehicleService) { }

  ngOnInit() {
    this.toggleLoading = true;
    this.lookup.getAllVehicles().subscribe((results) => {
      this.toggleLoading = false;
      this.vehicles = results.data;
      console.log(this.vehicles);
    }, error=>{
      this.messageService.add({
        severity: 'error',
        summary: 'error!',
        detail: error
      });
    });
    this.vehicleService.getVehicle.subscribe(() => {
      this.isLoading = true;
      this.lookup.getAllVehicles().subscribe((results) => {
        this.isLoading = false;
        this.vehicles = results.data;
        console.log(this.vehicles);
      }, error=>{
        this.messageService.add({
          severity: 'error',
          summary: 'error!',
          detail: error
        });
      });
    });
  }

  onSelect(e) {
    console.log(e);
  }

  onRowClick(row) {
    console.log(row);
    // this.modalService.open(VehicleUpdateModalComponent);
    let modalRef = this.modalService.open(VehicleUpdateModalComponent);
    modalRef.componentInstance.data = row;
    modalRef.result
      .then(() => {
        console.log('then');
      })
      .catch(() => {
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Edited!',
          detail: "You didn't change the old value"
        });
      })
  }

  add() {
    // this.modalService.open(VehicleUpdateModalComponent);
    let modalRef = this.modalService.open(VehicleUpdateModalComponent);
    modalRef.result
      .then(() => {
        console.log('then');
      })
      .catch(() => {
        this.messageService.add({
          severity: 'info',
          summary: 'Nothing Edited!',
          detail: "You didn't change the old value"
        });
      })
  }


  updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex)
    this.editing[rowIndex + '-' + cell] = false;
    this.vehicles[rowIndex][cell] = event.target.value;
    this.vehicles = [...this.vehicles];
    console.log('UPDATED!', this.vehicles[rowIndex][cell]);
    this.messageService.add({
      severity: 'info',
      summary: 'Nothing Edited!',
      detail: "Click on save button to submit the new value"
    });
  }

  save(row) {
    this.isLoading = true;
    this.lookup.updateVehcile(row).subscribe((results) => {
      console.log(results);
      this.isLoading = false;
      this.messageService.add({
        severity: 'success',
        summary: 'Successful!',
        detail: "Saved Successfully!"
      });
    }, error=>{
      this.messageService.add({
        severity: 'error',
        summary: 'error!',
        detail: error
      });
    });
  }

}
