import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LookupService } from '../../../../api-module/services/lookup-services/lookup.service';
import { MessageService } from 'primeng/primeng';
import { VehicleService } from '../vehicle.service';

@Component({
  selector: 'app-vehicle-update-modal',
  templateUrl: './vehicle-update-modal.component.html',
  styleUrls: ['./vehicle-update-modal.component.css']
})
export class VehicleUpdateModalComponent implements OnInit {

  @Input() data;
  plateNo: string = "";
  constructor(private activeModal: NgbActiveModal, private lookup:LookupService, private messageService: MessageService, private vehicleService:VehicleService) { }

  ngOnInit() {
    if (this.data) {
      this.plateNo = this.data.plate_no;
    }
  }
  onSubmit(){
    this.lookup.addVehcile(this.plateNo).subscribe((result)=>{
      console.log(result);
      this.vehicleService.getVehicle.next();
      this.messageService.add({
        severity: 'success',
        summary: 'Successful!',
        detail: "Saved Successfully!"
      });
      this.activeModal.close();
    }, error=>{
      this.messageService.add({
        severity: 'error',
        summary: 'error!',
        detail: error
      });
    })
  }


  close() {
    this.activeModal.dismiss();
    this.messageService.add({
      severity: 'info',
      summary: 'Nothing Added!',
      detail: "You didn't saved new value."
    });
  }

}
