import { Component, OnInit, Input, OnChanges, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from "primeng/components/common/messageservice";
import { PromptComponent } from "../../shared-module/shared/prompt/prompt.component";
import { languagesComponent } from "../../shared-module/shared/languageModel/languageModel.component"
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: "app-simple-admin-content",
  templateUrl: "./simple-admin-content.component.html",
  styleUrls: ["./simple-admin-content.component.css"]
})
export class SimpleAdminContentComponent implements OnInit, OnChanges, OnDestroy {
  @Input() type: string;
  @Input() getData: string;
  dataSubscription: Subscription;
  deleteDataSubscription: Subscription;
  modalRef: any;
  rows: any[];
  toggleLoading: boolean;
  editing = {};
  languageArray: Array<any> = [];
  resultColumns: Array<any> = [];

  constructor(private lookUp: LookupService, private messageService: MessageService, private modalService: NgbModal, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.rows = [];

    // this.toggleLoading = true;
  }

  generateColumns(rows: Array<any>) {
    // this.rows.forEach((item, index) => {
    //   this.resultColumns[index] = [];
    //   item.languagesDictionaries.forEach((langObj) => {
    //     let resultColumnsObj = {};
    //     resultColumnsObj[langObj.key] = langObj.value
    //     this.resultColumns[index].push(resultColumnsObj);
    //   })

    // });
    // console.log(this.resultColumns);

    this.rows.forEach((item, index) => {
      this.rows[index].languagesDictionaries.forEach((langObj) => {
        item[langObj.key] = langObj.value;
      });
    });
  }

  createLangArray() {
    if (this.rows && this.rows[0] && this.rows[0].languagesDictionaries) {
      this.rows[0].languagesDictionaries.forEach((langObj) => {
        this.languageArray.push(langObj);
      });
    }
  }

  updateLanguageValue(event, langIndex, rowIndex) {
    this.rows[rowIndex].languagesDictionaries[langIndex].value = event.target.value;
    this.rows = [...this.rows];
    this.generateColumns(this.rows);
  }


  ngOnChanges() {
    this.toggleLoading = true;
    !this.rows && (this.rows = []);

    if (this.type === "Area" && this.getData === "Area") {
      this.dataSubscription = this.lookUp.getAllAreasDataManagement().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
          this.generateColumns(this.rows);
          this.languageArray = [];
          this.createLangArray();
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    } else if (this.type === "Problems" && this.getData === "Problems") {
      this.dataSubscription = this.lookUp.getAllProblemsLocalized().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
          this.generateColumns(this.rows);
          this.languageArray = [];
          this.createLangArray();
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Attendance Status" && this.getData === "Attendance Status") {
      this.dataSubscription = this.lookUp.getAllAttendanceStates().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
          this.generateColumns(this.rows);
          this.languageArray = [];
          this.createLangArray();
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Availability" && this.getData === "Availability") {
      this.dataSubscription = this.lookUp.getAllAvailability().subscribe((data) => {
        this.toggleLoading = false;
        this.rows = data.data;
        this.generateColumns(this.rows);
        this.languageArray = [];
        this.createLangArray();

      },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Building Types" && this.getData === "Building Types") {
      this.dataSubscription = this.lookUp.getBuildingTypes().subscribe((data) => {
        this.toggleLoading = false;
        this.rows = data.data;
        this.generateColumns(this.rows);
        this.languageArray = [];
        this.createLangArray();
        //console.log(this.rows);
      },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Company Code" && this.getData === "Company Code") {
      this.dataSubscription = this.lookUp.getCompanyCodes().subscribe((data) => {
        this.toggleLoading = false;
        this.rows = data.data;
        this.generateColumns(this.rows);
        this.languageArray = [];
        this.createLangArray();
        //console.log(this.rows);
      },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Contract Type" && this.getData === "Contract Type") {
      this.dataSubscription = this.lookUp.getContractTypes().subscribe((data) => {
        this.toggleLoading = false;
        this.rows = data.data;
        this.generateColumns(this.rows);
        this.languageArray = [];
        this.createLangArray();
        //console.log(this.rows);
      },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Cost Center" && this.getData === "Cost Center") {
      this.dataSubscription = this.lookUp.getCostCenter().subscribe((data) => {
        this.toggleLoading = false;
        this.rows = data.data;
        this.languageArray = [];
        this.createLangArray();
        //console.log(this.rows);
      },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Division" && this.getData === "Division") {
      this.dataSubscription = this.lookUp.getDivision().subscribe((data) => {
        this.toggleLoading = false;
        this.rows = data.data;
        this.generateColumns(this.rows);
        this.languageArray = [];
        this.createLangArray();
        //console.log(this.rows);
      },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Governorates" && this.getData === "Governorates") {
      this.dataSubscription = this.lookUp.getGovernorates().subscribe((data) => {
        this.toggleLoading = false;
        this.rows = data.data;
        this.generateColumns(this.rows);
        this.languageArray = [];
        this.createLangArray();
        //console.log(this.rows);
      },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Shift" && this.getData === "Shift") {
      this.dataSubscription = this.lookUp.getShift().subscribe((data) => {
        this.toggleLoading = false;
        this.rows = data.data;
        this.languageArray = [];
        this.createLangArray();
        //console.log(this.rows);
      },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }
    else if (this.type === "Languages" && this.getData === "Languages") {

      this.dataSubscription = this.lookUp.getlanguages().subscribe(
        (data) => {
          this.toggleLoading = false;
          this.rows = data.data;
          this.languageArray = [];
          this.createLangArray();

        },
        err => {
          this.messageService.add({
            severity: "error",
            summary: "Failed!",
            detail: "Failed to update due to network error"
          });
          this.toggleLoading = false;
        });
    }

  }

  ngOnDestroy() {
    this.dataSubscription && this.dataSubscription.unsubscribe();
    this.deleteDataSubscription && this.deleteDataSubscription.unsubscribe();
  }

  add() {
    this.openModal({}, `Add ${this.type}`, this.type);
    this.modalRef.result.then((newValue) => {
      // newValue = {
      //   name: newValue.name
      // };
      console.log(newValue);
      if (this.type == "Area") {

        this.dataSubscription = this.lookUp.postArea(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! ."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Problems") {
        this.dataSubscription = this.lookUp.postProblem(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully!."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Attendance Status") {
        this.dataSubscription = this.lookUp.postAttendanceStates(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully!."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to save due to network error"
            });
          });
      }
      else if (this.type == "Availability") {
        this.dataSubscription = this.lookUp.postAvailability(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved successfully! . please refresh your browser to see it."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Building Types") {
        this.dataSubscription = this.lookUp.postBuildingTypes(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully!."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Company Code") {
        this.dataSubscription = this.lookUp.postCompanyCode(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! ."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Contract Type") {
        this.dataSubscription = this.lookUp.postContractType(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! ."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Cost Center") {
        this.dataSubscription = this.lookUp.postCostCenter(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! ."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Division") {
        this.dataSubscription = this.lookUp.postDivision(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! ."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Governorates") {
        this.dataSubscription = this.lookUp.postGovernorates(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! ."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Shift") {
        this.dataSubscription = this.lookUp.postShift(newValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! ."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Languages") {

        this.dataSubscription = this.lookUp.postLanguages(newValue).subscribe(
          (data) => {
            if (data.isSucceeded === true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! ."
              });
              this.rows.push(data);
              this.ngOnChanges();
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }

    })
      .catch((result) => {
        //console.log('nothing added');
        this.messageService.add({
          severity: "info",
          summary: "Nothing Added!",
          detail: "You didn't saved new value."
        });
      });
  }

  edit(row) {
    console.log(row);
    this.openModal(Object.assign({}, row), "Edit", this.type);
    this.modalRef.result.then((editedValue) => {
      console.log(editedValue);
      // let editedToPost = {
      //   id: row.id,
      //   name: editedValue.name,
      //   fK_OrderStatus_Id: editedValue.fK_OrderStatus_Id
      // };
      // console.log(editedValue);
      if (this.type == "Area") {
        row.area_No = editedValue.area_No;
        row.name = editedValue.name;

        this.dataSubscription = this.lookUp.editArea(row).subscribe(
          (data) => {
            console.log(row);
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "Edit and Saved Successfully!"
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to update due to network error"
            });
          });
      }
      else if (this.type == "Attendance Status") {
        row.name = editedValue.name;

        this.dataSubscription = this.lookUp.editAttendanceStates(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "Edit and Saved Successfully!"
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to update due to network error"
            });
          });
      }
      else if (this.type == "Availability") {
        row.name = editedValue.name;
        this.dataSubscription = this.lookUp.editAvailability(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "Edit and Saved Successfully!"
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to update due to network error"
            });
          });
      }
      else if (this.type == "Building Types") {
        row.code = editedValue.code;
        row.name = editedValue.name;

        this.dataSubscription = this.lookUp.editBuildingTypes(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "Edit and Saved Successfully!"
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to update due to network error"
            });
          });
      }
      else if (this.type == "Company Code") {
        row.code = editedValue.code;
        row.name = editedValue.name;

        this.dataSubscription = this.lookUp.editCompanyCode(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "Edit and Saved Successfully!"
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to update due to network error"
            });
          });
      }
      else if (this.type == "Contract Type") {
        row.code = editedValue.code;
        row.name = editedValue.name;

        this.dataSubscription = this.lookUp.editContractTypes(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "Edit and Saved Successfully!"
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to update due to network error"
            });
          });
      }
      else if (this.type == "Cost Center") {
        row.name = editedValue.name;

        this.dataSubscription = this.lookUp.editCostCenter(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              console.log(row);
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "Edit and Saved Successfully!"
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to update due to network error"
            });
          });
      }
      else if (this.type == "Division") {
        row.code = editedValue.code;
        row.name = editedValue.name;

        this.dataSubscription = this.lookUp.editDivision(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! ."
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Governorates") {
        row.name = editedValue.name;

        this.dataSubscription = this.lookUp.editGovernorates(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! . please refresh your browser to see it."
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Shift") {
        console.log(row.fromTime);
        row.day = editedValue.day;
        row.fromTime = new Date(editedValue.fromTime);
        row.toTime = new Date(editedValue.toTime);
        this.dataSubscription = this.lookUp.editShift(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! . please refresh your browser to see it."
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }
      else if (this.type == "Languages") {
        console.log("Languages");
        row.code = editedValue.code;
        row.name = editedValue.name;

        this.dataSubscription = this.lookUp.editLanguages(editedValue).subscribe(
          (data) => {
            if (data.isSucceeded == true) {
              this.messageService.add({
                severity: "success",
                summary: "Successful!",
                detail: "New value saved Successfully! . please refresh your browser to see it."
              });
            }
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: "error",
              summary: "Failed!",
              detail: "Failed to Save due to network error"
            });
          });
      }

    })
      .catch((result) => {
        this.messageService.add({
          severity: "info",
          summary: "Nothing Edited!",
          detail: "You didn't change the old value"
        });
      });
  }

  remove(row) {
    console.log(row.id);
    if (this.type == "Area") {
      this.deleteDataSubscription = this.lookUp.deleteArea(row.id).subscribe(
        () => {

          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });

          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Area removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type == "Attendance Status") {
      this.dataSubscription = this.lookUp.deleteAttendanceStates(row.id).subscribe(
        (data) => {
          // this.rows = data;
          // //console.log(this.rows);
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type == "Availability") {
      this.dataSubscription = this.lookUp.deleteAvailability(row.id).subscribe(
        (data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status === 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type === "Building Types") {
      this.dataSubscription = this.lookUp.deleteBuildingTypes(row.id).subscribe(
        (data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });

          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type == "Company Code") {
      this.dataSubscription = this.lookUp.deleteCompanyCode(row.id).subscribe(
        (data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type == "Contract Type") {
      this.dataSubscription = this.lookUp.deleteContractTypes(row.id).subscribe(
        (data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type == "Cost Center") {
      this.dataSubscription = this.lookUp.deleteCostCenter(row.name).subscribe(
        (data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type === "Division") {
      this.dataSubscription = this.lookUp.deleteDivision(row.id).subscribe(
        () => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type === "Governorates") {
      this.dataSubscription = this.lookUp.deleteGovernorates(row.id).subscribe(
        () => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type === "Shift") {
      this.dataSubscription = this.lookUp.deleteShift(row.id).subscribe(
        () => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }
    else if (this.type === "Languages") {
      this.dataSubscription = this.lookUp.deleteLanguages(row.id).subscribe(
        () => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: "success",
            summary: "Success!",
            detail: "Removed successfully!"
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: "error",
            summary: "Failed",
            detail: "Failed to remove due to server error!"
          });

        });
    }

  }

  addEditLang(row) {
    this.openModalLanguage(Object.assign({}, row), "language", this.type);

    this.modalRef.result.then(
      (newValue) => {
        this.ngOnChanges();

      })
      .catch((result) => {
        this.messageService.add({
          severity: "info",
          summary: "Nothing Edited!",
          detail: "You didn't change the old value"
        });
      });
  }

  openModal(data, header, type?) {
    console.log("open Modal");
    this.modalRef = this.modalService.open(PromptComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.type = type;
    this.modalRef.componentInstance.data = data;
  }

  openModalLanguage(data, header, type?) {
    console.log("open Modal Language");
    this.modalRef = this.modalService.open(languagesComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.type = type;
    this.modalRef.componentInstance.data = data;
    console.log(data);
  }


  save(row) {
    console.log(row);
    if (this.type == "Area") {
      this.lookUp.editAreaLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Attendance Status") {
      this.lookUp.editAttendanceStatesLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Availability") {
      this.lookUp.editAvailabilityLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Building Types") {
      this.lookUp.editBuildingTypesLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Company Code") {
      this.lookUp.editCompanyCodeLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Contract Type") {
      this.lookUp.editContractTypesLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Cost Center") {
      this.lookUp.editCostCenterLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Division") {
      this.lookUp.editDivisionLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Governorates") {
      this.lookUp.editGovernoratesLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Shift") {
      this.lookUp.editShiftLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Languages") {
      this.lookUp.editLanguages(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    } else if (this.type == "Problems") {
      this.lookUp.editProblem(row).subscribe(() => {
        console.log(this.type, "Edited");
      });
    }
  }


  updateValue(event, cell, rowIndex) {
    console.log("In-line editing rowIndex", rowIndex);
    this.editing[rowIndex + "-" + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
    console.log("UPDATED!", this.rows[rowIndex][cell]);
  }

}
