import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { MessageService } from 'primeng/primeng';

@Component({
  selector: 'app-map-settings',
  templateUrl: './map-settings.component.html',
  styleUrls: ['./map-settings.component.css']
})
export class MapSettingsComponent implements OnInit {
  isLoading: boolean = false;
  settings: any = {}
  numberOfServicesFinished: number = 0;
  numberOfServicesCalled: number = 0;
  userId: string = "";

  constructor(private lookupService: LookupService, private messageService: MessageService) { }

  ngOnInit() {
    this.isLoading = true;

    this.lookupService.getMapSettings(1).subscribe(
      (response) => {
        if (response && response.isSucceeded) {
          this.isLoading = false;

          this.settings.APIKeyForWeb = response.data.mobile_Api_Key;
          this.settings.APIKeyForMobile = response.data.web_Api_Key;
          this.settings.RefreshRate = response.data.refresh_Rate;
        }
      });
  }

  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished === this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }


  onSaveSettings() {
    let obj = {
      mobile_Api_Key: this.settings.APIKeyForMobile,
      web_Api_Key: this.settings.APIKeyForWeb,
      refresh_Rate: this.settings.RefreshRate,
      id: 1
    };

    this.isLoading = true;
    this.lookupService.putMapSettings(obj).subscribe(
      (response) => {
        if (response && response.isSucceeded) {
          this.isLoading = false;
          this.messageService.add({
            severity: 'success',
            summary: 'Successful!',
            detail: `info Edited & Saved Successfully!`
          });
        } else {
          this.isLoading = false;
          this.messageService.add({
            severity: 'error',
            summary: 'invalid values!',
            detail: response.status.message
          });

        }
      }, (error) => {
        this.isLoading = false;
        this.messageService.add({
          severity: 'error',
          summary: 'error!',
          detail: error
        });
      });
  }


}
