import { Injectable } from "@angular/core";
import { RequestOptions, Headers } from "@angular/http";

@Injectable({
  providedIn: "root"
})
export class AuthService {

  jwt() {
    // create authorization header with jwt token
    const currentUser = JSON.parse(this.getLocalStorage("AlghanimCurrentUser"));

    if (currentUser && currentUser.data.token) {
      const headers = new Headers({ 'Authorization': "bearer " + currentUser.data.token });
      headers.append("Content-Type", "application/json");

      return new RequestOptions({ headers: headers });
    }

    return null;
  }

  jwtWithParams(params) {
    // create authorization header with jwt token
    const currentUser = JSON.parse(this.getLocalStorage("AlghanimCurrentUser"));

    if (currentUser && currentUser.data.token) {
      const headers = new Headers({ 'Authorization': "bearer " + currentUser.data.token });
      headers.append("Content-Type", "application/json");

      return new RequestOptions({ headers: headers, params: params });
    }

    return null;
  }

  get currentUserId(): string {
    return this.getLocalStorage("userId");
  }

  getLocalStorage(key: string) {
    return localStorage.getItem(key);
  }

  setLocalStorage(key: string, value) {
    localStorage.setItem(key, value);
  }
}
