import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BaseRequestOptions, HttpModule } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgDragDropModule } from "ng-drag-drop";
import { DragulaModule } from "ng2-dragula/ng2-dragula";
import { MessageService } from "primeng/components/common/messageservice";
import { pagesItemSharedService } from "./api-module/pagesItemsShared/pagesItemsShared.service";
import { AuthenticationServicesService } from "./api-module/services/authentication/authentication-services.service";
import { UtilitiesService } from "./api-module/services/utilities/utilities.service";
import { AppComponent } from "./app.component";
import { PageNotFoundComponentComponent } from "./page-not-found-component/page-not-found-component.component";
// import {SignalRService} from './shared-module/signal-r.service';
import { RoutingComponents, RoutingModule } from "./router.module";
import { SharedModuleModule } from "./shared-module/shared-module.module";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents,
    PageNotFoundComponentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RoutingModule,
    FormsModule,
    HttpModule,
    DragulaModule,
    NgDragDropModule.forRoot(),
    SharedModuleModule.forRoot()
    //, SignalRModule.forRoot(createConfig)
  ],
  providers: [
    {
      provide: LocationStrategy, useClass: HashLocationStrategy
    },

    BaseRequestOptions, UtilitiesService, MessageService,
    AuthenticationServicesService, pagesItemSharedService],

  bootstrap: [AppComponent]

})
export class AppModule {
}
