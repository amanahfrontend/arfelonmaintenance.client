"use strict";

// APIs Configurations {Start}.
export const BaseUrl = "http://localhost/Arfelon/";

//export const BaseUrlIdentity = BaseUrl + "Arfelon.Identity.Management.API/api/";
//export const BaseUrlDataManagement = BaseUrl + "Arfelon.Data.Management.API/api/";
//export const BaseUrlUserManagement = BaseUrl + "Arfelon.User.Management.API/api/";
//export const BaseUrlOrder = BaseUrl + "Arfelon.Order.Management.API/api/";
//export const BaseUrlLocation = BaseUrl + "Arfelon.Dispatching.Location.API/api/";
//export const BaseUrlDropout = BaseUrl + "Arfelon.Dropout.API/api/";

// Debug:
// Remove this section when building a production and uncomment the above links.
export const BaseUrlIdentity = "http://localhost:49846/api/";
export const BaseUrlDataManagement = "http://localhost:51816/api/";
export const BaseUrlUserManagement = "http://localhost:54042/api/";
export const BaseUrlOrder = "http://localhost:57535/api/";
export const BaseUrlLocation = "http://localhost:55553/api/";
export const BaseUrlDropout = "http://localhost:49346/api/";
// APIs Configurations {End}.


export const MainPath = "http://localhostMainPath/Arfelon/";
// // Not found.
export const BaseUrlCustomer = BaseUrl + "Arfelon.Customer.API/api/";
export const BaseUrlCall = BaseUrl + "Arfelon.Call.API/api/";
export const BaseUrlContract = BaseUrl + "Arfelon.Contract.API/api/";
export const BaseUrlInventory = BaseUrl + "Arfelon.Inventory.API/api/";
export const BaseUrlEstimationQuotation = BaseUrl + "Arfelon.EstimationQuotation.API/api/";
export const BaseUrlSalary = BaseUrl + "Arfelon.Salary.API/api/";
export const BaseUrlVehicle = BaseUrl + "Arfelon.Vehicle.API/api/";


/***************************************/
// SignalR function
/***************************************/

//export function vehicleHubUrl(userId, roles, token) {
//  return BaseUrlVehicle.replace("/api/", "/") + "dispatchingHub?userId=" + userId + "&&roles=" + roles + "&&token=" + token;
//}

export function quotationHubUrl(userId, roles, token) {
  return BaseUrlEstimationQuotation.replace("/api/", "/") + "quotationHub?userId=" + userId + "&&roles=" + roles + "&&token=" + token;
}

export function callHubUrl(userId, roles, token) {
  return BaseUrlOrder.replace("/api/", "/") + "/callingHub?userId=" + userId + "&&roles=" + roles + "&&token=" + token;
}

export function orderHubUrl(userId, roles, token) {
  return BaseUrlOrder.replace("/api/", "/") + "orderingHub?userId=" + userId + "&&roles=" + roles + "&&token=" + token;
}

/***************************************/

/*---JSON mocked - Mohamed Ragab ---*/
export const SearchCustomer = BaseUrlCall + "Call/Search/";
//export const callPriorities = BaseUrlCall + "CallPriority"; // Json mocking
//export const callsTypes = BaseUrlCall + "CallType"; // Json mocking
//export const customerTypes = BaseUrlCustomer + "CustomerType/GetAll"; // Json mocking
//export const phoneTypes = BaseUrlCustomer + "PhoneType/GetAll"; // Json mocking
export const locationByCustomer = BaseUrlCustomer + "location/GetLocationByCustomerId?customerId="; // Json mocking
export const callsHistory = BaseUrlCall + "Call/GetAll"; // Json mocking
export const callsLogs = BaseUrlCall + "CallLog/GetByCall/"; // Json mocking
//export const status = BaseUrlCall + "CallStatus"; // Json mocking
export const item = BaseUrlEstimationQuotation + "EstimationItem/SearchItems/"; // Json mocking
//export const itemSearch = BaseUrlInventory + "item/GetByCode/"; // Json mocking
export const estimations = BaseUrlEstimationQuotation + "Estimation"; // Json mocking
export const searchByPhoneOrName = BaseUrlEstimationQuotation + "Estimation/Search/"; // Json mocking
export const estimationDetails = BaseUrlEstimationQuotation + "Estimation/Get/"; // Json mocking
export const quotations = BaseUrlEstimationQuotation + "Quotation/GetAll"; // Json mocking
export const searchQuotation = BaseUrlEstimationQuotation + "Quotation/Search/"; // Json mocking
export const quotationbyId = BaseUrlEstimationQuotation + "Quotation/Get/"; // Json mocking
export const quotationbyRefNo = BaseUrlEstimationQuotation + "Quotation/GetByRef/"; // Json mocking
//export const contractTypes = BaseUrlContract + "ContractType/GetAll"; // Json mocking
export const orderTypes = BaseUrlOrder + "OrderType/GetAll"; // Json mocking
export const orderPriorities = BaseUrlOrder + "OrderPriority/GetAll"; // Json mocking
export const searchContract = BaseUrlContract + "Contract/search/"; // Json mocking
export const allContracts = BaseUrlContract + "Contract/GetAll"; // Json mocking
export const contractById = BaseUrlContract + "Contract/Get/"; // Json mocking
export const roles = BaseUrlUserManagement + "Role/getall"; // Json mocking
export const users = BaseUrlUserManagement + "User/getall"; // Json mocking
export const allOrders = BaseUrlOrder + "Order/GetAll";

export const searchOrders = BaseUrlOrder + "Order/Search/";

export const orderStatus = BaseUrlOrder + "orderStatus/getAll";

export const allPreventive = BaseUrlContract + "Contract/GetAllPreventiveMaintainence";

export const GetCallDetails = BaseUrlCall + "Call/Get/";

export const GetCustomerDetailsByID = BaseUrlCall + "Call/Get/";

export const contractByCustomer = BaseUrlContract + "Contract/GetByCustomer/";

export const orderByCustomer = BaseUrlOrder + "Order/SearchByCustomerId/";

export const callrByCustomer = BaseUrlCall + "Call/Search/";

export const allLocation = BaseUrlCustomer + "location/getall";

export const allGovernorates = BaseUrlCustomer + "location/GetAllGovernorates";

export const allAreas = BaseUrlCustomer + "location/GetAreas";
export const allBlocks = BaseUrlCustomer + "location/GetBlocks";
export const allStreets = BaseUrlCustomer + "location/GetStreets";
export const allGetLocationByPaci = BaseUrlCustomer + "location/GetLocationByPaci?paciNumber=";
export const allDispatchers = BaseUrlUserManagement + "User/GetDispatchers";
export const unAssignedTecs = BaseUrlOrder + "AssignedTechnicans/GetUnAssignedTechnicans";
export const assignedTecs = BaseUrlOrder + "AssignedTechnicans/GetTechnicansByDispatcherId/";
export const ordersByDis = BaseUrlOrder + "AssignedTechnicans/GetDispatcher/";

export const assignedItems = BaseUrlInventory + "TechnicanAssignedItems/GetAssignedItemsByTechnican/";

export const usedItems = BaseUrlInventory + "TechnicanUsedItems/GetUsedItemsByTechnican/";

export const allTecs = BaseUrlUserManagement + "User/GetTechnicans";

export const allCustomers = BaseUrlCustomer + "Customer/GetAll";

/*----- Started 12/12 services mocks -----*/

export const orderProgressUrl = "OrderAction/GetOrderProgress/";

export const addUserDeviceUrl = "User/AddUserDevice";

export const progressStatuses = BaseUrlOrder + "ProgressStatus/GetAll";

export const getDiByProb = BaseUrlOrder + "OrderDistributionCriteria/GetAllDistribution";

export const allProbs = BaseUrlOrder + "Lang_OrderProblem/GetAll";

export const allProbsLocalized = BaseUrlOrder + "Lang_OrderProblem/GetAllLanguages";

export const addProbURL = "OrderProblem/Add";

export const allAreasDispatcher = BaseUrlDataManagement + "Areas/GetAll";

export const removeDisWithProb = BaseUrlOrder + "/OrderDistributionCriteria/Delete/";

export const getDisWithAllProbs = BaseUrlOrder + "OrderDistributionCriteria/GetByDispatcherId/";

export const getAllVehicles = BaseUrlVehicle + "Vehicle/GetVehicles";

export const getAllItems = BaseUrlInventory + "Item";

export const allOrderProgressUrl = BaseUrlOrder + "ProgressStatus/GetAll";

export const allOrderProblemsUrl = BaseUrlOrder + "OrderProblem/GetAll";

export const getCustomerDetails = BaseUrlCustomer + "Customer/Get/";

/*-------------------post--------------------*/

export const logPostUrl = BaseUrlCall + "CallLog/Add"; // Json mocking
export const estimationPostUrl = BaseUrlEstimationQuotation + "Estimation/Add"; // Json mocking
export const callPostUrl = BaseUrlCall + "Call/Add"; // Json mocking
export const generateQuotationUrl = BaseUrlEstimationQuotation + "Quotation/Add"; // Json mocking
export const generateContractUrl = BaseUrlContract + "Contract/Add"; // Json mocking
export const newUserUrl = BaseUrlUserManagement + "User/Add"; // Json mocking
export const itemsExcelSheetUrl = BaseUrlInventory + "ProcessItem/UpdateSalaries"; // Json mocking
export const salaryExcelSheetUrl = BaseUrlSalary + "ProcessEmployeeSalary/UpdateSalaries"; // Json mocking
export const postNewOrderUrl = BaseUrlOrder + "Order/Add"; // Json mocking
export const postCustomerTypesUrl = BaseUrlCustomer + "CustomerType/Add"; // Json mocking
export const postCallsTypesUrl = BaseUrlCall + "CallType/Add"; // Json mocking
export const postCallPrioritiesUrl = BaseUrlCall + "CallPriority/Add"; // Json mocking
export const postPhoneTypesUrl = BaseUrlCustomer + "PhoneType/Add"; // Json mocking
export const postActionStateUrl = BaseUrlCall + "CallStatus/Add"; // Json mocking
export const updateStatusUrl = BaseUrlCall + "CallStatus/Update";

export const postContractTypeUrl = BaseUrlContract + "ContractType/Add"; // Json mocking
export const postRoleUrl = BaseUrlUserManagement + "Role/Add"; // Json mocking
export const postOrderTypeUrl = BaseUrlOrder + "OrderType/Add"; // Json mocking
export const postOrderPriorityUrl = BaseUrlOrder + "OrderPriority/Add"; // Json mocking
export const postOrderStateUrl = BaseUrlOrder + "OrderStatus/Add"; // Json mocking
export const postTecForDist = BaseUrlOrder + "AssignedTechnicans/Add"; // Json mocking
export const orderByTec = BaseUrlOrder + "Order/AssignOrderToTechnican";
export const unAssignedOrder = BaseUrlOrder + "Order/UnAssignOrderToTechnican/";
export const tecAvailable = BaseUrlUserManagement + "User/ChangeUserAvailability";

// new urls 12/12/2017
export const postPreferedVisitDate = BaseUrlOrder + "Order/SetPreferedVisitTime";
export const transferToDispatcherUrl = BaseUrlOrder + "Order/TransferOrderToDispatcher";
export const postProgressUrl = BaseUrlOrder + "Order/AddOrderProgress";
export const postDispatcherWithProblemsUrl = BaseUrlOrder + "/OrderDistributionCriteria/AssignCriteriaToDispatchers";
export const filterOrdersUrl = BaseUrlOrder + "Order/GetFilteredOrderByDispatcherId";
export const filterOrdersMapUrl = BaseUrlOrder + "Order/GetMapFilteredOrderByDispatcherId";
export const getOrdersByDis = "Order/GetMapOrdersForDispatcher?dispatcherId=";
// dispatcher map filter api
export const postMapOrderFilterURL = "Order/OrderFilter";
// dispatcher board filter api
export const postBoardOrderFilterURL = "Order/assignedOrdersBoardFilter";

export const postUnassignedOrdersBoardFilterURL = "Order/UnassignedOrdersBoardFilter";


export const assignItemUrl = BaseUrlInventory + "TechnicanAssignedItems/AssignItem";
export const quotationWordUrl = BaseUrlEstimationQuotation + "Quotation/GenerateWord/";
export const filterUsedItems = BaseUrlInventory + "TechnicanUsedItems/Filter";

// ***********************filter preventive***************

export const filterPreventiveUrl = BaseUrlContract + "Contract/FilterPreventiveMaintainence";

export const filterEstimationsUrl = BaseUrlEstimationQuotation + "Estimation/Filter";

export const filterQuotationsUrl = BaseUrlEstimationQuotation + "Quotation/Filter";

export const postCustomerUrl = BaseUrlCustomer + "Customer/Add";

export const assignOrdersToDispatcherUrl = BaseUrlOrder + "Order/AssignOrdersToDispatcher";

export const assignOrdersToTecUrl = BaseUrlOrder + "Order/AssignOrdersToTechnican";

export const assignedItemSearch = BaseUrlInventory + "TechnicanAssignedItems/ChekItemExistForTechnican";

export const postOrderProgress = BaseUrlOrder + "ProgressStatus/Add";

export const postOrderProblems = BaseUrlOrder + "OrderProblem/Add";

export const postNewCustomerLocationUrl = BaseUrlCustomer + "location/add";

export const removeProgressStatusUrl = BaseUrlOrder + "ProgressStatus/Delete/";

export const removeProblemUrl = BaseUrlOrder + "OrderProblem/Delete/";


/*-------------- Delete -------------------*/

export const deletEstimationUrl = BaseUrlEstimationQuotation + "Estimation/Delete/"; // Json mocking
export const deleteQuotationUrl = BaseUrlEstimationQuotation + "Quotation/Delete/"; // Json mocking
export const deleteContractUrl = BaseUrlContract + "Contract/Delete/"; // Json mocking
export const deleteCustomertypeUrl = BaseUrlCustomer + "CustomerType/Delete/"; // Json mocking
export const deleteCallTypeUrl = BaseUrlCall + "CallType/Delete/"; // Json mocking
export const deletePrioritiesUrl = BaseUrlCall + "CallPriority/Delete/"; // Json mocking
export const deletePhoneTypeUrl = BaseUrlCustomer + "PhoneType/Delete/"; // Json mocking
export const deleteStatusUrl = BaseUrlCall + "CallStatus/Delete/"; // Json mocking
export const deleteContractTypeUrl = BaseUrlContract + "ContractType/Delete/"; // Json mocking
export const deleteRoleUrl = BaseUrlUserManagement + "Role/Delete/"; // Json mocking
export const deleteUserUrl = BaseUrlUserManagement + "User/Delete/"; // Json mocking
export const deleteOrderUrl = BaseUrlOrder + "Order/Delete/"; // Json mocking
export const deleteOrderStatusUrl = BaseUrlOrder + "OrderStatus/Delete/"; // Json mocking
export const deleteOrderTypeUrl = BaseUrlOrder + "OrderType/Delete/"; // Json mocking
export const deleteOrderPriorityUrl = BaseUrlOrder + "OrderPriority/Delete/"; // Json mocking
export const deleteTecFromDis = BaseUrlOrder + "AssignedTechnicans/DeleteAssignedTechnican/"; // Json mocking

/*----------------- PUT -------------------*/

export const updateEstimationUrl = BaseUrlEstimationQuotation + "Estimation/Update";
export const updateCustomerTypeUrl = BaseUrlCustomer + "CustomerType/Update";
export const updateCallTypeUrl = BaseUrlCall + "CallType/Update";
export const updatePriorityUrl = BaseUrlCall + "CallPriority/Update";
export const updatePhoneTypeUrl = BaseUrlCustomer + "PhoneType/Update";
export const updateContractTypeUrl = BaseUrlContract + "ContractType/Update";
export const updateRoleUrl = BaseUrlUserManagement + "Role/Update";
export const updateUserUrl = BaseUrlUserManagement + "User/Update";
//export const updateOrderUrl = BaseUrlOrder + 'Order/Update';
export const updateOrderStatusUrl = BaseUrlOrder + "OrderStatus/Update";
export const updateOrderPriorityUrl = BaseUrlOrder + "OrderPriority/Update";
export const updateOrderTypeUrl = BaseUrlOrder + "OrderType/Update";
export const updateOrderProgressUrl = BaseUrlOrder + "ProgressStatus/Update";
export const updateOrderProblemsUrl = BaseUrlOrder + "OrderProblem/Update";

export const updateCustomerUrl = BaseUrlCustomer + "Customer/Update";

export const updateLocationUrl = BaseUrlCustomer + "location/update";


//*************************************************************
//*************************************************************
//*************************************************************
// -------------------- Customers URLs ------------------------

// export const SearchCustomer = MainPath + '/api/customer/SearchCustomer?searchToken=';
export const CreatenewCustomer = MainPath + "/api/customer/CreateDetailedCustomer";
// export const GetCustomerDetailsByID = MainPath + '/api/customer/CustomerHistory/';
export const EditCustomerInfo = MainPath + "/api/customer/Update";
export const EditContract = MainPath + "/api/contract/Update";
export const AddContract = MainPath + "/api/contract/add";
export const AddNewWorkOrder = MainPath + "/api/WorkOrder/AddWorkOrder";
export const AddLocation = MainPath + "/api/location/add";

export const EditComplain = MainPath + "/api/complain/Update";
export const AddComplain = MainPath + "/api/complain/add";
export const EditWorkOrder = MainPath + "/api/WorkOrder/EditWorkOrder";

//-------------------- Lookups URls -----------------------------
export const allCustomerTypes = MainPath + "/api/CustomerType/getall";
export const allContractType = MainPath + "/api/ContractType/getall";
export const allWorkItems = MainPath + "/api/Item/GetAll";
// export const allLocation = MainPath + '/api/location/getall';
export const allFactories = MainPath + "/api/Factory/GetAll";
// export const allGovernorates = MainPath + '/api/location/GetAllGovernorates';

// export const allAreas = MainPath + '/api/location/GetAreas';
// export const allBlocks = MainPath + '/api/location/GetBlocks';
// export const allStreets = MainPath + '/api/location/GetStreets';
// export const allGetLocationByPaci = MainPath + '/api/location/GetLocationByPaci?paciNumber=';
export const allStatus = MainPath + "/api/Status/getall";


export const editStatus = MainPath + "/api/Status/Update";
export const AddStatus = MainPath + "/api/Status/Add";
export const DeleteStatus = MainPath + "/api/Status/Delete";

export const allRole = MainPath + "/api/Role/getall";
export const editRole = MainPath + "/api/Role/Update";
export const AddRole = MainPath + "/api/Role/Add";
export const DeleteRole = MainPath + "/api/Role/Delete";

export const allPeriorities = MainPath + "/api/Priority/getall";
export const editPeriorities = MainPath + "/api/Priority/Update";
export const AddPeriorities = MainPath + "/api/Priority/Add";
export const DeletePeriorities = MainPath + "/api/Priority/Delete";

export const editCustomerType = MainPath + "/api/CustomerType/Update";
export const AddCustomerType = MainPath + "/api/CustomerType/Add";
export const DeleteCustomerType = MainPath + "/api/CustomerType/Delete";

export const editContractType = MainPath + "/api/ContractType/Update";
export const AddContractType = MainPath + "/api/ContractType/Add";
export const DeleteContractType = MainPath + "/api/ContractType/Delete";

export const allEquipmentType = MainPath + "/api/EquipmentType/getall";
export const editEquipmentType = MainPath + "/api/EquipmentType/Update";
export const AddEquipmentType = MainPath + "/api/EquipmentType/Add";
export const DeleteEquipmentType = MainPath + "/api/EquipmentType/Delete";

export const allEquipment = MainPath + "/api/Equipment/getall";
export const editEquipment = MainPath + "/api/Equipment/Update";
export const AddEquipment = MainPath + "/api/Equipment/Add";
export const DeleteEquipment = MainPath + "/api/Equipment/Delete";

export const editItem = MainPath + "/api/Item/Update";
export const AddItem = MainPath + "/api/Item/Add";
export const DeleteItem = MainPath + "/api/Item/Delete";

export const editFactory = MainPath + "/api/Factory/Update";
export const AddFactory = MainPath + "/api/Factory/Add";
export const DeleteFactory = MainPath + "/api/Factory/Delete";

export const alluser = MainPath + "/api/User/getall";
export const Adduser = MainPath + "/api/User/Add";
export const Deleteuser = MainPath + "/api/User/Delete";

export const DeleteComplain = MainPath + "/api/complain/delete?id=";
export const DeleteContract = MainPath + "/api/contract/delete?id=";
export const DeleteWorkOrder = MainPath + "/api/workorder/delete?id=";

export const UsernameExists = MainPath + "/api/User/UsernameExists?username=";
export const EmailExists = MainPath + "/api/User/EmailExists?email=";
// ---------------------------Work Order Management page -------------------------

export const AllWorkOrdersDispature = MainPath + "/api/WorkOrder/GetAllSuborders";
export const AllWorkOrdersMovementController = MainPath + "/api/WorkOrder/GetByMovementController";
export const AvailableEquipmentInFactory = MainPath + "/api/Equipment/GetAvailableEquipmentInFactory?factoryId=";
export const UpdateAssignmentToWorkOrder = MainPath + "/api/Equipment/UpdateAssignmentToWorkOrder";
export const UpdateSubOrder = MainPath + "/api/WorkOrder/UpdateSubOrder";
export const reassinFactory = MainPath + "/api/WorkOrder/CanAssignToFactory?workorderId=";
export const AssignmentToFactory = MainPath + "/api/WorkOrder/UpdateOrderAssignmentToFactory?orderId=";
export const JobDetails = MainPath + "/api/job/GetByWorkOrder?workorderId=";
export const AvailableVehicleInFactory = MainPath + "/api/vehicle/GetAvailableVehicleInFactory?factoryId=";
export const AddJop = MainPath + "/api/job/add";
export const CancelJop = MainPath + "/api/job/Cancel?jobId=";
export const ReassignJop = MainPath + "/api/job/Reassign?jobId=";
export const AllWorkOrder = MainPath + "/api/WorkOrder/getall";
export const checkCustomerNameExist = MainPath + "/api/customer/IsCustomerNameExist?name=";
export const checkCustomerphoneExist = MainPath + "/api/customer/IsPhoneExist?phone=";
export const AllVehicles = MainPath + "/api/vehicle/getall";


export const NotificationOrders = MainPath + "/api/WorkOrder/getall";
export const RemindOrders = MainPath + "/api/WorkOrder/getall";


export const loginUrl = "User/Login";
export const logoutUrl = "User/Logout";
//////////////////////DataManagement/////////////////////////////////
//////////////Get/////////////
export const areasUrl = "Lang_Areas/GetAllLanguages";
export const areasPaginatedUrl = "Areas/GetAllPaginated";
export const attendanceStatusUrl = "Lang_AttendanceStates/GetAllLanguages";
export const availabilitysUrl = "Lang_Availability/GetAllLanguages";
export const buildingTypesUrl = "Lang_BuildingTypes/GetAllLanguages";
export const companyCodeUrl = "Lang_CompanyCode/GetAllLanguages";
export const contractTypesUrl = "Lang_ContractTypes/GetAllLanguages";
export const costCenterUrl = "CostCenter/GetAll";
export const divisionUrl = "Lang_Division/GetAllLanguages";
export const divisionEnUrl = "Division/GetAll";
export const governoratesUrl = "Lang_Governorates/GetAllLanguages";
export const shiftUrl = "Shift/GetAll";
export const languagesUrl = "SupportedLanguages/GetAll";
export const rolesUrl = "Role/GetAll";
export const adminUserUrl = "User/GetAdminUsers";

/////////GetById///////////////
export const areaLanguageByIdUrl = "Lang_Areas/GetAllLanguagesByAreaId/";
export const attendanceStatesByIdUrl = "Lang_AttendanceStates/GetAllLanguagesByAttendanceStatesId/";
export const availabilityByIdUrl = "Lang_Availability/GetAllLanguagesByAvailabilityId/";
export const buildingTypesByIdUrl = "Lang_BuildingTypes/GetAllLanguagesByBuildingTypesId/";
export const companyCodeByIdUrl = "Lang_CompanyCode/GetAllLanguagesByCompanyCodeId/";
export const contractTypesByIdUrl = "Lang_ContractTypes/GetAllLanguagesByContractTypesId/";
export const CostCentersByIdUrl = "Lang_CostCenter/GetAllLanguagesByCostCenterId/";
export const divisionByIdUrl = "Lang_Division/GetAllLanguagesByDivisionId/";
export const governoratesByIdUrl = "Lang_Governorates/GetAllLanguagesByGovernorateId/";
export const ShiftByIdUrl = "Lang_Shift/GetAllLanguagesByShiftId/";

//////////////Post/////////////
export const postPoblemUrl = "Lang_OrderProblem/Update";
export const postAreasUrl = "Areas/Add";
export const postAttendanceStates = "AttendanceStates/Add";
export const postAvailability = "Availability/Add";
export const postBuildingTypes = "BuildingTypes/Add";
export const postCompanyCode = "CompanyCode/Add";
export const postContractTypes = "ContractTypes/Add";
export const postCostCenter = "CostCenter/Add";
export const postDivision = "Division/Add";
export const postGovernorates = "Governorates/Add";
export const postShift = "Shift/Add";
export const postLanguages = "SupportedLanguages/Add";

export const postAreaLanguage = "Lang_Areas/AddMulti";
export const postAttendanceStatesLanguage = "Lang_AttendanceStates/AddMulti";
export const postAvailabilityLanguage = "Lang_Availability/AddMulti";
export const postBuildingTypesLanguage = "Lang_BuildingTypes/AddMulti";
export const postCompanyCodeLanguage = "Lang_CompanyCode/AddMulti";
export const postContractTypesLanguage = "Lang_ContractTypes/AddMulti";
export const postCostCenterLanguage = "Lang_CostCenter/AddMulti";
export const postDivisionLanguage = "Lang_Division/AddMulti";
export const postGovernoratesLanguage = "Lang_Governorates/AddMulti";
export const postShiftLanguage = "Lang_Shift/AddMulti";


//////////////put///////////
export const putAreasUrl = "Areas/Update";
export const putAttendanceStatesUrl = "AttendanceStates/Update";
export const putAvailabilityUrl = "Availability/Update";
export const putBuildingTypesUrl = "BuildingTypes/Update";
export const putCompanyCodeUrl = "CompanyCode/Update";
export const putContractTypesUrl = "ContractTypes/Update";
export const putCostCenterUrl = "CostCenter/Update";
export const putDivisionUrl = "Division/Update";
export const putGovernoratesUrl = "Governorates/Update";
export const putShiftUrl = "Shift/Update";
export const putLanguagesUrl = "SupportedLanguages/Update";

export const putAreaLanguageUrl = "Lang_Areas/UpdateAllLanguages";
export const putAttendanceStatesLanguageUrl = "Lang_AttendanceStates/UpdateAllLanguages";
export const putAvailabilityLanguageUrl = "Lang_Availability/UpdateAllLanguages";
export const putBuildingTypesLanguageUrl = "Lang_BuildingTypes/UpdateAllLanguages";
export const putCompanyCodeLanguageUrl = "Lang_CompanyCode/UpdateAllLanguages";
export const putContractTypesLanguageUrl = "Lang_ContractTypes/UpdateAllLanguages";
export const putLang_CostCenterLanguageUrl = "Lang_CostCenter/UpdateAllLanguages";
export const putDivisionLanguageUrl = "Lang_Division/UpdateAllLanguages";
export const putGovernoratesLanguageUrl = "Lang_Governorates/UpdateAllLanguages";
export const putShiftLanguageUrl = "Lang_Shift/UpdateLanguages";
export const putProblemLanguageUrl = "Lang_OrderProblem/Update";

/////////////delete////////////////
export const deleteAreaUrl = "Areas/Delete/";
export const deleteAttendanceStatesUrl = "AttendanceStates/Delete/";
export const deleteAvailabilityUrl = "Availability/Delete/";
export const deleteBuildingTypesUrl = "BuildingTypes/Delete/";
export const deleteCompanyCodeUrl = "CompanyCode/Delete/";
export const deleteContractTypesUrl = "ContractTypes/Delete/";
export const deleteCostCenterUrl = "CostCenter/Delete/";
export const deleteDivisionUrl = "Division/Delete/";
export const deleteGovernoratesUrl = "Governorates/Delete/";
export const deleteShiftUrl = "Shift/Delete/";
export const deleteLanguagesUrl = "SupportedLanguages/Delete/";

export const deleteAreaLanguagesUrl = "Lang_Areas/Delete/";
export const deleteAttendanceStatesLanguagesUrl = "Lang_AttendanceStates/Delete/";


///////////////////User Management////////////////////
////////Get//////////
export const areasEnUrl = "Areas/GetAll";
export const getDispatcherUrl = "Dispatcher/GetAll";
export const getDriverUrl = "Driver/GetAll";
export const getICAgentUrl = "";
export const getSuperVisorUrl = "Supervisor/GetAll";
export const getEngineerUrl = "Engineer/GetAll";
export const getTechnicianUrl = "Technician/GetAll";
export const getForemanUrl = "Foreman/GetAll";
export const getManagerUrl = "Manager/GetAll";
export const getMaterialUrl = "Material/GetAll";
export const getTeamsForForemen = "Team/GetByFormanId";

/////////Post/////////////
export const postUserUrl = "User/Add";
export const postUserDispatcherUrl = "Dispatcher/Add";
export const postSupervisorUrl = "Supervisor/Add";
export const postDriverUrl = "Driver/Add";
export const postEngineerUrl = "Engineer/Add";
export const postTechnicianUrl = "Technician/Add";
export const postForemanUrl = "Foreman/Add";
export const postManagerUrl = "Manager/Add";
export const postMaterialControllerUrl = "Material/Add";
export const resetPassword = "User/ResetPassword";


//////////Update//////////////
export const putUserUrl = "User/Update";
export const putUserDispatcherUrl = "Dispatcher/Update";
export const putUserSupervisorUrl = "Supervisor/Update";
export const putUserDriverUrl = "Driver/Update";
export const putUserMangerUrl = "Manager/Update";
export const putUserMaterialControllerUrl = "Material/Update";
export const putUserEngineerUrl = "Engineer/Update";
export const putUserTechnicianUrl = "Technician/Update";
export const putUserForemanUrl = "Foreman/Update";


//////////////GetById///////////////
export const getDispatcherUserById = "Dispatcher/GetDispatcherUser/";
export const getSupervisorUserById = "Supervisor/GetSupervisorUser/";
export const getSupervisorUser = "Supervisor/Get/";
export const getSupervisorUserByUserId = "Supervisor/GetSupervisorByUserId/";
export const getDriverUserById = "Driver/GetDriverUser/";
export const getEngineerUserById = "Engineer/GetEngineerUser/";
export const getTechnicianUserById = "Technician/GetTechnicianUser/";
export const getForemanUserById = "Foreman/GetForemanUser/";
export const getMangerUserById = "Manager/GetManagerUser/";
export const getMateialControllerUserById = "Material/GetMaterialControllerUser/";
///////////GetByAnotherRole
export const getSupervisorUserBydivison = "Supervisor/GetSupervisorsByDivisionId/";
export const getEngineerUserBydivison = "Engineer/GetEngineerUsersByDivisionId/";
export const getDriverUserBydivison = "Driver/GetDriverUsersByDivisionId/";
export const getDispatchersUserBySuper = "Dispatcher/GetDispatcherUsersBySupervisorId/";
export const getFormanUserByDispatcher = "Foreman/GetForemansByDispatcherId/";


////////////////Identity/////////////////
export const getrUserById = "User/Get?id=";
export const checkUserNameExist = "User/IsUsernameExist";
export const checkUserPhoneExist = "User/IsPhoneExist";
export const checkUserEmailExist = "User/IsEmailExist";

///////////////////Team Management////////////////////////
/////////Get/////////
export const addVehicleURL = "Vehicle/Add";
export const updateVehicleURL = "Vehicle/Update";
export const getVehicleAllUrl = "Vehicle/GetAll";
export const getVehicleByPlateNoUrl = "TeamMember/SearchVec?word=";
export const getTechnicianByPFUrl = "TeamMember/SearchTech?word=";
export const getTeamByNameUrl = "Team/SearchByTeamName?word=";
export const getUnassignedVehiclesUrl = "TeamMember/GetUnassignedVehicleMember";
export const getUnassignedMempersUrl = "TeamMember/GetUnassignedMember";
export const getUnassignedMempersByDivisionIdUrl = "TeamMember/GetUnassignedMemberByDivisionId/";
export const getByDispatcherByIdUrl = "Team/GetByDispatcherId/";
export const getTeams = "Team/GetAllWithDispatcher";
export const getAllTeams = "Team/GetAll";
export const getMembersByTeamIdUrl = "TeamMember/GetMembersByTeamId/";

////////////Post///////////////
export const postTeam = "Team/Add";
export const updateTeam = "Team/Update";
export const assignTeamMember = "TeamMember/assignedMemberToTeam";
export const assignTeamMembers = "TeamMember/assignedMembersToTeam/";
export const reassignTeamToDispatcher = "Team/ReassignTeamToDispatcher";
export const unassignedMember = "TeamMember/UnassignedMember";
export const unassignedMembersURL = "TeamMember/UnassignedMembers";
export const postProblemArea = "DispatcherSettings/Add";

////////////delete/////////////////////
export const deleteTeamURL = "Team/Delete/{id}";


////////////////////////Dispatcher Setting////////////
export const OrderProblem = "OrderProblem/GetAll";
export const disSettingUrl = "DispatcherSettings/GetAll";
export const GetMultiAreasAndProblemsbyDispatcherId = "DispatcherSettings/GetMultiAreasAndProblemsbyDispatcherId/{dispatcherId}";
export const getDispatcherSettingById = "DispatcherSettings/GetbyDispatcherId/{0}";
export const deleteDispatcherSetting = "DispatcherSettings/Delete/{0}";
export const disSettingUrls = "DispatcherSettings/Search";
export const postDispatcherAllProblemsandArea = "DispatcherSettings/AddWithMultiAreasAndOrderProblems";
export const UpdateMulitWithAreasAndOrderProblemsUrl = "DispatcherSettings/UpdateMulitWithAreasAndOrderProblems";
export const AddMulitWithAreasAndOrderProblemsUrl = "DispatcherSettings/AddWithMultiAreasAndOrderProblems  ";
export const getALLDispatcherSettingsListUrl = "DispatcherSettings/GetAllDispatchers";
export const getALLDispatcherSettingsbyGroupIdUrl = "DispatcherSettings/GetbyGroupId/{GroupId}";


//////////////////////OrderManagement/////////////////////////////////
//////////////Get/////////////
export const unAssignedOrdersForDispatcherUrl = "Order/GetUnAssignedOrdersForDispatcher?dispatcherId=";
export const assignedOrdersForDispatcherUrl = "Order/GetAssignedOrdersForDispatcher?dispatcherId=";
export const unAssignedOrdersForSupervisorUrl = "Order/GetUnAssignedOrdersForSupervisor?supervisrId=";
export const assignedOrdersForSupervisorUrl = "Order/GetAssignedOrdersForDispatchersBySupervisor?supervisorId=";
export const assigndOrdersToDispatcherUrl = "Order/AssignDispatcher";
export const unAssigndOrdersFromDispatcherUrl = "Order/UnAssignDispatcher";
export const assigndOrdersUrl = "Order/Assign";
export const unAssigndOrdersUrl = "Order/UnAssign";
export const rankTeamOrdersUrl = "Order/ChangeTeamRank";
export const orderDetailsUrl = "Order/Get/{ID}";
export const addOrderUrl = "Order/Add";
export const updateOrderUrl = "Order/Update";
export const getAllPriorityUrl = "OrderPriority/GetAll";
export const getAllStatusUrl = "OrderStatus/GetAll";
export const getCompanyCodeUrl = "CompanyCode/GetAll";
export const allDivisionsUrl = "Division/GetAll";
export const allOrderTypesUrl = "OrderType/GetAll";
export const getPaciLocationUrl = "Paci/GetLocationByPaci?paciNumber=";
export const filterMapOrdersURL = "Order/OrderFilter";
export const orderSubStatus = "OrderSubStatus/GetByStatusId?id=";
export const orderChangeStatus = "Order/ChangeStatus";
export const getFilteredOrdersForPrint = "Order/PrintOut";


////////////////////// time sheet //////////////////
export const TimeSheetUrlgetdate = "OrderAction/GetTimeSheet";
export const exportTimeSheetResultsUrl = "OrderAction/GetTimeSheetExcel";



//////////////////NotificationCenter //////////////////////

export const PostNotificationCenterURL = "NotificationCenter/GetByRecieverIdAndIsRead/";
export const GetNotificationCenterByIdURL = "NotificationCenter/Get/{id}";
export const GetNotificationCenterCountURL = "NotificationCenter/Count/";
export const PostNotificationCenterGetByIdsURL = "NotificationCenter/GetByIds";
export const GetAllNotificationCenterURL = "NotificationCenter/GetAll";
export const PutNotificationCenterItemURL = "NotificationCenter/Update/";
export const PostAddNotificationCenterURL = "NotificationCenter/Add";
export const PostAddMultiNotificationCenterURL = "NotificationCenter/AddMulti";
export const GetAllPaginatedNotificationCenterURL = "NotificationCenter/GetAllPaginated";
export const GetAllPaginatedByPredicateNotificationCenterURL = "NotificationCenter/GetAllPaginatedByPredicate";
export const DeleteNotificationCenterURL = "NotificationCenter/Delete/{id}";
export const GetNotificationCenterCountByRecieverIdURL = "NotificationCenter/CountByRecieverId/{recieverId}";
export const MarkAllAsReadByRecieverIdURL = "NotificationCenter/MarkAllAsRead?recieverId=";
export const PutNotificationCenterChangeTeamOrderRankURL = "Order/ChangeOrderRank/{acceptChange}";
export const PostNotificationCenterFilteredByDate = "NotificationCenter/FilterByDate";



///////////////map settings
export const getMapSettingsURL = "MapAdminRelated/Get/";
export const putMapSettingsURL = "MapAdminRelated/Update";
