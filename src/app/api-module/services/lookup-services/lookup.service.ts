// import {allCustomerLookup} from './../../models/lookups-modal';
import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { AuthService } from "../../../shared/services/auth.service";
// import {Observable} from 'rxjs/Observable';
import * as myGlobals from "../globalPath";

@Injectable()
export class LookupService {

  constructor(private http: Http, private authService: AuthService) {
  }

  logout(obj) {
    return this.http.put(myGlobals.BaseUrlIdentity + myGlobals.logoutUrl + "?userId=" + obj.userId + "&deviceId=" + obj.deviceId, this.authService.jwt()).map((response: Response) => response.json());
  }

  /*new services Mohamed Ragab*/

  /*GET*/

  getAllPreventive() {
    return this.http.get(myGlobals.allPreventive, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllAvailability() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.availabilitysUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getBuildingTypes() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.buildingTypesUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllAreasDataManagement() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.areasUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllAreasDataManagementPaginated(pageNoSize) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.areasUrl, pageNoSize, this.authService.jwt()).map((response: Response) => response.json());
  }

  getCallsHistory() {
    return this.http.get(myGlobals.callsHistory, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllAttendanceStates() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.attendanceStatusUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getCallLogs(id) {
    return this.http.get(myGlobals.callsLogs + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getCustomerCall(id) {
    return this.http.get(myGlobals.GetCustomerDetailsByID + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getCompanyCodes() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.companyCodeUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getItem(code) {
    return this.http.get(myGlobals.item + code, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllEstimations() {
    return this.http.get(myGlobals.estimations, this.authService.jwt()).map((response: Response) => response.json());
  }

  searchByNameOrPhone(searchPhrase) {
    return this.http.get(myGlobals.searchByPhoneOrName + searchPhrase, this.authService.jwt()).map((response: Response) => response.json());
  }

  getEstimationDetails(id) {
    return this.http.get(myGlobals.estimationDetails + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllQuotations() {
    return this.http.get(myGlobals.quotations, this.authService.jwt()).map((response: Response) => response.json());
  }

  getQuoutationByRefernceNo(searchText) {
    return this.http.get(myGlobals.searchQuotation + searchText, this.authService.jwt()).map((response: Response) => response.json());
  }

  getQuotationDetailsById(id) {
    return this.http.get(myGlobals.quotationbyId + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getQuotationDetailsByRefNo(refNo) {
    return this.http.get(myGlobals.quotationbyRefNo + refNo, this.authService.jwt()).map((response: Response) => response.json());
  }

  getContractTypes() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.contractTypesUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  searchContract(refNo) {
    return this.http.get(myGlobals.searchContract + refNo, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllContracts() {
    return this.http.get(myGlobals.allContracts, this.authService.jwt()).map((response: Response) => response.json());
  }

  getContractById(id) {
    return this.http.get(myGlobals.contractById + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getRoles() {
    return this.http.get(myGlobals.roles, this.authService.jwt()).map((response: Response) => response.json());
  }

  getCostCenter() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.costCenterUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getUsers() {
    return this.http.get(myGlobals.users, this.authService.jwt()).map((response: Response) => response.json());
  }

  getDivision() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.divisionUrl, this.authService.jwt()).map((response: Response) => response.json());
  }
  getDivisionEnUrl() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.divisionEnUrl, this.authService.jwt()).map((response: Response) => response.json());

  }

  getGovernorates() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.governoratesUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getShift() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.shiftUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getlanguages() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.languagesUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getOrderPriority() {
    return this.http.get(myGlobals.orderPriorities, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllOrders() {
    return this.http.get(myGlobals.allOrders, this.authService.jwt()).map((response: Response) => response.json());
  }

  searchByOrderNumber(number) {
    return this.http.get(myGlobals.searchOrders + number, this.authService.jwt()).map((response: Response) => response.json());
  }

  getOrderStatus() {
    return this.http.get(myGlobals.orderStatus, this.authService.jwt()).map((response: Response) => response.json());
  }

  getContractByCustomerId(id) {
    return this.http.get(myGlobals.contractByCustomer + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getOrderByCustomerId(id) {
    return this.http.get(myGlobals.orderByCustomer + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getCallByCustomerId(id) {
    return this.http.get(myGlobals.callrByCustomer + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getLocationByCustomerId(id) {
    return this.http.get(myGlobals.locationByCustomer + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getDispatcherOrders(id) {
    return this.http.get(myGlobals.ordersByDis + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getDispatchers() {
    return this.http.get(myGlobals.allDispatchers, this.authService.jwt()).map((response: Response) => response.json());
  }

  getTecs() {
    return this.http.get(myGlobals.unAssignedTecs, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllTecs() {
    return this.http.get(myGlobals.allTecs, this.authService.jwt()).map((response: Response) => response.json());
  }

  getTecsByDis(id) {
    return this.http.get(myGlobals.assignedTecs + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getOrderProgress(id) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.orderProgressUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  addUserDevice(key) {
    const deviceData = {
      'deveiceId': key,
      'fk_AppUser_Id': JSON.parse(localStorage.getItem("AlghanimCurrentUser")).data.userId
    };
    return this.http.post(myGlobals.BaseUrlIdentity + myGlobals.addUserDeviceUrl, deviceData, this.authService.jwt()).map((response: Response) => response.json());
  }

  getProgressStatuses() {
    return this.http.get(myGlobals.progressStatuses, this.authService.jwt()).map((response: Response) => response.json());
  }

  getDispatchersWithProb() {
    return this.http.get(myGlobals.getDiByProb, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllProblems() {
    return this.http.get(myGlobals.allProbs, this.authService.jwt()).map((response: Response) => response.json());
  }
  getAllProblemsLocalized() {
    return this.http.get(myGlobals.allProbsLocalized, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllDivisions() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.allDivisionsUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllOrderTypes() {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.allOrderTypesUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllAreas() {
    return this.http.get(myGlobals.allAreasDispatcher, this.authService.jwt()).map((response: Response) => response.json());
  }

  getDisAllOrders(id) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.getOrdersByDis + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getTecAssignedItems(id) {
    return this.http.get(myGlobals.assignedItems + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getTecReportUsedItems(id) {
    return this.http.get(myGlobals.usedItems + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllCustomers() {
    return this.http.get(myGlobals.allCustomers, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllItems() {
    return this.http.get(myGlobals.getAllItems, this.authService.jwt()).map((response: Response) => response.json());
  }

  downloadQuotationWord(id) {
    return this.http.get(myGlobals.quotationWordUrl + id, this.authService.jwt()).map((response: Response) => response);
  }

  getAllOrderProgress() {
    return this.http.get(myGlobals.allOrderProgressUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllOrderProblems() {
    return this.http.get(myGlobals.allOrderProblemsUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getCustomerDetails(customerId) {
    return this.http.get(myGlobals.getCustomerDetails + customerId, this.authService.jwt()).map((response: Response) => response.json());
  }

  // downloadQuotationWord(id) {
  //   console.log(myGlobals.quotationWordUrl + id);
  //   return this.http.get(myGlobals.quotationWordUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  // }

  /*POST*/

  postNewLog(newLog) {
    return this.http.post(myGlobals.logPostUrl, newLog, this.authService.jwt()).map((response: Response) => response.json());
  }

  postEstimation(estimations) {
    return this.http.post(myGlobals.estimationPostUrl, estimations, this.authService.jwt()).map((response: Response) => response.json());
  }

  postNewCall(call) {
    return this.http.post(myGlobals.callPostUrl, call, this.authService.jwt()).map((response: Response) => response.json());
  }

  generateQuotation(estimation) {
    return this.http.post(myGlobals.generateQuotationUrl, estimation, this.authService.jwt()).map((response: Response) => response.json());
  }

  postNewContract(newContract) {
    return this.http.post(myGlobals.generateContractUrl, newContract, this.authService.jwt()).map((response: Response) => response.json());
  }

  postNewUser(newUser) {
    return this.http.post(myGlobals.newUserUrl, newUser, this.authService.jwt()).map((response: Response) => response.json());
  }

  postItemsExcelSheet(excelSheetObject) {
    return this.http.post(myGlobals.itemsExcelSheetUrl, excelSheetObject, this.authService.jwt()).map((response: Response) => response.json());
  }

  postSalaryExcelSheet(excelSheetObject) {
    return this.http.post(myGlobals.salaryExcelSheetUrl, excelSheetObject, this.authService.jwt()).map((response: Response) => response.json());
  }

  postNewOrder(newOrder) {
    return this.http.post(myGlobals.postNewOrderUrl, newOrder, this.authService.jwt()).map((response: Response) => response.json());
  }

  postCustomerTypes(customerType) {
    return this.http.post(myGlobals.postCustomerTypesUrl, customerType, this.authService.jwt()).map((response: Response) => response.json());
  }

  postCallsTypes(callType) {
    return this.http.post(myGlobals.postCallsTypesUrl, callType, this.authService.jwt()).map((response: Response) => response.json());
  }

  postCallPriorities(callPriority) {
    return this.http.post(myGlobals.postCallPrioritiesUrl, callPriority, this.authService.jwt()).map((response: Response) => response.json());
  }

  postPhoneTypes(phoneType) {
    return this.http.post(myGlobals.postPhoneTypesUrl, phoneType, this.authService.jwt()).map((response: Response) => response.json());
  }

  postActionStatues(status) {
    return this.http.post(myGlobals.postActionStateUrl, status, this.authService.jwt()).map((response: Response) => response.json());
  }

  postContractTypes(contractType) {
    return this.http.post(myGlobals.postContractTypeUrl, contractType, this.authService.jwt()).map((response: Response) => response.json());
  }

  postRoles(role) {
    return this.http.post(myGlobals.postRoleUrl, role, this.authService.jwt()).map((response: Response) => response.json());
  }

  postOrderType(orderType) {
    return this.http.post(myGlobals.postOrderTypeUrl, orderType, this.authService.jwt()).map((response: Response) => response.json());
  }

  postOrderPriority(orderPriority) {
    return this.http.post(myGlobals.postOrderPriorityUrl, orderPriority, this.authService.jwt()).map((response: Response) => response.json());
  }

  postOrderStatus(orderState) {
    return this.http.post(myGlobals.postOrderStateUrl, orderState, this.authService.jwt()).map((response: Response) => response.json());
  }

  postTecnichanWithDis(tecWithDis) {
    return this.http.post(myGlobals.postTecForDist, tecWithDis, this.authService.jwt()).map((response: Response) => response.json());
  }

  postOrderToTec(orderByTec) {
    return this.http.post(myGlobals.orderByTec, orderByTec, this.authService.jwt()).map((response: Response) => response.json());
  }

  postUnAssignedOrder(id) {
    return this.http.get(myGlobals.unAssignedOrder + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  postTecAvailability(availability) {
    return this.http.post(myGlobals.tecAvailable, availability, this.authService.jwt()).map((response: Response) => response.json());
  }

  postVisitTime(date) {
    return this.http.post(myGlobals.postPreferedVisitDate, date, this.authService.jwt()).map((response: Response) => response.json());
  }

  postTransferOrder(transferToDispatcher) {
    return this.http.post(myGlobals.transferToDispatcherUrl, transferToDispatcher, this.authService.jwt()).map((response: Response) => response.json());
  }

  postProgress(progress) {
    return this.http.post(myGlobals.postProgressUrl, progress, this.authService.jwt()).map((response: Response) => response.json());
  }

  postDispatcherWithProblemsAndAreas(DisWithProb) {
    return this.http.post(myGlobals.postDispatcherWithProblemsUrl, DisWithProb, this.authService.jwt()).map((response: Response) => response.json());
  }

  filterOrders(filterObj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.filterMapOrdersURL, filterObj, this.authService.jwt()).map((response: Response) => response.json());
  }

  filterOrdersMap(filterObj) {
    return this.http.post(myGlobals.filterOrdersMapUrl, filterObj, this.authService.jwt()).map((response: Response) => response.json());
  }

  postMapOrderFilter(params) { //api/Order/OrderFilter
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.postMapOrderFilterURL, params, this.authService.jwt()).map((response: Response) => response.json());
  }

  postBoardOrderFilter(params) { // dispatcher board filter function
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.postBoardOrderFilterURL, params, this.authService.jwt()).map((response: Response) => response.json());
  }

  postBoardUnAssignedOrderFilter(params) { // dispatcher board filter function
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.postUnassignedOrdersBoardFilterURL, params, this.authService.jwt()).map((response: Response) => response.json());
  }

  assignItemToTec(obj) {
    return this.http.post(myGlobals.assignItemUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  postCustomer(obj) {
    return this.http.post(myGlobals.postCustomerUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  assignOrdersToDispatcher(obj) {
    return this.http.post(myGlobals.assignOrdersToDispatcherUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  assignOrdersToTec(obj) {
    return this.http.post(myGlobals.assignOrdersToTecUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  searchAssignedItems(searchObject) {
    return this.http.post(myGlobals.assignedItemSearch, searchObject, this.authService.jwt()).map((response: Response) => response.json());
  }

  postOrderProgress(objectProgress) {
    return this.http.post(myGlobals.postOrderProgress, objectProgress, this.authService.jwt()).map((response: Response) => response.json());
  }

  postOrderProblems(objectProblems) {
    return this.http.post(myGlobals.postOrderProblems, objectProblems, this.authService.jwt()).map((response: Response) => response.json());
  }

  filterUsedItems(filterObject) {
    return this.http.post(myGlobals.filterUsedItems, filterObject, this.authService.jwt()).map((response: Response) => response.json());
  }

  filterPreventive(filterObject) {
    return this.http.post(myGlobals.filterPreventiveUrl, filterObject, this.authService.jwt()).map((response: Response) => response.json());
  }

  filterEstimations(filterObject) {
    return this.http.post(myGlobals.filterEstimationsUrl, filterObject, this.authService.jwt()).map((response: Response) => response.json());
  }

  filterQuotations(filterObject) {
    return this.http.post(myGlobals.filterQuotationsUrl, filterObject, this.authService.jwt()).map((response: Response) => response.json());
  }

  postNewLocation(location) {
    return this.http.post(myGlobals.postNewCustomerLocationUrl, location, this.authService.jwt()).map((response: Response) => response.json());
  }

  ////////////////////////////////////////// New //////////////////////////////////////////////////
  getAllRoles() {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.rolesUrl, this.authService.jwt()).map((response: Response) => response.json());
  }
  postArea(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAreasUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editArea(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAreasUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  getAreaLangById(areaId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.areaLanguageByIdUrl + areaId, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteArea(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAreaUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  postProblem(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.addProbURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  postAttendanceStates(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAttendanceStates, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editAttendanceStates(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAttendanceStatesUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteAttendanceStates(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAttendanceStatesUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getAttendanceStatesById(attendId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.attendanceStatesByIdUrl + attendId, this.authService.jwt()).map((response: Response) => response.json());
  }
  postAvailability(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAvailability, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editAvailability(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAvailabilityUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteAvailability(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAvailabilityUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getAvailabilityById(avaliblId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.availabilityByIdUrl + avaliblId, this.authService.jwt()).map((response: Response) => response.json());
  }
  postBuildingTypes(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postBuildingTypes, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editBuildingTypes(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putBuildingTypesUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteBuildingTypes(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteBuildingTypesUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getBuildingTypesById(buildId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.buildingTypesByIdUrl + buildId, this.authService.jwt()).map((response: Response) => response.json());
  }
  postCompanyCode(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postCompanyCode, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editCompanyCode(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putCompanyCodeUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteCompanyCode(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteCompanyCodeUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getCompanyCodeById(companyId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.companyCodeByIdUrl + companyId, this.authService.jwt()).map((response: Response) => response.json());
  }
  postContractType(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postContractTypes, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editContractTypes(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putContractTypesUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteContractTypes(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteContractTypesUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getContractTypeById(contId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.contractTypesByIdUrl + contId, this.authService.jwt()).map((response: Response) => response.json());
  }
  postCostCenter(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postCostCenter, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editCostCenter(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putCostCenterUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteCostCenter(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteCostCenterUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getCostCenterById(costId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.CostCentersByIdUrl + costId, this.authService.jwt()).map((response: Response) => response.json());
  }
  postDivision(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postDivision, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editDivision(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putDivisionUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteDivision(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteDivisionUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getDivisionById(DivisionId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.divisionByIdUrl + DivisionId, this.authService.jwt()).map((response: Response) => response.json());
  }
  postGovernorates(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postGovernorates, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editGovernorates(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putGovernoratesUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteGovernorates(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteGovernoratesUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getGovernoratesById(govId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.governoratesByIdUrl + govId, this.authService.jwt()).map((response: Response) => response.json());
  }
  postShift(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postShift, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editShift(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putShiftUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteShift(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteShiftUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getShiftById(govId) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.ShiftByIdUrl + govId, this.authService.jwt()).map((response: Response) => response.json());
  }
  postLanguages(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postLanguages, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putLanguagesUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteLanguages(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteLanguagesUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  postAreaLanguages(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAreaLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editAreaLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAreaLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteAreaLanguages(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAreaLanguagesUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  postAttendanceStatesLanguages(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAttendanceStatesLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editAttendanceStatesLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAttendanceStatesLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  deleteAttendanceStatesLanguages(id) {
    return this.http.delete(myGlobals.BaseUrlDataManagement + myGlobals.deleteAttendanceStatesLanguagesUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  postAvailabilityLanguage(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postAvailabilityLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editAvailabilityLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putAvailabilityLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  postBuildingTypesLanguage(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postBuildingTypesLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editBuildingTypesLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putBuildingTypesLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  postCompanyCodeLanguage(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postCompanyCodeLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editCompanyCodeLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putCompanyCodeLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  postContractTypesLanguage(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postContractTypesLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editContractTypesLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putContractTypesLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  postCostCenterLanguage(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postCostCenterLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editCostCenterLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putLang_CostCenterLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  postDivisionLanguage(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postDivisionLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editDivisionLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putDivisionLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  postGovernoratesLanguage(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postGovernoratesLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editGovernoratesLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putGovernoratesLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  postShiftLanguage(obj) {
    return this.http.post(myGlobals.BaseUrlDataManagement + myGlobals.postShiftLanguage, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editShiftLanguages(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putShiftLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editProblem(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putProblemLanguageUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  ///////////User Management//////////
  getAllUserAmin() {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.adminUserUrl, this.authService.jwt()).map((response: Response) => response.json());
  }
  getUserById(id) {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.getrUserById + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  postUser(obj) {
    return this.http.post(myGlobals.BaseUrlIdentity + myGlobals.postUserUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editUser(obj) {
    return this.http.put(myGlobals.BaseUrlIdentity + myGlobals.putUserUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  getAllDispatchers() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getDispatcherUrl, this.authService.jwt()).map((response: Response) => response.json());
  }
  getDispatcherById(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getDispatcherUserById + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getDispatcherBySuper(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getDispatchersUserBySuper + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  postUserDispatcher(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postUserDispatcherUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editUserDispatcher(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.putUserDispatcherUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  getAllDrivers() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getDriverUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllIcAgents() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getICAgentUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllSuperVisor() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getSuperVisorUrl, this.authService.jwt()).map((response: Response) => response.json());
  }
  postSuperVisor(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postSupervisorUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editUserSuperVisor(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.putUserSupervisorUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  getSuperVisorById(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getSupervisorUserById + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getSuperVisor(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getSupervisorUser + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getSupervisorByUserId(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getSupervisorUserByUserId + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getSuperVisorByDivision(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getSupervisorUserBydivison + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  postDriver(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postDriverUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editUserDiver(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.putUserDriverUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  editUserManager(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.putUserMangerUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  editUserMateialController(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.putUserMaterialControllerUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  getDriverById(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getDriverUserById + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getManagerById(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getMangerUserById + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getMaterialControllerById(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getMateialControllerUserById + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getDriverByDivision(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getDriverUserBydivison + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getAllEngineers() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getEngineerUrl, this.authService.jwt()).map((response: Response) => response.json());
  }
  postEngineer(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postEngineerUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editUserEngineer(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.putUserEngineerUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  getEngineerById(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getEngineerUserById + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getEngineerByDivision(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getEngineerUserBydivison + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getAllTechnicians() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getTechnicianUrl, this.authService.jwt()).map((response: Response) => response.json());
  }
  postTechnician(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postTechnicianUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editUserTechnician(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.putUserTechnicianUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  getTechnicianById(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getTechnicianUserById + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllForemans() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getForemanUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  postForeman(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postForemanUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  editUserForeman(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.putUserForemanUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }
  getForemanById(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getForemanUserById + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getForemanByDispatcher(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getFormanUserByDispatcher + id, this.authService.jwt()).map((response: Response) => response.json());
  }
  getAllMangaer() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getManagerUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  postManager(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postManagerUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  postMaterialController(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postMaterialControllerUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllMaterial() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getMaterialUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  resetUserPassword(obj) {
    return this.http.post(myGlobals.BaseUrlIdentity + myGlobals.resetPassword, obj, this.authService.jwt()).map((response: Response) => response.json());
  }


  ///////////Team Management//////////
  getAllUnassignedMempers() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getUnassignedMempersUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllUnassignedMempersByDivision(divisionId) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getUnassignedMempersByDivisionIdUrl + divisionId, this.authService.jwt()).map((response: Response) => response.json());

  }

  getAllUnassignedVehicles() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getUnassignedVehiclesUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getTechnicianByPF(pf) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getTechnicianByPFUrl + pf, this.authService.jwt()).map((response: Response) => response.json());
  }

  getTeamByName(name) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getTeamByNameUrl + name, this.authService.jwt()).map((response: Response) => response.json());
  }

  getVehicleByPlateNo(PlateNo) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getVehicleByPlateNoUrl + PlateNo, this.authService.jwt()).map((response: Response) => response.json());
  }

  addVehcile(PlateNo) {
    let obj = {
      plate_no: PlateNo
    };
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.addVehicleURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateVehcile(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.updateVehicleURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllVehicles() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getVehicleAllUrl, this.authService.jwt()).map((response: Response) => response.json());
  }


  getMembersByTeamId(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getMembersByTeamIdUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getTeamByDispatcherById(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getByDispatcherByIdUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getTeams() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getTeams, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllTeams() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getAllTeams, this.authService.jwt()).map((response: Response) => response.json());
  }

  postTeam(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postTeam, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateTeam(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.updateTeam, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  assginMemberToTeam(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.assignTeamMember + "?teamId=" + obj.teamId + "&memberId=" + obj.memberId, null, this.authService.jwt()).map((response: Response) => response.json());
  }

  assginMembersToTeam(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.assignTeamMembers + obj.teamId, obj.members, this.authService.jwt()).map((response: Response) => response.json());
  }
  reassignTeamToDispatcher(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.reassignTeamToDispatcher + "?teamId=" + obj.teamId + "&dispatcherId=" + obj.dispatcherId, null, this.authService.jwt()).map((response: Response) => response.json());
  }
  unAssginMember(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.unassignedMember + "?memberId=" + obj.memberId, null, this.authService.jwt()).map((response: Response) => response.json());
  }


  unAssginMembers(obj) {
    return this.http.put(myGlobals.BaseUrlUserManagement + myGlobals.unassignedMembersURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteTeam(id) {
    return this.http.delete(myGlobals.BaseUrlUserManagement + myGlobals.deleteTeamURL.replace("{id}", id), this.authService.jwt()).map((response: Response) => response.json());
  }


  ///////////dispatcherSetting////////////
  getALLareasEnUrl() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.areasEnUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllOrderProblem() {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.OrderProblem, this.authService.jwt()).map((response: Response) => response.json());
  }

  getALLDispatcherSettingsList() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getALLDispatcherSettingsListUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllDispatcherSettings() {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.disSettingUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllDispatcherSettingspaginated(pageobj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.disSettingUrls, pageobj, this.authService.jwt()).map((response: Response) => response.json());
  }


  getDispatcherSettingById(disId) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getDispatcherSettingById.replace("{0}", disId), this.authService.jwt()).map((response: Response) => response.json());
  }

  GetMultiAreasAndProblemsbyDispatcherId(disId) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.GetMultiAreasAndProblemsbyDispatcherId.replace("{dispatcherId}", disId), this.authService.jwt()).map((response: Response) => response.json());

  }

  deleteDisDetting(disId) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.deleteDispatcherSetting.replace("{0}", disId), this.authService.jwt()).map((response: Response) => response.json());
  }

  postDispatcherAllProblemsandArea(Disobj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postDispatcherAllProblemsandArea, Disobj, this.authService.jwt()).map((response: Response) => response.json())
  }

  UpdateMulitWithAreasAndOrderProblems(Disobj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.UpdateMulitWithAreasAndOrderProblemsUrl, Disobj, this.authService.jwt()).map((response: Response) => response.json())
  }

  AddMulitWithAreasAndOrderProblems(Disobj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.AddMulitWithAreasAndOrderProblemsUrl, Disobj, this.authService.jwt()).map((response: Response) => response.json())
  }


  getALLDispatcherSettingsbyGroup(id) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getALLDispatcherSettingsbyGroupIdUrl.replace("{GroupId}", id), this.authService.jwt()).map((response: Response) => response.json())

  }
  postProblemArea(obj) {
    return this.http.post(myGlobals.BaseUrlUserManagement + myGlobals.postProblemArea, (obj), this.authService.jwt()).map((response: Response) => response.json());
  }


  ////////////////////// time sheet ///////////////////

  GetTimeSheet(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.TimeSheetUrlgetdate, obj, this.authService.jwt()).map((response: Response) => response.json())
  }

  exportTimeSheetResults(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.exportTimeSheetResultsUrl, obj, this.authService.jwt());
  }

  //////////////////order management////////////////////////////
  getAllUnAssignedOrdersForDispatcher(dispatcherId) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.unAssignedOrdersForDispatcherUrl + dispatcherId, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllAssignedOrdersForDispatcher(dispatcherId) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.assignedOrdersForDispatcherUrl + dispatcherId, this.authService.jwt()).map((response: Response) => response.json());
  }

  //////////////////Supervisor////////////////////////////
  getAllUnAssignedOrdersForSupervisor(Id) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.unAssignedOrdersForSupervisorUrl + Id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllAssignedOrdersForSupervisor(Id) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.assignedOrdersForSupervisorUrl + Id, this.authService.jwt()).map((response: Response) => response.json());
  }

  unAssginOrderFromDispatcher(obj) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.unAssigndOrdersFromDispatcherUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  assginOrderToDispatcher(obj) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.assigndOrdersToDispatcherUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }



  assginOrder(obj) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.assigndOrdersUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  unAssginOrder(obj) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.unAssigndOrdersUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  rankTeamOrders(obj) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.rankTeamOrdersUrl, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  getOrderDetails(orderId) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.orderDetailsUrl.replace("{ID}", orderId), this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllSubOrderStatus(statusId) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.orderSubStatus + statusId, this.authService.jwt()).map((response: Response) => response.json());
  }

  changeOrderStatus(obj) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.orderChangeStatus, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  createOrder(order) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.addOrderUrl, order, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateOrder(order) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.updateOrderUrl, order, this.authService.jwt()).map((response: Response) => response.json());
  }

  getLocationByPaci(paciCode) {
    return this.http.get(myGlobals.BaseUrlLocation + myGlobals.getPaciLocationUrl + paciCode, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllPriority() {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.getAllPriorityUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllStatus() {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.getAllStatusUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getCompanyCode() {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.getCompanyCodeUrl, this.authService.jwt()).map((response: Response) => response.json());
  }

  getTeamsForForemen(data) {
    return this.http.get(myGlobals.BaseUrlUserManagement + myGlobals.getTeamsForForemen, this.authService.jwtWithParams(data)).map((response: Response) => response.json());
  }

  getFilteredOrdersForPrint(data) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.getFilteredOrdersForPrint, data, this.authService.jwt()).map((response: Response) => response.json());
  }


  /*DELETE*/

  deleteEstimation(id) {
    return this.http.delete(myGlobals.deletEstimationUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteQuotation(id) {
    return this.http.delete(myGlobals.deleteQuotationUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteContract(id) {
    return this.http.delete(myGlobals.deleteContractUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteCustomertype(id) {
    return this.http.delete(myGlobals.deleteCustomertypeUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteCallType(id) {
    return this.http.delete(myGlobals.deleteCallTypeUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deletePriorities(id) {
    return this.http.delete(myGlobals.deletePrioritiesUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deletePhoneTypes(id) {
    return this.http.delete(myGlobals.deletePhoneTypeUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteStatus(id) {
    return this.http.delete(myGlobals.deleteStatusUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteContractType(id) {
    return this.http.delete(myGlobals.deleteContractTypeUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteRole(id) {
    return this.http.delete(myGlobals.deleteRoleUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteUser(id) {
    return this.http.delete(myGlobals.deleteUserUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteOrder(id) {
    return this.http.delete(myGlobals.deleteOrderUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteOrderType(id) {
    return this.http.delete(myGlobals.deleteOrderTypeUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteOrderPriority(id) {
    return this.http.delete(myGlobals.deleteOrderPriorityUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  deleteOrderStatus(id) {
    return this.http.delete(myGlobals.deleteOrderStatusUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  removeTecFromDis(id) {
    return this.http.delete(myGlobals.deleteTecFromDis + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  removeDisByProb(id) {
    return this.http.delete(myGlobals.removeDisWithProb + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  removeProgressStatus(id) {
    return this.http.delete(myGlobals.removeProgressStatusUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  removeProblem(id) {
    return this.http.delete(myGlobals.removeProblemUrl + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  /*UPDATE*/

  updateEstimation(estimation) {
    return this.http.put(myGlobals.updateEstimationUrl, estimation, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateCustomerTypes(customerType) {
    return this.http.put(myGlobals.updateCustomerTypeUrl, customerType, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateCallsTypes(callType) {
    return this.http.put(myGlobals.updateCallTypeUrl, callType, this.authService.jwt()).map((response: Response) => response.json());
  }

  updatePriorities(priority) {
    return this.http.put(myGlobals.updatePriorityUrl, priority, this.authService.jwt()).map((response: Response) => response.json());
  }

  updatePhoneTypes(phoneType) {
    return this.http.put(myGlobals.updatePhoneTypeUrl, phoneType, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateStatues(status) {
    return this.http.put(myGlobals.updateStatusUrl, status, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateContractTypes(contractType) {
    return this.http.put(myGlobals.updateContractTypeUrl, contractType, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateRoles(role) {
    return this.http.post(myGlobals.updateRoleUrl, role, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateUser(userData) {
    return this.http.post(myGlobals.updateUserUrl, userData, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateOrderStatus(status) {
    return this.http.put(myGlobals.updateOrderStatusUrl, status, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateOrderPriority(priority) {
    return this.http.put(myGlobals.updateOrderPriorityUrl, priority, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateOrderType(type) {
    return this.http.put(myGlobals.updateOrderTypeUrl, type, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateOrderProgressLookUp(progress) {
    return this.http.put(myGlobals.updateOrderProgressUrl, progress, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateOrderProblemsLookUp(problem) {
    return this.http.put(myGlobals.updateOrderProblemsUrl, problem, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateCustomer(customerData) {
    return this.http.put(myGlobals.updateCustomerUrl, customerData, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateLocation(location) {
    return this.http.post(myGlobals.updateLocationUrl, location, this.authService.jwt()).map((response: Response) => response.json());
  }

  /*-----------------------------------------------*/

  GetallGovernorates() {
    return this.http.get(myGlobals.allGovernorates, this.authService.jwt()).map((response: Response) => response.json());

  }

  GetallAreas(Gov: number) {
    return this.http.get(myGlobals.allAreas + "?govId=" + Gov, this.authService.jwt()).map((response: Response) => response.json());

  }

  GetallBlocks(area: number) {
    return this.http.get(myGlobals.allBlocks + "?areaId=" + area, this.authService.jwt()).map((response: Response) => response.json());

  }

  GetallStreets(area: number, block: string, gov: number) {
    return this.http.get(myGlobals.allStreets + "?govId=" + gov + "&areaId=" + area + "&blockName=" + block, this.authService.jwt()).map((response: Response) => response.json());

  }

  GetLocationByPaci(PACI: number) {
    return this.http.get(myGlobals.allGetLocationByPaci + PACI, this.authService.jwt()).map((response: Response) => response.json());

  }


  /////////////////////////// notification center/////////////////////////

  postNotificationCenter(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.PostNotificationCenterURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  getNotificationCenterById(id) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.GetNotificationCenterByIdURL.replace("{id}", id), this.authService.jwt()).map((response: Response) => response.json());
  }

  getNotificationCenterCount(id) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.GetNotificationCenterByIdURL + id, this.authService.jwt()).map((response: Response) => response.json());
  }

  getNotificationCenterByIds(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.PostNotificationCenterGetByIdsURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  GetAllNotificationCenter() {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.GetAllNotificationCenterURL, this.authService.jwt()).map((response: Response) => response.json());
  }

  updateNotificationCenterItem(obj) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.PutNotificationCenterItemURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  AddNotificationCenter(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.PostAddNotificationCenterURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  AddMultiNotificationCenter(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.PostAddMultiNotificationCenterURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllPaginatedNotificationCenter(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.GetAllPaginatedNotificationCenterURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }

  getAllPaginatedByPredicateNotificationCenter(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.GetAllPaginatedByPredicateNotificationCenterURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }


  deleteNotificationCenter(id) {
    return this.http.delete(myGlobals.BaseUrlOrder + myGlobals.DeleteNotificationCenterURL.replace("{id}", id), this.authService.jwt()).map((response: Response) => response.json());
  }

  GetNotificationCenterCountByRecieverId(id) {
    return this.http.get(myGlobals.BaseUrlOrder + myGlobals.GetNotificationCenterCountByRecieverIdURL.replace("{recieverId}", id), this.authService.jwt()).map((response: Response) => response.json());
  }

  putMarkAllAsReadByRecieverIdURL(id) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.MarkAllAsReadByRecieverIdURL + id, this.authService.jwt()).map((response: Response) => response.json());
  }


  putNotificationCenterChangeTeamOrderRank(acceptChange, obj) {
    return this.http.put(myGlobals.BaseUrlOrder + myGlobals.PutNotificationCenterChangeTeamOrderRankURL.replace("{acceptChange}", acceptChange), obj, this.authService.jwt()).map((response: Response) => response.json());
  }


  postNotificationCenterFilteredByDate(obj) {
    return this.http.post(myGlobals.BaseUrlOrder + myGlobals.PostNotificationCenterFilteredByDate, obj, this.authService.jwt()).map((response: Response) => response.json());
  }


  //////////////////////////map settings

  getMapSettings(id) {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.getMapSettingsURL + id, this.authService.jwt()).map((response: Response) => response.json());
  }


  putMapSettings(obj) {
    return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.putMapSettingsURL, obj, this.authService.jwt()).map((response: Response) => response.json());
  }



  // private helper methods
  //private jwt() {
  //  // create authorization header with jwt token
  //  let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
  //  if (currentUser && currentUser.data.token) {
  //    let headers = new Headers({ 'Authorization': 'bearer ' + currentUser.data.token });
  //    headers.append('Content-Type', 'application/json');
  //    return new RequestOptions({ headers: headers });
  //  }
  //}

  // private helper methods
  //private jwtWithParams(params) {
  //  // create authorization header with jwt token
  //  let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
  //  if (currentUser && currentUser.data.token) {
  //    let headers = new Headers({ 'Authorization': 'bearer ' + currentUser.data.token });
  //    headers.append('Content-Type', 'application/json');
  //    return new RequestOptions({ headers: headers, params: params });
  //  }
  //}

  // private jwtDoc() {
  //   // create authorization header with jwt token
  //   let currentUser = JSON.parse(localStorage.getItem('currentUser'));
  //   if (currentUser && currentUser.token.accessToken) {
  //     let headers = new Headers({'Authorization': 'bearer ' + currentUser.token.accessToken});
  //     headers.append('Content-Type', 'application/json');
  //     return new RequestOptions({headers: headers});
  //   }
  // }

  //private header() {
  //  // create authorization header with jwt token
  //  let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
  //  if (currentUser && currentUser.data.token) {
  //    var headers = new Headers({ 'Authorization': 'bearer ' + currentUser.data.token });
  //    headers.append('Content-Type', 'application/json');
  //    return headers;
  //  }
  //}


}
