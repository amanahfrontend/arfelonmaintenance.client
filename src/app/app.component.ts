import { Component, OnInit, DoCheck } from '@angular/core';
import { AuthenticationServicesService } from './api-module/services/authentication/authentication-services.service';
import { UtilitiesService } from './api-module/services/utilities/utilities.service';
import { LookupService } from './api-module/services/lookup-services/lookup.service';
// import {HubConnection} from "@aspnet/signalr-client";
// import {quotationHubUrl} from '../app/api-module/services/globalPath';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, DoCheck {
  title = 'app';
  menuItemToggle: boolean;
  roles: string[];
  toggleFullWidth: any;
  growlMessage = [];

  constructor(private authService: AuthenticationServicesService,
    private utilities: UtilitiesService,
    private lookupService: LookupService) {

  }

  ngDoCheck() {
    this.menuItemToggle = this.authService.CurrentUser();
    this.toggleFullWidth = this.utilities.toggleFullWidth;
    // console.log(!this.hubInited);
  }

  ngOnInit() {
    // this.authService.activateSignalR();
    this.roles = [];
    this.authService.userRoles.subscribe((roles) => {
      this.roles = roles || [];
    });
    this.subscribeToGrowlMessage();
    this.getMapSettings();
  }

  subscribeToGrowlMessage() {
    this.utilities.growlMessage.subscribe((message) => {
      this.growlMessage.push(message);
    });
  }

  openNav() {
    document.getElementById('mySidenav').style.width = '250px';
    document.getElementById('main').style.marginRight = '250px';
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    document.getElementById('main').style.marginRight = '0';
  }

  getMapSettings() {
    this.lookupService.getMapSettings(1).subscribe(
      res => {
        if (res && res.isSucceeded) {
          localStorage.setItem('webApiKey', res.data.web_Api_Key);
        }
      });
  }

}
